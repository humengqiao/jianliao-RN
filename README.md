#**简聊RN版**

##简介：以简聊为原型（我之前做的一个即时通信APP（基于ApiCloud和融云））。此次基于React-Native，redux，redux-saga，leancloud，react-native-navigation ...等技术及组件<br/>

***
##新增功能
 1. 自定义主题色<br/>
 2. 更好的好友添加逻辑<br/>
 3. 支持自定分组及管理<br/>
 ...<br/>

***

##不足及未实现
  1. 登录连接leancloud服务器偶尔失败（可能是我用了登录加密（需要调用云引擎函数，而leancloud有免费实例停机策略，导致有时候第一次很慢））<br/>
  2. 位置消息未实现（位置消息未找到好用的开源地图组件）<br/>
  ...<br/>

***

##截图
  <img src="https://gitee.com/humengqiao/jianliao-RN/raw/master/screenshots/Screenshot_20171129-202012.png" width=200 height=200 alt="登录页"/>
  <img src="https://gitee.com/humengqiao/jianliao-RN/raw/master/screenshots/Screenshot_20171129-202039.png" width=200 height=200 alt="注册页"/>
  <img src="https://gitee.com/humengqiao/jianliao-RN/raw/master/screenshots/Screenshot_20171129-202637.png" width=200 height=200 alt="会话"/>
  <img src="https://gitee.com/humengqiao/jianliao-RN/raw/master/screenshots/Screenshot_20171129-202152.png" width=200 height=200 alt="联系人页"/>
  <img src="https://gitee.com/humengqiao/jianliao-RN/raw/master/screenshots/Screenshot_20171129-202234.png" width=200 height=200 alt="个人中心页"/>
  <img src="https://gitee.com/humengqiao/jianliao-RN/raw/master/screenshots/Screenshot_20171129-202301.png" width=200 height=200 alt="聊天页"/>
