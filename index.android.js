import {Provider} from 'react-redux';
import configureStore from './src/store';
import {registerScreens} from './src/screens';
import rootSaga from './src/store/saga/rootSaga';
import config from './src/config';
import {Realtime} from 'leancloud-realtime';
import AV from 'leancloud-storage';
import {TypedMessagesPlugin} from 'leancloud-realtime-plugin-typed-messages';
import {watchNetStatus} from './src/service/common';
const {store} = configureStore();

 //关闭其中某些yellow警告
 console.ignoredYellowBox = ['Warning: BackAndroid is deprecated. Please use BackHandler instead.','source.uri should not be an empty string','Invalid props.style key']; 
 // 关闭全部yellow警告
console.disableYellowBox = true 

//注册所有screen
registerScreens(store, Provider)

//运行saga
store.runSaga(rootSaga)

/*---------------*/
//初始化leancloud sdk 存储sdk 富媒体插件
//初始化存储sdk
AV.init({
  appId: config.appId,
  appKey: config.appKey,
  serverURLs: 'https://qci4lopd.lc-cn-n1-shared.com'
})

//初始化实时通讯sdk
global.realtime = new Realtime({  //暴露realtime对象给全局使用
  appId: config.appId,
  appKey: config.appKey,
  region: config.region,
  pushOfflineMessages: config.pushOfflineMessages,   //设置离线消息
  plugins: [TypedMessagesPlugin] //注册富媒体消息插件
})

//注册网络状态变化监听
watchNetStatus(store.dispatch)
