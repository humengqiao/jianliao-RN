package com.hmq;

import android.content.Context;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import me.leolin.shortcutbadger.ShortcutBadgeException;
import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by humengqiao on 2017/10/28.
 */

public class badgeModule extends ReactContextBaseJavaModule {
    private static final String moduleName = "badgeModule";
    private final ReactApplicationContext reactContext;

    public badgeModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return moduleName;
    }

    @ReactMethod
    public void setBadge(int badgeCount, Promise promise) {
        try {
            ShortcutBadger.applyCountOrThrow(reactContext, badgeCount);
            promise.resolve("设置badge成功");
        }catch(ShortcutBadgeException e) {
            promise.reject(e.toString());
        }
    }

    @ReactMethod
    public void removeBadge(Promise promise){
        try {
            ShortcutBadger.removeCountOrThrow(reactContext);
            promise.resolve("清除badge成功");
        }catch(ShortcutBadgeException e){
            promise.reject(e.toString());
        }
    }
}
