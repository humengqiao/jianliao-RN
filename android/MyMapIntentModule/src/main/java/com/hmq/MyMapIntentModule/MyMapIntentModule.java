package com.hmq.MyMapIntentModule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import android.provider.Settings;

/**
 * Created by humengqiao on 2017/10/27.
 */

public class MyMapIntentModule extends ReactContextBaseJavaModule {

    public static final String REACTCLASSNAME = "MyMapIntentModule";
    private Context mContext;

    public MyMapIntentModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mContext = reactContext;
    }

    @Override
    public String getName() {
        return REACTCLASSNAME;
    }

    @ReactMethod
    public void openNetWorkActivity(Promise promise){
        try{
            Activity currentActivity = getCurrentActivity();
            if(null!=currentActivity){
                Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                currentActivity.startActivity(intent);
                promise.resolve("调用网络activity成功");
            }
        }catch(Exception e){
            promise.reject(e.toString());
        }
    }
}
