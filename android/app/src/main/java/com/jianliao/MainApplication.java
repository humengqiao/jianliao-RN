package com.jianliao;

import com.RNFetchBlob.RNFetchBlobPackage;
import com.example.qiepeipei.react_native_clear_cache.ClearCachePackage;
import com.facebook.react.ReactPackage;
import com.hmq.MyMapIntentModule.MyReactPackage;
import com.hmq.badgePackage;
import com.imagepicker.ImagePickerPackage;
import com.reactnativecomponent.amaplocation.RCTAMapLocationPackage;
import com.reactnativenavigation.NavigationApplication;
import com.chinaztt.encapsulation.EncryptionReactPackager;
import com.rnim.rn.audio.ReactNativeAudioPackage;
import com.zmxv.RNSound.RNSoundPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {
    @Override
    public boolean isDebug() {
        // Make sure you are using BuildConfig from your own application
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage
        return Arrays.<ReactPackage>asList(
                new EncryptionReactPackager(),
                new MyReactPackage(),
                new badgePackage(),
                new ImagePickerPackage(),
                new RNFetchBlobPackage(),
                new ClearCachePackage(),
                new RCTAMapLocationPackage(),
                new ReactNativeAudioPackage(),
                new RNSoundPackage()
        );
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
