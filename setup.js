import {
  Platform
} from 'react-native'
import {Navigation} from 'react-native-navigation';
import config from './src/config';

export const LOGIN_SCREEN = 'login';
export const HOME_SCREEN = 'home';

export default function startApp(screen, themeColor, selectedIndex = 1) {
  if(screen === HOME_SCREEN) {
    global.client = null
    const navigatorStyle = {
      navBarTextColor: config.navBarTextColor,
      navBarTextFontSize: config.navBarTextFontSize,
      navBarBackgroundColor: themeColor,
      navBarButtonColor: config.navBarButtonColor,
      navBarTitleTextCentered: config.navBarTitleTextCentered,
      statusBarColor: config.statusBarColor,
      navBarHeight: config.navBarHeight
    }

    Navigation.startTabBasedApp({
      tabs: [
        {
          label: '会话',
          screen: 'jianliao.ConversationScreen',
          icon: require('./src/images/conversation.png'),
          selectedIcon: Platform.OS === 'ios' ? require('./src/images/conversation_selected.png') : '',
          iconInsets: Platform.OS === 'ios' ?
            {
              top: 6,
              left: 0,
              bottom: -6,
              right: 0
            }
          :
           null,
          title: '会话',
          navigatorStyle
        },
        {
          label: '联系人',
          screen: 'jianliao.ContactScreen',
          icon: require('./src/images/contact.png'),
          selectedIcon: require('./src/images/contact_selected.png'),
          iconInsets: Platform.OS === 'ios' ?
            {
              top: 6,
              left: 0,
              bottom: -6,
              right: 0
            }
          :
           null,
          title: '联系人',
          navigatorStyle
        },
        {
          label: '个人中心',
          screen: 'jianliao.MeScreen',
          icon: require('./src/images/me.png'),
          selectedIcon: require('./src/images/me_selected.png'),
          iconInsets: Platform.OS === 'ios' ?
            {
              top: 6,
              left: 0,
              bottom: -6,
              right: 0
            }
          :
           null,
          title: '个人中心',
          navigatorStyle
        },
      ],
      tabsStyle: {
        tabBarBackgroundColor: '#fff',
        tabBarButtonColor: '#000',              //ios only
        tabBarSelectedButtonColor: themeColor       //ios only
      },
      appStyle: {
        hideBackButtonTitle: true,
        tabBarSelectedButtonColor: Platform.OS === 'android' ? themeColor : '',  //android only
        tabBarButtonColor: Platform.OS === 'android' ? '#000' : '',             ////android only
        initialTabIndex: selectedIndex
      },
      animationType: 'fade'
    })
  }else if(screen === LOGIN_SCREEN){
    Navigation.startSingleScreenApp({
      screen: {
        screen: 'jianliao.LoginScreen',
        navigatorStyle: {
          navBarHidden: true
        }
      },
      animationType: 'fade'
    })
  }
}
