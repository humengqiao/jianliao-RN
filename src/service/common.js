import AV from 'leancloud-storage';
import * as connectStatus from '../config/connectStatus';
import {netStatusChange} from '../store/actions/chatEntry';

/*
 * phoneNumber int 手机号码
 * verify bool 表示是否设置user表的verifyPhoneNumber字段
 * return Promise
 */
export function getCheckCode(phoneNumber, verify, option = {}) {
  if(verify) {
    return AV.User.requestMobilePhoneVerify(phoneNumber)
  }else {
    return AV.Cloud.requestSmsCode({
      mobilePhoneNumber: phoneNumber
    }, option)
  }
}

/*
 * code int 验证码
 * verify bool 表示是否设置user表的verifyPhoneNumber字段
 * return Promise
 */
export function verifyCode(code, verify, phoneNumber) {
  if(verify) {
    return AV.User.verifyMobilePhone(code)
  }else {
    return AV.Cloud.verifySmsCode(code, phoneNumber)
  }
}

/*
 * width int 图形验证码宽
 * height int 图片验证码高
 * return Promise
 */
export function getCaptcha(width, height) {
  return AV.Captcha.request({
    width,
    height
  })
}

/*
 * captcha string 图形验证码
 * return Promise
 */
export function checkCaptcha(captchaObj, captcha) {
  return captchaObj.verify(captcha)
}


/*
 * email string 邮箱
 * return Promise
 */
export function postResetPassEmail(email) {
  return AV.User.requestPasswordReset(email)
}

/*
 * email string 邮箱
 * return Promise
 */
export function postVerifyEmail(email) {
  return AV.User.requestEmailVerify(email)
}


/*
 * phoneNumber int 手机号码
 * return Promsie
 */
export function getResetPassCheckCode(phoneNumber) {
  return AV.User.requestPasswordResetBySmsCode(phoneNumber)
}


/*
 * code int 验证码
 * phoneNumber int 手机号码
 * return Promise
 */
export function checkResetCheckCode(code, phoneNumber) {
  return AV.User.resetPasswordBySmsCode(code, phoneNumber)
}

export function watchNetStatus(dispatch) {
  global.realtime.on('disconnect', function() {
    dispatch(netStatusChange({status: connectStatus.DISCONNECT}))
  })

  global.realtime.on('offline', function() {
    dispatch(netStatusChange({status: connectStatus.OFFLINE}))
  })

  global.realtime.on('online', function() {
    dispatch(netStatusChange({status: connectStatus.ONLINE}))
  })

  global.realtime.on('schedule', function(attempt, delay) {
    dispatch(netStatusChange({status: connectStatus.SCHEDULE, desc: `${delay}ms后进行第${attempt + 1}次重连`}))
  })

  global.realtime.on('retry', function(attempt) {
    dispatch(netStatusChange({status: connectStatus.RETRY, desc: `正在进行第${attempt + 1}次重连`}))
  })
  global.realtime.on('reconnect', function() {
    dispatch(netStatusChange({status: connectStatus.RECONNECT}))
  })
}

export function runCloudFn(fnName, params) {
  return AV.Cloud.rpc(fnName, params)
}

export function runCloudFnRun(fnName, params) {
  return AV.Cloud.run(fnName, params)
}
