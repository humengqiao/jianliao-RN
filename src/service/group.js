import {doGetuser} from './user';
import AV from 'leancloud-storage';

export function doGetgroup(key, value, expectFields = ['groupId', 'nick', 'creator', 'avatar', 'members']) {
  if(key.replace(/\s/g, '') === '') {
    throw new Error('key不能为空')
  }

  const query = new AV.Query('Group')
  return query
    .equalTo(key, value)
    .select(expectFields)
    .first()
}

export async function searchGroup(keywords, myId) {
  const query1 = new AV.Query('Group')
  const query2 = new AV.Query('Group')
  query1.equalTo('groupId', keywords)
  query2.equalTo('nick', keywords)
  const combineQuery = AV.Query.or(query1, query2)
  const target = await combineQuery
      .select(['groupId', 'nick', 'creator', 'avatar', 'members'])
      .first()
  if(!target) {
    return null
  }

  const targetToJSON = target.toJSON()
  const me = await doGetuser('_id', myId, ['group'])
  const meToJSON = me.toJSON()
  const inMyGroup = meToJSON.group.list.findIndex(item => item === target.objectId) !== -1 ? true : false
  return {
    target: targetToJSON,
    inMyGroup
  }
}

export async function createGroup(groupId, nick, user, pic) {
  const result = await doGetgroup('groupId', groupId)
  if(result) {
    return Promise.reject('该群组已存在')
  }

  const groupClass = new AV.Object.extend('Group')
  const group = new groupClass()
  if(pic) {
    const file = new AV.File(pic.fileName, {
      blob: {
        uri: pic.uri
      },
      owner: {
        id: user.username
      }
    }, pic.type)
    group.set('avatar', file)
  }
  group.set('groupId', groupId)
  group.set('nick', nick)
  group.set('members', [user.id])
  group.set('creator', user.username)
  const newGroup = await group.save()
  const newGroupId = newGroup.id
  const me = await doGetuser('username', user.username, ['group'])
  const myGroupList = [...(me.get('group')).list]
  myGroupList.unshift(newGroupId)
  me.set('group.list', myGroupList)
  return await me.save()
}
