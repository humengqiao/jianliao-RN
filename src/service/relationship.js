import AV from 'leancloud-storage';
import {doGetuser} from './user';

//添加联系人
export async function addUserToContact(myId, contactId, divideName) {
  const query = new AV.Query('_User')
  query.select(['contact'])
  const user = await query.get(myId)
  const contactList = user.get('contact')
  contactList.forEach(contact => {
    if(contact.name === divideName) {
      const index = contact.list.findIndex(item => item === contactId)
      if(index === -1) {
        contact.list.push(contactId)
      }
    }
  })
  user.set('contact', contactList)
  try {
    await user.save()
    notifyUtil.toast('添加联系人成功')
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

//删除联系人
export async function removeUserFromContact(myId, userId, divideName) {
  const query = new AV.Query('_User')
  query.select(['contact'])
  const user = await query.get(myId)
  const contactList = user.get('contact')
  contactList.forEach(contact => {
    if(contact.name === divideName) {
      const index = contact.list.findIndex(item => item === userId)
      if(index !== -1) {
        contact.list.splice(index, 1)
      }
    }
  })
  user.set('contact', contactList)
  return user.save()
}

//移到黑名单
export async function moveToBlacklist(myId, userId, divideName) {
  const query = new AV.Query('_User')
  query.select(['contact', 'blacklist'])
  const user = await query.get(myId)
  const contactList = user.get('contact')
  const blackList = user.get('blacklist')
  contactList.forEach(contact => {
    if(contact.name === divideName) {
      const index = contact.list.findIndex(item => item === userId)
      if(index !== -1) {
        contact.list.splice(index, 1)
      }
    }
  })
  blackList.list.unshift(userId)
  user.set('contact', contactList)
  user.set('blacklist', blackList)
  return user.save()
}

//获取每个分组联系人列表
export function fetchContactList(contact, expectField = ['id', 'username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'birth', 'canBeSearched']) {
  const {label, name, list} = contact
  return new Promise(async (resolve) => {
    const wrappedList = list.map(id => {
      return new Promise(async (resolve) => {
        const query = new AV.Query('_User')
        query.equalTo('_id', id)
        query.select(expectField)
        try {
          const result = await query.first()
          const data = result.toJSON()
          const user = {
            ...data,
            avatar: {
              url: data.avatar.url,
              name: data.avatar.name,
              mime_type: data.avatar.mime_type
            }
          }
          resolve(user)
        }catch(error) {
          resolve(null)
        }
      })
    })

    let users = await Promise.all(wrappedList)
    users = users.filter(item => item !== null)
    resolve({
      label,
      name,
      list: users,
      expand: false
    })
  })
}

export function fetchGroupList(group, expectField = ['id', 'groupId', 'nick', 'avatar', 'creator', 'members']) {
  const {label, name, list} = group
  return new Promise(async (resolve) => {
    const wrappedList = list.map(async (id) => {
      return new Promise(async (resolve) => {
        const query = new AV.Query('Group')
        query.equalTo('_id', id)
        query.select(expectField)
        try {
          const result = await query.first()
          const data = result.toJSON()
          const group = {
            ...data,
            avatar: {
              url: data.avatar.url,
              name: data.avatar.name,
              mime_type: data.avatar.mime_type
            }
          }
          resolve(group)
        }catch(error) {
          resolve(null)
        }
      })
    })

    let groups = await Promise.all(wrappedList)
    groups = groups.filter(item => item !== null)
    resolve({
      label,
      name,
      list: groups,
      expand: false
    })
  })
}

export function fetchBlackList(blackList, expectField = ['id', 'username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'birth', 'canBeSearched']) {
  const {label, name, list} = blackList
  return new Promise(async (resolve) => {
    const wrappedList = list.map(async (id) => {
      return new Promise(async (resolve) => {
        const query = new AV.Query('_User')
        query.equalTo('_id', id)
        query.select(expectField)
        try {
          const result = await query.first()
          const data = result.toJSON()
          const user = {
            ...data,
            avatar: {
              url: data.avatar.url,
              name: data.avatar.name,
              mime_type: data.avatar.mime_type
            }
          }
          resolve(user)
        }catch(error) {
          resolve(null)
        }
      })
    })

    let users = await Promise.all(wrappedList)
    users = users.filter(item => item !== null)
    resolve({
      label,
      name,
      list: users,
      expand: false
    })
  })
}

export function modifyContact(myId, contact) {
  const user = AV.Object.createWithoutData('_User', myId)
  user.set('contact', contact)
  return user.save()
}

export async function removeUserFromBlacklist(myId, userId) {
  const user = AV.Object.createWithoutData('_User', myId)
  const result = await user.fetch()
  const blackList = result.get('blacklist')
  const index = blackList.list.findIndex(item => item === userId)
  if(index !== -1) {
    blackList.list.splice(index, 1)
  }
  user.set('blacklist', blackList)
  return user.save()
}

export async function exitGroup(groupId, myId) {  //groupId为id字段
  const wrappedOp = [
    new Promise(async resolve => {
      const user = AV.Object.createWithoutData('_User', myId)
      const result = await user.fetch()
      const groupList = result.get('group')
      const index = groupList.list.findIndex(item => item === groupId)
      if(index !== -1) {
        groupList.list.splice(index, 1)
      }
      user.set('group', groupList)
      resolve(user.save())
    }),
    new Promise(async resolve => {
      const group = AV.Object.createWithoutData('Group', groupId)
      const result = await group.fetch()
      const members = result.get('members')
      console.log(members)
      if(members.length === 1) {  //没有成员，即删除此群
        resolve(group.destroy())
      }else {
        const index = members.findIndex(item => item === myId)
        if(index !== -1) {
          members.splice(index, 1)
        }
        group.set('members', members)
        resolve(group.save())
      }
    })
  ]
  return await Promise.all(wrappedOp)
}

export function fetchGroupMembers(members, creator) {
  const membersWithoutCreator = members.filter(item => item !== creator)
  const wrappedMembers = membersWithoutCreator.map(item => {
    return doGetuser('_id', item)
  })

  return Promise.all(wrappedMembers)
}

export function modifyGroupInfoToServer(id, nick, pic) {
  const group = AV.Object.createWithoutData('Group', id)
  if(nick) {
    group.set('nick', nick)
  }

  if(pic) {
    const file = new AV.File(pic.fileName, {
      blob: {
        uri: pic.uri
      },
      owner: {
        id: ''
      }
    }, pic.type)
    group.set('avatar', file)
  }

  return group.save()
}
