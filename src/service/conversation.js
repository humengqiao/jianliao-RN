import * as types from '../config/conversationType';
import * as conversationTypes from '../config/conversationType';
import notifyUtil from '../utils/notifyUtil';
import {doGetuser} from '../service/user';

export function createConversation(params) {
  return global.client.createConversation(params)
}

export function fetchConversation(type) {
  if(type === conversationTypes.COMMON) {
    return global.client
      .getQuery()
      .withLastMessagesRefreshed(true)
      .containedIn('type', [conversationTypes.PRIVATE, conversationTypes.GROUP])
      .limit(1000)   //尽量全部获取，要计算所有会话消息总未读数，1000足够大了
      .find()
  }

  return global.client
    .getQuery()
    .withLastMessagesRefreshed(true)
    .equalTo('type', type)
    .limit(1000)   //尽量全部获取，要计算所有会话消息总未读数，1000足够大了
    .find()
}

export async function fetchConversationOrCreate(members, name, conversationType) {
  try {
    const result = await global.client
      .getQuery()
      .withLastMessagesRefreshed(true)
      .containsMembers(members)
      .equalTo('type', conversationType)
      .find()
    if(result.length === 0) {
      return createConversation({
        members,
        name,
        type: conversationType
      })
    }else {
      return result[0]
    }
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

export async function hideConversation(conversationId, username, flag) {
  try {
    const result = await doGetuser('username', username, ['hideConversationList'])
    const hideConversationListToJSON = result.toJSON().hideConversationList
    const hideConversationList = [...hideConversationListToJSON]
    const index = hideConversationList.findIndex(item => item === conversationId)
    if(flag) {
      if(index === -1) {
        hideConversationList.push(conversationId)
      }
    }else {
      if(index !== -1) {
        hideConversationList.splice(index, 1)
      }
    }
    result.set('hideConversationList', hideConversationList)
    return result.save()
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

export async function queryConversation(conversationType, target) {
  const result = await global.client.getQuery()
  if(conversationType === conversationTypes.GROUP) {
    result.equalTo('groupId', target)
  }else if(conversationType === conversationTypes.PRIVATE){
    result.withMembers([target], true)
  }

  return result
    .equalTo('type', conversationType)
    .find()
}
