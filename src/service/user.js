import AV from 'leancloud-storage';
import config from '../config';
import notifyUtil from '../utils/notifyUtil';
import {getDeviceId} from '../utils/common';
import errorTranslate from '../utils/leancloudErrorMsg';
import {signatureFactory, conversationSignatureFactory} from '../utils/signature';

export function doGetuser(key, value, expectFields = ['username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'onlyCurrentDeviceLogin', 'canBeSearched', 'deviceId', 'emailVerified', 'birth', 'mobilePhoneVerified']) {
  if(key.replace(/\s/g, '') === '') {
    throw new Error('key不能为空')
  }

  const query = new AV.Query('_User')
  return query
    .equalTo(key, value)
    .select(expectFields)
    .first()
}

export function doLogin(username, password) {
  return AV.User.logIn(username, password)
}

export function doLogout() {
  return Promise.all([AV.User.logOut(), global.client.close()])
}

export function createClient(username) {
  return global.realtime.createIMClient(username, {
    signatureFactory,              //登录操作认证
    conversationSignatureFactory   //会话操作认证
  })
}

export function doRegister(username, password, nick, email, birth, phoneNumber) {
  let user = new AV.User()
  user.setUsername(username)
  user.setPassword(password)
  user.setEmail(email)
  user.setMobilePhoneNumber(phoneNumber)
  user.set('nick', nick)
  user.set('birth', birth)
  user.set('gender', config.sex)
  user.set('deviceId', getDeviceId())
  user.set('onlyCurrentDeviceLogin', config.onlyCurrentDeviceLogin)
  user.set('canBeSearched', config.canBeSearched)
  return user.signUp()
}

export async function getCurrentUser() {
  return AV.User.currentAsync()
}

export async function updateAvatar(pic) {
  try {
    const currentUser = await getCurrentUser()
    const file = new AV.File(pic.fileName, {
      blob: {
        uri: pic.uri
      },
      owner: {
        id: currentUser.username
      }
    }, pic.type)
    currentUser.set('avatar', file)
    return currentUser.save()
  }catch(error) {
    notifyUtil.toast(errorTranslate(error), 'long')
  }
}

export function checkUserVerifyPhoneNumebr(phoneNumber) {
  return doGetuser('mobilePhoneNumber', phoneNumber)
}

export function postFeedback(name, phonenumber, qq, feedback) {
  const Feedback = AV.Object.extend('Feedback')
  let feedbackObj = new Feedback()
  feedbackObj.set({
    name,
    phonenumber,
    qq,
    feedback,
    solved: false,
    solvedBy: ''
  })
  return feedbackObj.save()
}

export async function searchContact(keywords, myId) {
    const query1 = new AV.Query('_User')
    const query2 = new AV.Query('_User')
    query1.equalTo('username', keywords)
    query2.equalTo('nick', keywords)
    const combineQuery = AV.Query.or(query1, query2)
    const target = await combineQuery
        .select(['username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'canBeSearched', 'birth', 'contact'])
        .first()
    if(!target) {
      return null
    }

    const targetToJSON = target.toJSON()
    const me = await doGetuser('_id', myId, ['contact', 'blacklist'])
    const meToJSON = me.toJSON()
    const index = meToJSON.blacklist.list.findIndex(item => item === targetToJSON.objectId)
    if(index !== -1) {
      return null
    }

    let inMyContact = false
    meToJSON.contact.forEach(item => {
      const index = item.list.findIndex(item => item === targetToJSON.objectId)
      if(index !== -1) {
        inMyContact = true
      }
    })

    const isMe = targetToJSON.objectId === myId ? true : false
    return {
      target: targetToJSON,
      inMyContact,
      isMe
    }
  }
