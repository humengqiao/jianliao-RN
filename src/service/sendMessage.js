import {TextMessage} from 'leancloud-realtime';
import * as customMessageTypes from '../config/customMessageType';
import * as conversationTypes from '../config/conversationType';
import {ImageMessage, AudioMessage} from 'leancloud-realtime-plugin-typed-messages';
import AV from 'leancloud-storage';
import {Platform} from 'react-native';
import config from '../config';
import {runCloudFnRun} from '../service/common';

export function sendAddContactMessage(conversation, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.ADD_CONTACT
  })
  return conversation.send(message)
}

export function sendAddGroupMessage(conversation, creator, groupId, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.ADD_GROUP
  })
  return conversation.send(message)
}

export function sendRefuseContactMessage(conversation, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.REFUSE_CONTACT
  })
  return conversation.send(message)
}

export function sendAcceptContactMessage(conversation, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.ACCEPT_CONTACT
  })
  return conversation.send(message)
}

export async function sendExitGroupMessage(creator, groupId, attributes) {
  const conversation = await global.client.createConversation({
    members: [creator],
    name: '',
    transient: false,
    unique: false,
    type: conversationTypes.GROUP_NOTIFY,
    groupId
  })
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.SELFEXIT_GROUP
  })
  return conversation.send(message)
}

export function sendRefuseGroupMessage(conversation, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.REFUSE_JOINGROUP
  })
  return conversation.send(message)
}

export function sendAcceptGroupMessage(conversation, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.ACCEPT_JOINGROUP
  })
  return conversation.send(message)
}

export function sendKickGroupMessage(conversation, attributes) {
  const message = new TextMessage()
  message.setAttributes({
    ...attributes,
    type: customMessageTypes.BEKICKED_GROUP
  })
  return conversation.send(message)
}

export function sendTextMessage(messageContent, conversation, attributes) {
  const message = new TextMessage(messageContent)
  message.setAttributes({
    ...attributes
  })
  return conversation.send(message)
}

export async function sendImageMessage(pic, conversation, attributes) {
  const file = new AV.File(pic.fileName, {
    blob: {
      uri: pic.uri
    },
    owner: {
      id: attributes.senderUserName
    }
  }, pic.type)
  file.metaData('width', pic.width)
  file.metaData('height', pic.height)
  const result = await file.save()
  const message = new ImageMessage(result)
  message.setText('[图片]')
  message.setAttributes({
    ...attributes
  })
  return conversation.send(message)
}

export async function sendAudioMessage(audioPath, conversation, attributes, duration) {
  const fileName = audioPath.match(/\/(\w+\.(\w+))$/)[1]
  const form = new FormData()
  let filePath = ''
  Platform.OS === 'android' ? filePath += 'file://' + audioPath : filePath = audioPath
  const fileObj = {uri: filePath, type: 'application/octet-stream', name: fileName}
  form.append('file', fileObj)
  form.append('filename', fileName)
  const token = await runCloudFnRun('genQiniuAuthorization')
  form.append('token', token)
  const uploadResponse = await fetch('http://up-z2.qiniup.com', {
    method: 'POST',
    headers: {
      Authorization: `Qiniu ${token}`,
      Date: (new Date()).toUTCString(),
      "Content-Type":"multipart/form-data"
    },
    body: form
  })
  const uploadResult = await uploadResponse.json()
  const fileUrl = `${config.qiniuResourceHost}/${uploadResult.hash}`
  const messageFromUrl = new AV.File.withURL(fileName, fileUrl)
  const saveResult = await messageFromUrl.save()
  const message = new AudioMessage(saveResult)
  message.setText('[语音]')
  message.setAttributes({
    ...attributes,
    duration: parseInt(duration)
  })
  return conversation.send(message)
}
