import {genFetchPostParams} from '../utils/common';

/*
 * url string 请求的url地址
 * params object 提交参数
 * headers http请求头
 * return Promise
 */
export function doPost(url, params, headers={}) {
  return fetch(url, {
    method: 'POST',
    headers,
    body: genFetchPostParams(params)
  })
}


/*
 * url string 请求url地址
 * headers http请求头
 * return Promise
 */
export function doGet(url, headers={}) {
  return fetch(url, {
    headers
  })
}
