import logger from 'redux-logger';

let middleWares = []

if(__DEV__) {
  middleWares.push(logger)
}

export default middleWares
