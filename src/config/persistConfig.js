import {AsyncStorage} from 'react-native';
import config from './index';

export default {
  storage: AsyncStorage,
  keyPrefix: 'jianliaoRN_'
}
