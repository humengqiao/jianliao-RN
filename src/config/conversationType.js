export const PRIVATE = 'PRIVATE'
export const GROUP = 'GROUP'
export const COMMON = 'COMMON'  //COMMON为PRIVATE和GROUP会话（只做公共部分逻辑判断）
export const CONTACT_NOTIFY = 'CONTACT_NOTIFY'
export const GROUP_NOTIFY = 'GROUP_NOTIFY'
