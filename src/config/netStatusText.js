import * as connectStatus from './connectStatus';

export default {
  DISCONNECT: '与服务端连接断开',
  OFFLINE: '网络不可用',
  ONLINE: '网络恢复',
  RECONNECT: '与服务端连接恢复'
}
