export default {
  appId: 'qcI4lOpd5Nplblfoj0iYvGdG-gzGzoHsz',
  appKey: 'n3BhovwbkNk6zCLRNacPK0we',
  region: 'cn',  //leancloud 连接的地区(cn中国, us美国)
  defaultAvatarName: 'default_avatar.jpg',
  defaultAvatar: 'asset:/images/default_avatar.jpg',
  defaultGroupAvatar: 'asset:/images/default_group_avatar.jpg',
  qiniuResourceHost: 'http://rhbabca4y.hn-bkt.clouddn.com',
  sex: '男',
  ex: {},
  minUsernameLength: 6,
  maxUsernameLength: 12,
  avatarWidth: 80,
  avatarHeight: 80,
  selectedIndex: 1,
  netStatus: '',
  getCheckCodeDelay: 10,  //10秒
  selectBirthTitle: '请选择您的生日',
  smsSendLeftTime: 10,
  phoneStatusText: '获取验证码(必填)',
  navBarHeight: 50,
  navBarTextFontSize: 20,
  navBarTextColor: '#fff',
  navBarButtonColor: '#fff',
  navBarTitleTextCentered: true,
  statusBarColor: '#555',
  enableCaptchaVerify: true,   //开启图形验证码
  birthYear: 1994,
  birthMonth: 8, //从0开始， 8表示9月
  birthDay: 17,
  onlyCurrentDeviceLogin: false,  //只允许当前设备登录（根据设备唯一id识别）
  canBeSearched: true,
  pushOfflineMessages: true,
  updateConversationDuration: 12 * 3600, //更新会话间隔（秒）
  showChatMessageDelay: 3,             //显示聊天消息时间间隔（分钟）
  fetchChatMessageCount: 5            //每次获取聊天记录条数
}
