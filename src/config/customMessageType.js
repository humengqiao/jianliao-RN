export const ADD_CONTACT = 1   //添加联系人消息
export const ACCEPT_CONTACT = 2  //同意添加联系人消息
export const REFUSE_CONTACT = 3  //拒绝添加联系人消息
export const ADD_GROUP = 4      //添加群组消息
export const ACCEPT_JOINGROUP = 5 //同意加入群组消息
export const REFUSE_JOINGROUP = 6  //拒绝加入群组消息
export const BEKICKED_GROUP = 7    //被群创建者踢出
export const SELFEXIT_GROUP = 8    //主动退出群组
