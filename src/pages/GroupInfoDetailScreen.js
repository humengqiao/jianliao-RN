import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import {connect} from 'react-redux';
import commonStyle from '../styles/common';
import {bindActionCreators} from 'redux';
import {getScreenSize, getPixel} from '../utils/common';
import ActionSheet from 'react-native-actionsheet';
import LoadingIndicator from '../components/LoadingIndicator';
import {exitGroup} from '../service/relationship';
import notifyUtil from '../utils/notifyUtil';
import {exitGroup as exitGroupAction} from '../store/actions/relationship';
import {sendExitGroupMessage} from '../service/sendMessage';
import {CustomCachedImage} from 'react-native-img-cache';
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
import * as conversationTypes from '../config/conversationType';

class GroupInfoDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      loading: false,
      actionSheetOptions: []
    }
  }

  onNavigatorEvent(event) {
    const themeColor = this.props.themeColor

    let actionSheetOptions
    if(event.type === 'NavBarButtonPress' && event.id === 'more') {
      actionSheetOptions = [
        <View style={{flexDirection: 'row'}}>
          <Image
            source={require('../images/exit.png')}
            style={{width: 20, height: 20, marginRight: 5, tintColor: themeColor}}/>
          <Text style={{color: themeColor, fontSize: 16}}>退出群</Text>
        </View>,
        <View style={{flexDirection: 'row'}}>
          <Image
            source={require('../images/group_members.png')}
            style={{width: 20, height: 20, marginRight: 5, tintColor: themeColor}}/>
          <Text style={{color: themeColor, fontSize: 16}}>查看群成员</Text>
        </View>,
        <View style={{flexDirection: 'row'}}>
          <Image
            source={require('../images/update.png')}
            style={{width: 20, height: 20, marginRight: 5, tintColor: themeColor}}/>
          <Text style={{color: themeColor, fontSize: 16}}>修改群资料</Text>
        </View>,
        <Text style={{color: themeColor, fontSize: 16}}>取消</Text>
      ]

      this.setState({
        actionSheetOptions
      }, () => {
        this.ActionSheet.show()
      })
    }
  }

  jumpToChatPage() {
    const {themeColor, group} = this.props
    const target = group.groupId
    const conversationType = conversationTypes.GROUP
    this.props.navigator.resetTo({
      screen: 'jianliao.ChatScreen',
      title: `与${group.nick}聊天中`,
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ],
        rightButtons: [
          {
            id: 'more',
            title: '更多',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        target,
        themeColor,
        conversationType
      }
    })
  }

  jumpToAddGroupPage() {
    const {themeColor, group, origin} = this.props
    this.props.navigator.push({
      screen: 'jianliao.AddUserOrGroupScreen',
      title: '群组添加',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ],
        rightButtons: [
          {
            id: 'send',
            title: '发送',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        group,
        themeColor,
        type: 'group',
        origin
      }
    })
  }

  showExitGroupConfirm() {
    Alert.alert(
      '提示',
      '您确定要退出该群吗？',
      [
        {text: '取消', onPress: () => {}, style: 'cancel'},
        {text: '确定', onPress: () => this.onEixtGroup()},
      ],
      {cancelable: false}
    )
  }

  async onEixtGroup() {
    this.setState({loading: true})
    try {
      const myId = this.props.id
      const nick = this.props.nick
      const {objectId: id, groupId, nick: groupName} = this.props.group
      const result = await exitGroup(id, myId)
      await sendExitGroupMessage(this.props.group.creator, groupId, {senderUserNick: nick, groupName})
      this.props.exitGroupAction(groupId)
      this.setState({loading: false})
      notifyUtil.toast('退出群组成功')
      this.props.navigator.dismissModal()
    }catch(error) {
      this.setState({loading: false})
      notifyUtil(error.toString(), 'long')
    }
  }

  async onPressItem(index) {
    if(index === this.state.actionSheetOptions.length -1) {
      return
    }

    const themeColor = this.props.themeColor
    const group = this.props.group
    const myId = this.props.id
    if(index === 0) {
      this.showExitGroupConfirm()
    }else if(index === 1) {
      const username = this.props.username
      this.props.navigator.push({
        screen: 'jianliao.GroupMembersScreen',
        title: '群成员',
        animationType: 'slide-up',
        navigatorStyle: {
          navBarTitleTextCentered: true,
          navBarBackgroundColor: themeColor,
          navBarTextColor: '#fff',
          screenBackgroundColor: '#fff'
        },
        navigatorButtons: {
          leftButtons: [
            {
              id: 'back',
              buttonColor: '#fff'
            }
          ]
        },
        passProps: {
          group,
          themeColor,
          username,
          myId
        }
      })
    }else if(index === 2) {  //修改群资料
      this.props.navigator.push({
        screen: 'jianliao.ModifyGroupInfoScreen',
        title: '修改群资料',
        animationType: 'slide-up',
        navigatorStyle: {
          navBarTitleTextCentered: true,
          navBarBackgroundColor: themeColor,
          navBarTextColor: '#fff',
          screenBackgroundColor: '#fff'
        },
        navigatorButtons: {
          leftButtons: [
            {
              id: 'back',
              buttonColor: '#fff'
            }
          ]
        },
        passProps: {
          group,
          themeColor
        }
      })
    }
  }

  renderButton() {
    const themeColor = this.props.themeColor
    const canSendMessage = this.props.canSendMessage
    if(canSendMessage) {
      return (
        <TouchableOpacity
          onPress={() => this.jumpToChatPage()}
          style={[styles.button, {backgroundColor: themeColor}]}>
          <Text style={styles.buttonText}>发送消息</Text>
        </TouchableOpacity>
      )
    }else {
      return (
        <TouchableOpacity
          onPress={() => this.jumpToAddGroupPage()}
          style={[styles.button, {backgroundColor: themeColor}]}>
          <Text style={styles.buttonText}>添加群组</Text>
        </TouchableOpacity>
      )
    }
  }

  renderGroupInfo() {
    const themeColor = this.props.themeColor
    const titleCommonStyle = {
      borderBottomColor: themeColor,
      color: themeColor
    }

    const {
      groupId,
      nick,
      creator,
      members
    } = this.props.group

    return (
      <View style={styles.content}>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>群名称：{groupId}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>群昵称：{nick}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>创建者：{creator}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>成员数：{members.length}</Text>
        {
          this.renderButton()
        }
      </View>
    )
  }

  render() {
    const avatarUrl = this.props.group.avatar.url
    const themeColor = this.props.themeColor
    return (
      <ScrollView
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={true}
        overScrollMode='always'>
        <CustomCachedImage
          component={CustomImage}
          source={{uri: avatarUrl}}
          indicator={Pie}
          style={styles.avatar}
          indicatorProps={{
            size: 80,
            borderWidth: 0,
            color: themeColor,
            unfilledColor: '#999'
        }}/>
        {
          this.renderGroupInfo()
        }
        <ActionSheet
          ref={el => this.ActionSheet = el}
          title={<Text style={{fontSize: 18, color: themeColor}}>提示</Text>}
          tintColor={themeColor}
          options={this.state.actionSheetOptions}
          cancelButtonIndex={this.state.actionSheetOptions.length - 1}
          onPress={index => this.onPressItem(index)}/>
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    paddingTop: 40,
    paddingHorizontal: 10
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  content: {
    marginTop: 20,
    width: getScreenSize().width - 60
  },
  title: {
    fontSize: 20,
    borderBottomWidth: getPixel() * 3,
    paddingVertical: 10,
    textAlignVertical: 'center',
    fontFamily: 'fzhaofang'
  },
  button: {
    height: 40,
    marginTop: 10,
    borderRadius: getPixel() * 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 18
  }
})

export default connect(state => ({
  id: state.user.user.id,
  username: state.user.user.username,
  nick: state.user.user.nick
}), dispatch => ({
  exitGroupAction: bindActionCreators(exitGroupAction, dispatch)
}))(GroupInfoDetailScreen)
