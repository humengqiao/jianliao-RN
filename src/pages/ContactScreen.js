import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  RefreshControl,
  FlatList,
  ScrollView,
  Alert,
  DeviceEventEmitter
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as chatEntryActionCreator from '../store/actions/chatEntry';
import * as relationshipActionCreator from '../store/actions/relationship';
import * as userActionCreator from '../store/actions/user';
import NetError from '../components/netError';
import * as connectStatus from '../config/connectStatus';
import MyMapIntentModule from '../libs/MyMapIntentModule';
import {setOrRemoveBadge} from '../utils/common';
import notifyUtil from '../utils/notifyUtil';
import commonStyle from '../styles/common';
import {getScreenSize} from '../utils/common';
import RelationCell from '../components/RelationCell';
import DivideMenu from '../components/DivideMenu';

class ContactScreen extends Component {
  constructor(props) {
    super(props)
    this.cellRefs = []
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      visible: false,
      y: 0
    }
  }

  onNavigatorEvent(event) {
    if(event.id === 'bottomTabSelected') {
      let selectedTabIndex = event.selectedTabIndex
      this.props.chatEntryaction.selectedIndexChange(selectedTabIndex)
    }
  }

  componentWillMount() {
    this.refreshContact = DeviceEventEmitter.addListener('refreshContact', () => {
      this.fetchAll()
    })
  }

  componentDidMount() {
    this.fetchAll()
  }

  componentWillUnmount() {
    this.refreshContact && this.refreshContact.remove()
  }

  fetchAll() {
    this.startFetchContact()
    this.startFetchGroup()
    this.startFetchBlackList()
  }

  startFetchContact() {
    const id = this.props.user.id
    this.props.relationshipAction.startFetchContact(id)
  }

  startFetchGroup() {
    const id = this.props.user.id
    this.props.relationshipAction.startFetchGroup(id)
  }

  startFetchBlackList() {
    const id = this.props.user.id
    this.props.relationshipAction.startFetchBlackList(id)
  }

  async openNetWorkActivity() {
    try {
      await MyMapIntentModule.openNetWorkActivity()
    }catch(error) {
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  toggleExpand(type, divideId) {
    this.props.relationshipAction.toggleExpand(type, divideId)
  }

  refreshRelation() {
    this.startFetchContact()
    this.startFetchGroup()
    this.startFetchBlackList()
  }

  jumpToDivideManage() {
    const {contact} = this.props.relationship
    const id = this.props.user.id
    const themeColor = this.props.themeColor
    this.props.navigator.showModal({
      screen: 'jianliao.DivideManageScreen',
      title: '分组管理',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ],
        rightButtons: [
          {
            id: 'done',
            title: '完成',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        id,
        themeColor
      }
    })
  }

  jumpToSearchPage(type) {
    const title = type === 'contact' ? '搜索联系人' : '搜索群组'
    const myId = this.props.user.id
    const themeColor = this.props.themeColor
    this.props.navigator.showModal({
      screen: 'jianliao.SearchContactOrGroupScreen',
      title,
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        myId,
        type,
        themeColor
      }
    })
  }

  jumpToCreateGroupPage() {
    const themeColor = this.props.themeColor
    const user = this.props.user
    this.props.navigator.showModal({
      screen: 'jianliao.CreateGroupScreen',
      title: '创建群组',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        user,
        themeColor
      }
    })
  }

  renderRightIcon() {
    const netStatus = this.props.chatEntry.netStatus.status
    if(netStatus === connectStatus.SCHEDULE || netStatus === connectStatus.RETRY) {
      return <Image source={require('../images/loading.gif')} style={styles.errorRightIcon}/>
    }else if(netStatus !== connectStatus.RECONNECT && netStatus !== connectStatus.ONLINE){
      return (
        <TouchableOpacity onPress={() => this.openNetWorkActivity()}>
          <Image source={require('../images/right_arrow.png')} style={styles.errorRightIcon}/>
        </TouchableOpacity>
      )
    }
    return null
  }

  renderRelation(relation, type) {
    const {themeColor, navigator} = this.props
    let relationViews = []
    relation.forEach((item, index) => {
      relationViews.push(
        <RelationCell
          key={item.name}
          themeColor={themeColor}
          refreshing={true}
          divide={item}
          type={type}
          navigator={navigator}
          onLongPress={y => {
            this.setState({
              visible: true,
              y
            })
          }}
          onPressTitle={() => this.toggleExpand(type, item.name)}/>
      )
    })
    return relationViews
  }

  render() {
    const themeColor = this.props.themeColor
    const {contact, group, blackList} = this.props.relationship
    const netStatus = this.props.chatEntry.netStatus
    const loading = this.props.loading
    return (
      <ScrollView
        overScrollMode='always'
        style={styles.container}
        showsVerticalScrollIndicator={true}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            colors={[themeColor]}
            onRefresh={() => this.refreshRelation()}/>
        }
        contentContainerStyle={{'alignItems': 'center'}}>
        <NetError
          netStatus={netStatus}
          containerStyle={{paddingLeft: 20, backgroundColor: themeColor}}
          textStyle={{fontSize: 14}}
          rightIcon={this.renderRightIcon()}/>
        <View style={styles.addWrapper}>
          <TouchableOpacity
            style={styles.addItem}
            onPress={() => this.jumpToSearchPage('contact')}>
            <Image
              style={[styles.addIcon, {tintColor: themeColor}]}
              source={require('../images/addContact.png')}/>
            <Text style={{color: themeColor}}>添加联系人</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.addItem}
            onPress={() => this.jumpToCreateGroupPage()}>
            <Image
              style={[styles.addIcon, {tintColor: themeColor}]}
              source={require('../images/createGroup.png')}/>
            <Text style={{color: themeColor}}>创建群组</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.addItem}
            onPress={() => this.jumpToSearchPage('group')}>
            <Image
              style={[styles.addIcon, {tintColor: themeColor}]}
              source={require('../images/joinGroup.png')}/>
            <Text style={{color: themeColor}}>加入群组</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.relationWrapper}>
          {
            contact.length > 0 ?
              this.renderRelation(contact, 'contact')
            :
              null
          }
          {
            group.name ?
              this.renderRelation([group], 'group')
            :
              null
          }
          {
            blackList.name ?
              this.renderRelation([blackList], 'blackList')
            :
              null
          }
        </View>
        <DivideMenu
          visible={this.state.visible}
          y={this.state.y}
          content={
            <TouchableOpacity
              style={{alignItems: 'center', justifyContent: 'center', height: '100%'}}
              onPress={() => {
                this.setState({
                  visible: false
                })
                this.jumpToDivideManage()
              }}>
              <Text style={{color: '#fff'}}>分组管理</Text>
            </TouchableOpacity>
          }
          onClose={() => {
            this.setState({
              visible: false
            })
          }}/>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eee'
  },
  errorRightIcon: {
    width: 20,
    height: 20,
    tintColor: '#fff'
  },
  addWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: getScreenSize().width,
    height: 100,
    marginTop: 20,
    backgroundColor: '#fff'
  },
  addItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  addIcon: {
    width: 60,
    height: 60
  },
  relationWrapper: {
    width: getScreenSize().width,
    marginTop: 10,
    backgroundColor: '#fff',
    paddingBottom: 10
  }
})

export default connect(state => ({
  chatEntry: state.chatEntry,
  user: state.user.user,
  loading: state.common.loading,
  themeColor: state.common.themeColor,
  relationship: state.relationship
}), dispatch => ({
  chatEntryaction: bindActionCreators(chatEntryActionCreator, dispatch),
  userAction: bindActionCreators(userActionCreator, dispatch),
  relationshipAction: bindActionCreators(relationshipActionCreator, dispatch)
}))(ContactScreen)
