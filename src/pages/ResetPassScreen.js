import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
  TouchableHighlight,
  Image
} from 'react-native';
import {getScreenSize} from '../utils/common';
import {formatLeancloudErrorMsg} from '../utils/format';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import alertErrorText from '../config/alert';
import {postResetPassEmail} from '../service/common';
import notifyUtil from '../utils/notifyUtil';
import LoadingIndicator from '../components/LoadingIndicator';
import {checkUserVerifyPhoneNumebr} from '../service/user';
import errorTranslate from '../utils/leancloudErrorMsg';

export default class ResetPassScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      target: '',
      way: 'phone'
    }
  }

  async onPressSubmit() {
    if(this.state.way === 'phone') {
      if(!this.state.target.replace(/\s/g, '')) {
        notifyUtil.toast(alertErrorText.registerErrorPhoneEmpty, 'long')
        return
      }

      if(!/\d{11}/.test(this.state.target) || !/^1[34578]\d{9}$/.test(this.state.target)) {
        notifyUtil.toast(alertErrorText.registerErrorPhoneNotValidate, 'long')
        return
      }

      this.setState({loading: true})

      try {
        const result = await checkUserVerifyPhoneNumebr(this.state.target)
        const mobilePhoneVerified = result[0].attributes.mobilePhoneVerified
        if(!mobilePhoneVerified) {
          this.setState({loading: false})
          notifyUtil.toast(alertErrorText.registerErrorNotVerify, 'long')
          return
        }

        this.setState({loading: false})

        this.props.navigator.push({
          screen: 'jianliao.ResetPassConfirmScreen',
          title: '手机号码重置密码',
          navigatorStyle: {
            navBarTitleTextCentered: true,
            navBarBackgroundColor: this.props.themeColor,
            navBarTextColor: '#fff',
            navBarButtonColor: '#fff'
          },
          passProps: {
            showCaptcha: true,
            phoneNumber: this.state.target,
            themeColor: this.props.themeColor
          }
        })
      }catch(error) {
        this.setState({loading: false})
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }else if(this.state.way === 'email') {
      if(!this.state.target.replace(/\s/g, '')) {
        notifyUtil.toast(alertErrorText.registerErrorEmailEmpty, 'long')
        return
      }

      if(!/^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(this.state.target)) {
        notifyUtil.toast(alertErrorText.registerErrorEmailNotValidate, 'long')
        return
      }

      this.setState({loading: true})

      try {
        await postResetPassEmail(this.state.target)
        this.setState({loading: false})
        notifyUtil.toast('发送邮件成功', 'long')
        this.props.navigator.resetTo({
          screen: 'jianliao.LoginScreen',
          navigatorStyle: {
            navBarHidden: true
          }
        })
      }catch(error) {
        this.setState({loading: false})
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={[styles.title, {color: this.props.themeColor}]}>以下方式选其一种</Text>
        <View style={styles.wayItem}>
          <TouchableOpacity onPress={() =>
              this.setState({
                way: 'phone'
              })}>
            <Image
              source={require('../images/radio.png')}
              style={[styles.radioButton, this.state.way === 'phone' ? {tintColor: this.props.themeColor} : {tintColor: '#999'}]}/>
          </TouchableOpacity>
          <View>
            <InputComponent
              onChangeText={text =>
                this.setState({
                  target: text
                })}
              placeholder='请输入手机号码'
              placeholderTextColor={this.props.themeColor}
              keyboardType='numeric'
              maxLength={11}
              editable={this.state.way === 'phone'}
              style={{width: getScreenSize().width * 0.7, marginLeft: 5, borderColor: this.props.themeColor}}/>
          </View>
        </View>
        <View style={[styles.wayItem, {marginTop: 10}]}>
          <TouchableOpacity onPress={() => this.setState({
            way: 'email'
          })}>
            <Image
              source={require('../images/radio.png')}
              style={[styles.radioButton, this.state.way === 'email' ? {tintColor: this.props.themeColor} : {tintColor: '#999'}]}/>
          </TouchableOpacity>
          <View>
            <InputComponent
              placeholder='请输入电子邮箱'
              placeholderTextColor={this.props.themeColor}
              keyboardType='email-address'
              editable={this.state.way === 'email'}
              style={{width: getScreenSize().width * 0.7, marginLeft: 5, borderColor: this.props.themeColor}}
              onChangeText={text => this.setState({
                target: text
              })}/>
          </View>
        </View>
        <ButtonComponent
          title='确 定'
          buttonStyle={{backgroundColor: this.props.themeColor}}
          onPressButton={() => this.onPressSubmit()}/>
        <LoadingIndicator
          visible={this.state.loading}
          color={this.props.themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: getScreenSize().width * 0.8,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 20
  },
  title: {
    marginTop: 30,
    fontSize: 16,
    marginBottom: 10
  },
  radioButton: {
    width: 30,
    height: 30,
    marginTop: 5
  },
  wayItem: {
    flexDirection: 'row',
    alignItems: 'center'
  }
})
