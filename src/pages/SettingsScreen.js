import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Modal,
  TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActionCreator from '../store/actions/user';
import commonStyle from '../styles/common';
import PersonalPageCell from '../components/PersonalPageCell';
import {Switch} from 'react-native-switch';
import LoadingIndicator from '../components/LoadingIndicator';
import clear from 'react-native-clear-cache';
import notifyUtil from '../utils/notifyUtil';
import {getScreenSize} from '../utils/common';
import config from '../config';

class SettingsScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      cacheSize: '',
      modalVisible: false
    }
  }

  componentDidMount() {
    this.getTotalCacheSize()
  }

  setOnlyCurrentDeviceLogin() {
    const newStatus = !this.props.onlyCurrentDeviceLogin
    const username = this.props.username
    this.props.userAction.startUpdateInfo({
      username,
      type: 'onlyCurrentDeviceLogin',
      info: newStatus
    })
  }

  setCanBeSearched() {
    const newStatus = !this.props.canBeSearched
    const username = this.props.username
    this.props.userAction.startUpdateInfo({
      username,
      type: 'canBeSearched',
      info: newStatus
    })
  }

  cantactMe() {
    this.setState({
      modalVisible: true
    })
  }

  cleanCache() {
    clear.runClearCache(() => {
      notifyUtil.toast('清理成功')
      clear.getCacheSize((value,unit) => {
        this.setState({
          cacheSize:value + unit
        })
      })
    })
  }

  jumpToFeedbackPage() {
    this.props.navigator.push({
      screen: 'jianliao.FeedbackScreen',
      title: '意见反馈',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor: this.props.themeColor
      }
    })
  }

  jumpToAboutPage() {
    this.props.navigator.push({
      screen: 'jianliao.AboutScreen',
      title: '关于',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor: this.props.themeColor
      }
    })
  }

  jumpToModifyThemePage() {
    this.props.navigator.push({
      screen: 'jianliao.ModifyThemeScreen',
      title: '修改主题',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff',
        navBarButtonColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ],
        rightButtons: [
          {
            id: 'modifyTheme',
            title: '确定'
          }
        ]
      }
    })
  }

  getTotalCacheSize() {
    clear.getCacheSize((value,unit)=>{
      this.setState({
        cacheSize: value + unit
      })
    })
  }

  renderContactModal() {
    return (
      <View style={styles.contactContent}>
        <Text style={[styles.contactTitle, {color: this.props.themeColor}]}>联系方式</Text>
        <Text style={[styles.contact, {color: this.props.themeColor}]}>QQ:573071547</Text>
        <Text style={[styles.contact, {color: this.props.themeColor}]}>手机号:17671012917</Text>
        <Text style={[styles.contact, {color: this.props.themeColor}]}>微信:humenqiao2016</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerWrapper}>
          <PersonalPageCell
            style={{paddingRight: 5}}
            title='只在当前设备登录'
            titleStyle={{color: this.props.themeColor}}
            style={{borderBottomColor: this.props.themeColor}}
            rightIcon={
              <Switch
                value={this.props.onlyCurrentDeviceLogin}
                circleSize={35}
                onValueChange={val => this.setOnlyCurrentDeviceLogin()}
                disabled={false}
                activeText='开'
                inActiveText='关'
                backgroundActive={this.props.themeColor}
                backgroundInactive='#999'
                circleActiveColor='#fff'
                circleInActiveColor='#fff'/>
            }/>
          <PersonalPageCell
            style={{paddingRight: 5}}
            title='其他人能否查找你'
            titleStyle={{color: this.props.themeColor}}
            style={{borderBottomColor: this.props.themeColor}}
            rightIcon={
              <Switch
                value={this.props.canBeSearched}
                circleSize={35}
                onValueChange={val => this.setCanBeSearched()}
                disabled={false}
                activeText='开'
                inActiveText='关'
                backgroundActive={this.props.themeColor}
                backgroundInactive='#999'
                circleActiveColor='#fff'
                circleInActiveColor='#fff'/>
            }/>
          <PersonalPageCell
            title='清理缓存'
            onPressCell={() => this.cleanCache()}
            content={this.state.cacheSize}
            contentStyle={{color: this.props.themeColor}}
            titleStyle={{color: this.props.themeColor}}
            style={{borderBottomColor: this.props.themeColor}}
            rightIcon={
              this.state.cacheSize !== ''?
                <Image
                  source={require('../images/right_arrow.png')}
                  style={[styles.rightIcon, {tintColor: this.props.themeColor}]}/>
              :
                <Image
                  spurce={require('../images/loading.gif')}
                  style={commonStyle.loading}/>
            }/>
            <PersonalPageCell
              title='意见反馈'
              onPressCell={() => this.jumpToFeedbackPage()}
              titleStyle={{color: this.props.themeColor}}
              style={{borderBottomColor: this.props.themeColor}}
              rightIcon={
                <Image
                  source={require('../images/right_arrow.png')}
                  style={[styles.rightIcon, {tintColor: this.props.themeColor}]}/>
              }/>
            <PersonalPageCell
              title='修改主题'
              onPressCell={() => this.jumpToModifyThemePage()}
              titleStyle={{color: this.props.themeColor}}
              style={{borderBottomColor: this.props.themeColor}}
              rightIcon={
                <Image
                  source={require('../images/right_arrow.png')}
                  style={[styles.rightIcon, {tintColor: this.props.themeColor}]}/>
              }/>
            <PersonalPageCell
              title='关于'
              onPressCell={() => this.jumpToAboutPage()}
              titleStyle={{color: this.props.themeColor}}
              style={{borderBottomColor: this.props.themeColor}}
              rightIcon={
                <Image
                  source={require('../images/right_arrow.png')}
                  style={[styles.rightIcon, {tintColor: this.props.themeColor}]}/>
              }/>
            <PersonalPageCell
              title='联系我'
              titleStyle={{color: this.props.themeColor}}
              style={{borderBottomColor: this.props.themeColor}}
              onPressCell={() => this.cantactMe()}/>
        </View>
        <LoadingIndicator
          visible={this.props.loading}
          color={this.props.themeColor}/>
        <Modal
          animationType='fade'
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => this.setState({modalVisible: false})}>
            <TouchableOpacity
              style={styles.contactModal}
              onPress={() => this.setState({modalVisible: false})}>
                {
                  this.renderContactModal()
                }
            </TouchableOpacity>
        </Modal>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    width: getScreenSize().width,
    paddingHorizontal: 10,
    backgroundColor: '#fff'
  },
  innerWrapper: {
    width: '100%'
  },
  rightIcon: {
    width: 30,
    height: 30
  },
  contactModal: {
    flex: 1,
    backgroundColor: '#000',
    opacity: 0.8,
    justifyContent: 'center',
    alignItems: 'center'
  },
  contactContent: {
    backgroundColor: '#fff',
    width: 300,
    height: 150,
    borderRadius: 3,
    padding: 10
  },
  contactTitle: {
    fontSize: 18,
    textAlign: 'center',
    color: '#000'
  },
  contact: {
    color: '#000',
    fontSize: 16,
    marginTop: 5
  },
  updateContainer: {
    width: getScreenSize().width * 0.9,
    height: 200,
    backgroundColor: '#fff',
    borderRadius: 3,
    padding: 10
  }
})

export default connect(state => ({
  onlyCurrentDeviceLogin: state.user.user.onlyCurrentDeviceLogin,
  username:state.user.user.username,
  loading: state.common.loading,
  canBeSearched: state.user.user.canBeSearched,
  themeColor: state.common.themeColor
}), dispatch => ({
  userAction: bindActionCreators(userActionCreator, dispatch)
}))(SettingsScreen)
