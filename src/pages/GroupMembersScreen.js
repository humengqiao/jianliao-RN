import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as conversationActionCreator from '../store/actions/conversation';
import * as conversationTypes from '../config/conversationType';
import commonStyle from '../styles/common';
import {getScreenSize, getPixel} from '../utils/common';
import {fetchGroupMembers} from '../service/relationship';
import {doGetuser} from '../service/user';
import Swipeout from 'react-native-swipeout';
import notifyUtil from '../utils/notifyUtil';
import Empty from '../components/Empty';
import {CustomCachedImage} from 'react-native-img-cache';
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
import {runCloudFn} from '../service/common';
import {sendKickGroupMessage} from '../service/sendMessage';
import LoadingIndicator from '../components/LoadingIndicator';

class GroupMembersScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      creator: null,
      members: []
    }
  }

  componentWillMount() {
    this.fetchGroupCreator()
    this.fetchGroupMemebers()
  }

  async fetchGroupCreator() {
    const creator = this.props.group.creator
    this.canSwipe = creator === this.props.username
    try {
      const result = await doGetuser('username', creator, ['username', 'nick', 'mobilePhoneNumber', 'email', 'canBeSearched', 'avatar', 'gender', 'birth'])
      const creatorInfo = result.toJSON()
      this.setState({
        creator: {...creatorInfo}
      })
    }catch(error) {
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  async fetchGroupMemebers() {
    const {members: memberIds, creator} = this.props.group
    try {
      const creatorResult = await doGetuser('username', creator, ['id'])
      const creatorId = creatorResult.toJSON().objectId
      const result = await fetchGroupMembers(memberIds, creatorId)
      if(result.length > 0) {
        const members = result.map(item => item.toJSON())
        this.setState({
          members: [...members]
        })
      }
    }catch(error) {
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  jumpToUserInfoPage(user) {
    const themeColor = this.props.themeColor
    const username = this.props.username
    const isMe = user.username === username ? true : false
    const inMyContact = this.props.contact.some(item => {
      return item.list.findIndex(item => item.username === user.username) !== -1
    })

    this.props.navigator.push({
      screen: 'jianliao.ContactInfoDetailScreen',
      title: '资料详情',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor,
        contact: user,
        canSendMessage: inMyContact,
        isMe,
        origin: '群组'
      }
    })
  }

  async removeMember(member) {
    this.setState({loading: true})
    try {
      const _groupId = this.props.group.objectId
      const _userId = member.objectId
      const senderUserNick = this.props.user.nick
      const {groupId, nick: groupName} = this.props.group
      const targetUserNick = member.nick
      await runCloudFn('kickGroupMember', {
        userId: _userId,
        groupId: _groupId
      })

      const conversation = await global.client.createConversation({
        members: [member.username],
        name: '',
        transient: false,
        unique: false,
        type: conversationTypes.GROUP_NOTIFY,
        groupId
      })

      await sendKickGroupMessage(conversation, {
        senderUserNick,
        groupName,
        targetUserNick
      })

      notifyUtil.toast('删除群成员成功')
      this.setState({loading: false})
      this.props.conversationAction.modifyConversation(conversation.toFullJSON())
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  renderCreator() {
    const themeColor = this.props.themeColor
    const creator = this.state.creator
    const username = this.props.username
    return (
      this.state.creator ?
        <View style={styles.creator}>
          <View style={[styles.sep, {backgroundColor:themeColor}]}>
            <Text style={{color: '#fff'}}>创建者</Text>
          </View>
          <TouchableOpacity
            style={styles.commonContent}
            onPress={() => this.jumpToUserInfoPage(creator)}>
            <View style={styles.content}>
              <Image
                source={{uri: creator.avatar.url}}
                style={styles.avatar}/>
              <Text style={[styles.nick, {color: themeColor}]}>{creator.nick}</Text>
            </View>
            {
              this.state.creator.username === username ?
                <Text style={[styles.me, {color: themeColor}]}>我</Text>
              :
                null
            }
          </TouchableOpacity>
        </View>
      :
        null
    )
  }

  renderMember(member) {
    const {username, themeColor} = this.props
    return (
      <Swipeout
        right={[
          {text: '删除', backgroundColor: '#ff0000', type: 'delete', underlayColor: '#999', onPress: () => this.removeMember(member)}
        ]}
        backgroundColor='#fff'
        buttonWidth={80}
        autoClose={true}
        close={true}
        disabled={!this.canSwipe}>
        <TouchableOpacity
          style={[styles.memberContent, styles.commonContent, {borderBottomColor: themeColor}]}
          onPress={() => this.jumpToUserInfoPage(member)}>
          <View style={styles.content}>
            <CustomCachedImage
              component={CustomImage}
              source={{uri: member.avatar.url}}
              indicator={Pie}
              style={styles.avatar}
              indicatorProps={{
                size: 80,
                borderWidth: 0,
                color: themeColor,
                unfilledColor: '#999'
            }}/>
            <Text style={[styles.nick, {color: themeColor}]}>{member.nick}</Text>
          </View>
          {
            member.username === username ?
              <Text style={[styles.me, {color: themeColor}]}>我</Text>
            :
              null
          }
        </TouchableOpacity>
      </Swipeout>
    )
  }

  render() {
    const themeColor = this.props.themeColor
    return (
      <View style={styles.container}>
        {
          this.renderCreator()
        }
        <View style={styles.members}>
          <View style={[styles.sep, {backgroundColor: themeColor}]}>
            <Text style={{color: '#fff'}}>成员 ({this.state.members.length})</Text>
          </View>
          <FlatList
            data={this.state.members}
            keyExtractor={(item, index) => index}
            ListEmptyComponent={<Empty title='该群组暂无成员' themeColor={themeColor}/>}
            renderItem={({item}) => this.renderMember(item)}/>
          <LoadingIndicator
            visible={this.state.loading}
            color={themeColor}/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container
  },
  creator: {
    width: getScreenSize().width
  },
  members: {
    width: getScreenSize().width
  },
  sep: {
    height: 30,
    justifyContent: 'center',
    paddingLeft: 20
  },
  commonContent: {
    paddingVertical: 10,
    flexDirection: 'row',
    paddingLeft: 10,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  memberContent: {
    borderBottomWidth: getPixel() * 3
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30
  },
  nick: {
    marginLeft: 20,
    fontSize: 18
  },
  me: {
    marginRight: 20
  }
})

export default connect(state => ({
  contact: state.relationship.contact,
  user: state.user.user
}), dispatch => ({
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(GroupMembersScreen)
