import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import commonStyle from '../styles/common';
import InputComponent from '../components/InputComponent';
import LoadingIndicator from '../components/LoadingIndicator';
import {fetchConversationOrCreate} from '../service/conversation';
import * as conversationTypes from '../config/conversationType';
import * as conversationActionCreator from '../store/actions/conversation';
import {getScreenSize} from '../utils/common';
import notifyUtil from '../utils/notifyUtil';
import {sendAddContactMessage, sendAddGroupMessage} from '../service/sendMessage';

class AddUserScreen extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      loading: false,
      reason: ''
    }
  }

  onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress' && event.id === 'send') {
      if(this.props.type === 'contact') {
        this.doSendAddUserMessage()
      }else if(this.props.type === 'group') {
        this.doSendAddGroupMessage()
      }
    }
  }

  async doSendAddGroupMessage() {
    try {
      const {creator, groupId, nick: groupName} = this.props.group
      const origin = this.props.origin
      const reason = this.state.reason
      const senderUserNick = this.props.me.nick
      this.setState({loading: true})
      const conversation = await global.client.createConversation({
        members: [creator],
        name: '',
        transient: false,
        unique: false,
        type: conversationTypes.GROUP_NOTIFY,
        groupId
      })
      await sendAddGroupMessage(conversation, creator, groupId, {
        senderUserNick,
        groupName,
        reason,
        origin
      })
      this.setState({loading: false})
      notifyUtil.toast('发送添加申请成功')
      this.props.conversationAction.modifyConversation(conversation.toFullJSON())
      this.props.navigator.pop()
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  async doSendAddUserMessage() {
    const myUsername = this.props.me.username
    const targetUsername = this.props.contact.username
    const reason = this.state.reason
    const senderUserNick = this.props.me.nick
    const origin = this.props.origin
    try {
      this.setState({loading: true})
      const conversation = await fetchConversationOrCreate([myUsername, targetUsername], '', conversationTypes.CONTACT_NOTIFY)
      await sendAddContactMessage(conversation, {reason, senderUserNick, origin})
      this.setState({loading: false})
      notifyUtil.toast('发送申请成功')
      this.props.conversationAction.startRemoveConversation(conversation, myUsername, false)
      this.props.conversationAction.modifyConversation(conversation.toFullJSON())
      this.props.navigator.pop()
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  render() {
    const themeColor = this.props.themeColor
    return(
      <View style={styles.container}>
        <InputComponent
          placeholder='输入添加申请说明'
          style={[styles.input, {borderBottomColor: themeColor}]}
          multiline={true}
          numberOfLines={5}
          inputBarStyle={{textAlignVertical: 'top'}}
          autoFocus={true}
          placeholderTextColor={themeColor}
          onChangeText={value => this.setState({reason: value})}/>
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container
  },
  input: {
    width: getScreenSize().width - 20,
    height: 100
  }
})

export default connect(state => ({
  me: state.user.user
}), dispatch => ({
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(AddUserScreen)
