import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {startUpdateInfo} from '../store/actions/user';
import {getScreenSize} from '../utils/common';
import {formatLeancloudErrorMsg} from '../utils/format';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import {getCaptcha, checkCaptcha, getCheckCode, verifyCode} from '../service/common';
import notifyUtil from '../utils/notifyUtil';
import LoadingIndicator from '../components/LoadingIndicator';
import {postVerifyEmail} from '../service/common';
import errorTranslate from '../utils/leancloudErrorMsg';

class VerifyScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      code: '',
      captcha: '',
      captchaUrl: ''
    }
  }

  componentDidMount() {
    this.showCaptcha()
  }

  async showCaptcha() {
    if(this.props.showCaptcha) {
      try {
        this.captcha = await getCaptcha(100, 50)
        this.setState({
          captchaUrl: this.captcha.url
        })
      }catch(error) {
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  async verifyPhoneNumber() {
    if(!this.state.code.replace(/\s/g, '')) {
      notifyUtil.toast('再看看，少了什么？')
      return
    }
    this.setState({
      loading: true
    })

    try {
      await verifyCode(this.state.code, true)
      this.setState({
        loading: false
      })

      notifyUtil.toast('验证成功')
      this.props.navigator.dismissModal({
        animationType: 'slide-down'
      })
      if(this.props.from === 'RegisterScreen') {
        this.props.navigator.resetTo({
          screen: 'jianliao.LoginScreen',
          animated: true,
          animationType: 'slide-horizontal',
          navigatorStyle: {
            navBarHidden: true
          }
        })
      }else if(this.props.from === 'MeScreen') {
        this.props.navigator.pop()
        const username = this.props.username
        this.props.startUpdateInfo({
          username,
          type: 'mobilePhoneVerified',
          info: true
        })
      }
    }catch(error) {
      this.setState({
        loading: false
      })
      notifyUtil.toast(errorTranslate(error), 'long')
    }
  }

  async reSendCode() {
    if(this.props.showCaptcha) {
      if(!this.state.captcha.replace(/\s/g, '')) {
        notifyUtil.toast('别调皮，你忘了输入图形验证码了', 'long')
        return
      }
      if(!/.{4}/.test(this.state.captcha)) {
        notifyUtil.toast('图形验证码只能是4位哦', 'long')
        return
      }
      this.setState({loading: true})

      try {
        await checkCaptcha(this.captcha, this.state.captcha)
        this.setState({loading: false})
        const result = await getCheckCode(this.props.numberOrEmail, true)
        notifyUtil.toast('发送成功')
      }catch(error) {
        this.setState({loading: false})
        this.showCaptcha()
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }else if(this.props.type === 'verifyPhone') {
      this.setState({loading: true})
      try {
        await getCheckCode(this.props.numberOrEmail)
        this.setState({loading: false})
        notifyUtil.toast('发送成功')
      }catch(error) {
        this.setState({loading: false})
        this.showCaptcha()
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  renderCaptcha() {
    if(this.props.showCaptcha) {
      return (
        <View>
          <View style={styles.captchaWrapper}>
            {
              this.state.captchaUrl ?
                <Image
                  source={{uri: this.state.captchaUrl}}
                  style={styles.captcha}/>
                :
                  null
            }
            <TouchableOpacity onPress={() => this.showCaptcha()}>
              <Text style={{fontSize: 16, marginLeft: 10, color: this.props.themeColor}}>看不清楚？</Text>
            </TouchableOpacity>
          </View>
          <InputComponent
            placeholder='输入图形验证码(重发短信时)'
            keyboardType='email-address'
            maxLength={4}
            placeholderTextColor={this.props.themeColor}
            style={{borderColor: this.props.themeColor}}
            onChangeText={captcha => {
              this.setState({
                captcha
              })
            }}/>
        </View>
      )
    }
  }

  renderContent() {
      return (
        <View>
          {this.renderCaptcha()}
          <View>
            <InputComponent
              placeholder={this.props.type === 'verifyPhone' ? '输入短信验证码' : ''}
              keyboardType='email-address'
              autoFocus={true}
              placeholderTextColor={this.props.themeColor}
              style={{borderColor: this.props.themeColor}}
              onChangeText={code => {
                this.setState({
                  code
                })
              }}
              rightElement={() =>
                <TouchableOpacity onPress={() => this.reSendCode()}>
                  <Text style={{fontSize: 14, color: this.props.themeColor}}>没收到？</Text>
                </TouchableOpacity>
              }/>
            <ButtonComponent
              onPressButton={() => this.verifyPhoneNumber()}
              buttonStyle={{backgroundColor: this.props.themeColor}}
              title='确定'/>
          </View>
        </View>
      )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderContent()}
        <LoadingIndicator
          visible={this.state.loading}
          color={this.props.themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: getScreenSize().width * 0.8,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 80
  },
  captchaWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  captcha: {
    width: 100,
    height: 50
  }
})

export default connect(state => ({
  username: state.user.user.username
}), dispatch => ({
  startUpdateInfo: bindActionCreators(startUpdateInfo, dispatch)
}))(VerifyScreen)
