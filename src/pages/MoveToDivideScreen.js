import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import commonStyle from '../styles/common';
import notifyUtil from '../utils/notifyUtil';
import LoadingIndicator from '../components/LoadingIndicator';
import {doGetuser} from '../service/user';
import {getPixel} from '../utils/common';
import {fetchContactList} from '../service/relationship';
import {startModifyContact} from '../store/actions/relationship';

class MoveToDivideScreen extends Component {
  constructor(props) {
    super(props)
    const divideName = this.props.divideName
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      loading: true,
      currentDivide: divideName,
      divide: []
    }
  }

  onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress' && event.id === 'done') {
      this.saveContactChange()
    }
  }

  componentWillMount() {
    this.fetchDivideList()
  }

  saveContactChange() {
    if(this.state.currentDivide === this.props.divideName) {
      this.props.navigator.pop()
      return
    }

    const id = this.props.myId
    const localDivide = this.state.divide
    const cloudDivide = localDivide.map(item => {
      const newDivide = {
        ...item,
        list: item.list.map(item => item.objectId)
      }
      return newDivide
    })
    this.props.modifyContactAction(id, cloudDivide, localDivide)
  }

  async fetchDivideList() {
    const id = this.props.myId
    try {
      const result = await doGetuser('_id', id, ['contact'])
      const {contact} = result.toJSON()
      const wrappedContact = contact.map(item => {
        return fetchContactList(item)
      })
      const divide = await Promise.all(wrappedContact)
      this.setState({
        divide,
        loading: false
      })
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  selectDivide(divide) {
    const divideName = this.state.currentDivide
    const user = this.props.user
    if(divideName === divide.name) {
      return
    }
    
    const newDivide = this.state.divide.map(item => {
      if(item.name === divideName) {
        const index = item.list.findIndex(item => item.username === user.username)
        if(index !== -1) {
          item.list.splice(index, 1)
        }
      }else if(item.name === divide.name) {
        item.list.unshift(user)
      }
      return item
    })
    this.setState({
      currentDivide: divide.name,
      divide: newDivide
    })
  }

  renderDivide() {
    const themeColor = this.props.themeColor
    let divideViews = this.state.divide.map((item, index) => {
      return (
        <TouchableOpacity
          key={index}
          style={[styles.divideItem, {borderBottomColor: themeColor}]}
          onPress={() => this.selectDivide(item)}>
          <Text style={[styles.divideLabel, {color: themeColor}]}>{item.label} ({(item.list.length)}人)</Text>
          {
            this.state.currentDivide === item.name ?
              <Image
                source={require('../images/correct.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            :
              null
          }
        </TouchableOpacity>
      )
    })
    return divideViews
  }

  render() {
    const {themeColor} = this.props
    return (
      this.state.divide.length > 0 ?
        <View style={styles.container}>
          <ScrollView>
            {
              this.renderDivide()
            }
          </ScrollView>
        </View>
      :
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    marginTop: 10
  },
  divideItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    borderBottomWidth: getPixel() * 5,
    paddingHorizontal: 10
  },
  divideLabel: {
    fontSize: 16
  }
})

export default connect(null, dispatch => ({
  modifyContactAction: bindActionCreators(startModifyContact, dispatch)
}))(MoveToDivideScreen)
