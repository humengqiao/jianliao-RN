import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as conversationActionCreator from '../store/actions/conversation';
import commonStyle from '../styles/common';
import Empty from '../components/Empty';
import ConversationCell from '../components/ConversationCell';
import {doGetuser} from '../service/user';
import {getChatAndConversationTime} from '../utils/format';
import config from '../config';
import Swipeout from 'react-native-swipeout';
import * as conversationTypes from '../config/conversationType';
import {filterConversation, getContactMessageTypeContent} from '../utils/conversation';

class ContactNotifyScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notifys: []
    }
  }

  componentWillMount() {
    const {contactNotify, hideConversationList} = this.props
    const filteredContactNotify = filterConversation(contactNotify, hideConversationList)
    this.fetchContactNotifyUserInfo(filteredContactNotify)
  }

  componentWillReceiveProps(nextProps) {
    const {contactNotify, hideConversationList} = nextProps
    const filteredContactNotify = filterConversation(contactNotify, hideConversationList)
    this.fetchContactNotifyUserInfo(filteredContactNotify)
  }

  clearUnreadStatus(notify) {
    this.props.conversationAction.startClearUnreadStatus(notify, conversationTypes.CONTACT_NOTIFY)
  }

  removeConversation(notify) {
    const username = this.props.username
    this.props.conversationAction.startRemoveConversation(notify, username, true)
  }

  async fetchContactNotifyUserInfo(filteredContactNotify) {
    const username= this.props.username
    const wrappedContactNotify = filteredContactNotify.map(item => {
      const target = item.members.filter(item => item !== username)[0]
      if(!target) {
        return
      }
      return new Promise(async resolve => {
        try {
          const result = await doGetuser('username', target, ['username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'canBeSearched', 'birth'])
          const user = result.toJSON()
          resolve({
            ...item,
            user: {
              ...user
            }
          })
        }catch(error) {
          resolve()
        }
      })
    })
    const contactNotifyWithUser = await Promise.all(wrappedContactNotify)
    this.setState({
      notifys: contactNotifyWithUser
    })
  }

  jumpToContactNotifyPageDetail(notify) {
    const themeColor = this.props.themeColor
    this.props.navigator.push({
      screen: 'jianliao.ContactNotifyDetailScreen',
      title: '联系人通知回复',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor,
        notify
      }
    })
  }

  renderCell(notify) {
    const user = notify.user
    const themeColor = this.props.themeColor
    const myUsername = this.props.username
    const creatorUsername = notify.creator
    const {type: messageType, reason, origin} = notify.lastMessage.data._lcattrs
    const lastMessageAt = getChatAndConversationTime(notify.lastMessageAt)
    const messageTypeContent = getContactMessageTypeContent(messageType, creatorUsername, myUsername)
    return (
      <Swipeout
        right={[
          {text: '设为已读', backgroundColor: themeColor, type: 'default', underlayColor: '#999', onPress: () => this.clearUnreadStatus(notify)},
          {text: '删除', backgroundColor: '#ff0000', type: 'delete', underlayColor: '#999', onPress: () => this.removeConversation(notify)}
        ]}
        backgroundColor='#fff'
        buttonWidth={80}
        autoClose={true}
        close={true}>
        <ConversationCell
          key={notify.id}
          title={user.nick}
          onPressCell={() => this.jumpToContactNotifyPageDetail(notify)}
          avatar={{uri: user.avatar.url}}
          content={
            <View>
              <Text style={{color: themeColor}}>
                {reason}
              </Text>
              <Text style={{color: themeColor}}>
                {
                  origin ?
                    `来源：${origin}`
                  :
                    ''
                }
                {
                  messageTypeContent ? `(${messageTypeContent})` : ''
                }
              </Text>
            </View>
          }
          time={lastMessageAt}
          cacheImage={!user.avatar.url === config.defaultAvatar}
          notifyContent={notify.unreadMessagesCount}
          themeColor={themeColor}/>
      </Swipeout>
    )
  }

  render() {
    const themeColor = this.props.themeColor
    const notifys = this.state.notifys
    return (
      <View style={styles.container}>
        <FlatList
          data={notifys}
          keyExtractor={(item, index) => index}
          ListEmptyComponent={<Empty title='暂无联系人通知' titleStyle={commonStyle.empty} themeColor={themeColor}/>}
          renderItem={({item}) => this.renderCell(item)}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container
  }
})

export default connect(state => ({
  contactNotify: state.conversation.contactNotify,
  hideConversationList: state.conversation.hideConversationList
}), dispatch => ({
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(ContactNotifyScreen)
