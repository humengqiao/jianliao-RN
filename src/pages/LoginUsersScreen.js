import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActionCreator from '../store/actions/user';
import {getScreenSize} from '../utils/common';
import {CachedImage} from 'react-native-img-cache';

class ModalScreen extends Component {
  constructor(props) {
    super(props)
  }

  onSelecteUser(user) {
    this.props.onSelected(user)
    this.props.navigator.dismissLightBox()
  }

  onRemoveFromUsers(username) {
    Alert.alert(
      '提示',
      '您确定要删除此账号登录记录吗?',
      [
        {text: '确定', onPress: () => this.props.userAction.removeFromUsers(username)},
        {text: '取消', onPress: () => {}, style: 'cancel'}
      ],
      {cancelable: true})
  }

  renderUsers() {
    let usersView = []

    this.props.loginedUsers.forEach(item => {
      usersView.push(
        <View key={item.id}>
          <TouchableOpacity
            style={styles.userItem}
            onPress={() => this.onSelecteUser(item)}>
            <View style={{flexDirection: 'row', alignItems:'center'}}>
              <CachedImage
                source={{uri: item.avatar.url}}
                style={styles.avatar}/>
              <Text style={[styles.username, {color: this.props.themeColor}]}>{item.username}</Text>
            </View>
            <TouchableOpacity onPress={() => this.onRemoveFromUsers(item.username)}>
              <Image
              source={require('../images/delete_icon.png')}
              style={[styles.opIcon, {tintColor: this.props.themeColor}]}/>
            </TouchableOpacity>
          </TouchableOpacity>
          <View style={{height: 2, backgroundColor: this.props.themeColor}}></View>
        </View>
      )
    })
    return usersView
  }

  renderEmpty() {
    return (
      <View style={styles.emptyWrapper}>
        <Text style={styles.emptyText}>暂无用户登录</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.usersWrapper}>
        <ScrollView
          style={styles.scrollWrapper}
          showsVerticalScrollIndicator={true}>
          {this.props.loginedUsers.length > 0 ? this.renderUsers() : this.renderEmpty()}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  usersWrapper: {
    width: getScreenSize().width * 0.8,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  emptyWrapper: {
    backgroundColor: '#fff',
    height: 60,
    borderRadius: 5,
    justifyContent: 'center'
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 18
  },
  userItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 70,
    backgroundColor: '#fff',
    paddingLeft: 10,
    borderRadius: 3,
    alignItems: 'center'
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25
  },
  username: {
    color: '#000',
    marginLeft: 10,
    fontSize: 16
  },
  scrollWrapper: {
    borderRadius: 3,
    backgroundColor: '#fff'
  },
  opIcon: {
    width: 30,
    height: 30,
    tintColor: '#999',
    marginRight: 5
  }
})

export default connect(state => ({
  loginedUsers: state.user.loginedUsers
}), dispatch => ({
  userAction: bindActionCreators(userActionCreator, dispatch)
}))(ModalScreen)
