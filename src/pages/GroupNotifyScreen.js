import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as conversationActionCreator from '../store/actions/conversation';
import commonStyle from '../styles/common';
import Empty from '../components/Empty';
import ConversationCell from '../components/ConversationCell';
import {doGetuser} from '../service/user';
import {doGetgroup} from '../service/group';
import {getChatAndConversationTime} from '../utils/format';
import config from '../config';
import Swipeout from 'react-native-swipeout';
import * as conversationTypes from '../config/conversationType';
import {filterConversation, getGroupMessageTypeContent} from '../utils/conversation';

class ContactNotifyScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notifys: []
    }
  }

  componentWillMount() {
    const {groupNotify, hideConversationList} = this.props
    const filteredGroupNotify = filterConversation(groupNotify, hideConversationList)
    this.fetchGroupNotifyUserInfo(filteredGroupNotify)
  }

  componentWillReceiveProps(nextProps) {
    const {groupNotify, hideConversationList} = nextProps
    const filteredGroupNotify = filterConversation(groupNotify, hideConversationList)
    this.fetchGroupNotifyUserInfo(filteredGroupNotify)
  }

  clearUnreadStatus(notify) {
    this.props.conversationAction.startClearUnreadStatus(notify, conversationTypes.GROUP_NOTIFY)
  }

  removeConversation(notify) {
    const username = this.props.username
    this.props.conversationAction.startRemoveConversation(notify, username, true)
  }

  async fetchGroupNotifyUserInfo(filteredGroupNotify) {
    const username= this.props.username
    const wrappedGroupNotify = filteredGroupNotify.map(item => {
      const target = item.members.filter(item => item !== username)[0]
      if(!target) {
        return
      }
      return new Promise(async resolve => {
        try {
          const result1 = await doGetuser('username', target, ['username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'canBeSearched', 'birth'])
          const result2 = await doGetgroup('groupId', item.groupId, ['nick'])
          const user = result1.toJSON()
          const group = result2.toJSON()
          resolve({
            ...item,
            user: {
              ...user
            },
            group: {
              groupName: group.nick
            }
          })
        }catch(error) {
          resolve()
        }
      })
    })
    const contactGroupWithUser = await Promise.all(wrappedGroupNotify)
    this.setState({
      notifys: contactGroupWithUser
    })
  }

  jumpToContactNotifyPageDetail(notify) {
    const themeColor = this.props.themeColor
    this.props.navigator.push({
      screen: 'jianliao.GroupNotifyDetailScreen',
      title: '群组通知回复',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor,
        notify
      }
    })
  }

  renderCell(notify) {
    const user = notify.user
    const themeColor = this.props.themeColor
    const myUsername = this.props.username
    const creatorUsername = notify.creator
    const {type: messageType, reason, origin} = notify.lastMessage.data._lcattrs
    const lastMessageAt = getChatAndConversationTime(notify.lastMessageAt)
    const messageTypeContent = getGroupMessageTypeContent(messageType, creatorUsername, myUsername)
    return (
      <Swipeout
        right={[
          {text: '设为已读', backgroundColor: themeColor, type: 'default', underlayColor: '#999', onPress: () => this.clearUnreadStatus(notify)},
          {text: '删除', backgroundColor: '#ff0000', type: 'delete', underlayColor: '#999', onPress: () => this.removeConversation(notify)}
        ]}
        backgroundColor='#fff'
        buttonWidth={80}
        autoClose={true}
        close={true}>
        <ConversationCell
          key={notify.id}
          title={user.nick}
          onPressCell={() => this.jumpToContactNotifyPageDetail(notify)}
          avatar={{uri: user.avatar.url}}
          content={
            <View>
              <Text style={{color: themeColor}}>
                {reason}
              </Text>
              <Text style={{color: themeColor}}>
                {
                  origin ?
                    `来源：${origin}`
                  :
                    ''
                }
                {
                  messageTypeContent ? `(${messageTypeContent})` : ''
                }
              </Text>
            </View>
          }
          time={lastMessageAt}
          cacheImage={!user.avatar.url === config.defaultAvatar}
          notifyContent={notify.unreadMessagesCount}
          themeColor={themeColor}/>
      </Swipeout>
    )
  }

  render() {
    const themeColor = this.props.themeColor
    const notifys = this.state.notifys
    return (
      <View style={styles.container}>
        <FlatList
          data={notifys}
          keyExtractor={(item, index) => index}
          ListEmptyComponent={<Empty title='暂无群组通知' titleStyle={commonStyle.empty} themeColor={themeColor}/>}
          renderItem={({item}) => this.renderCell(item)}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container
  }
})

export default connect(state => ({
  groupNotify: state.conversation.groupNotify,
  hideConversationList: state.conversation.hideConversationList
}), dispatch => ({
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(ContactNotifyScreen)
