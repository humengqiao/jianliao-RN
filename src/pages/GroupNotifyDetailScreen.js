import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getScreenSize, getPixel} from '../utils/common';
import commonStyle from '../styles/common';
import {CustomCachedImage} from 'react-native-img-cache';
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
import * as customMessageTypes from '../config/customMessageType';
import * as conversationTypes from '../config/conversationType';
import * as conversationActionCreator from '../store/actions/conversation';
import notifyUtil from '../utils/notifyUtil';
import {sendRefuseGroupMessage, sendAcceptGroupMessage} from '../service/sendMessage';
import ModalComponent from '../components/ModalComponent';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import LoadingIndicator from '../components/LoadingIndicator';
import Empty from '../components/Empty';
import {runCloudFn} from '../service/common';

class GroupNotifyDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      showRefuseConfirm: false,
      reason: ''
    }
  }

  refuseGroupConfirm() {
    this.setState({
      showRefuseConfirm: true
    })
  }

  async refuseGroup() {
    const notify = this.props.notify
    const myUsername = this.props.user.username
    const targetUsername = notify.lastMessage.from
    const reason = this.state.reason
    const senderUserNick = this.props.user.nick
    const groupName = notify.group.groupName
    this.setState({loading: true, showRefuseConfirm: false})
    try {
      const notifyToObject = await global.client.parseConversation(notify)
      const result = await sendRefuseGroupMessage(notifyToObject, {
        reason,
        senderUserNick,
        groupName
      })
      this.setState({loading: false})
      notifyUtil.toast('拒绝成功')
      this.props.conversationAction.modifyConversation(notifyToObject.toFullJSON())
      this.props.conversationAction.startClearUnreadStatus(notify, conversationTypes.GROUP_NOTIFY)
      this.props.navigator.pop()
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  async acceptGroup() {
    const notify = this.props.notify
    const targetUsername = this.props.notify.lastMessage.from
    const senderUserNick = this.props.user.nick
    const targetUserNick = notify.user.nick
    const groupId = notify.groupId
    const groupName = notify.group.groupName
    this.setState({loading: true})
    try {
      //使用云函数执行添加逻辑（客户端有_User表权限问题，云函数可以用masterkey绕过全选（谨慎操作！））
      await runCloudFn('addGroup', {
        targetUsername,
        groupId
      })

      const notifyToObject = await global.client.parseConversation(notify)
      const result = await sendAcceptGroupMessage(notifyToObject, {
        senderUserNick,
        targetUserNick,
        groupName
      })
      this.setState({loading: false})
      notifyUtil.toast('添加成功')
      this.props.conversationAction.modifyConversation(notifyToObject.toFullJSON())
      this.props.conversationAction.startClearUnreadStatus(notify, conversationTypes.GROUP_NOTIFY)
      this.props.navigator.pop()
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  ifSendeFromMe() {
    const fromUsername = this.props.notify.lastMessage.from
    const myUsername = this.props.user.username
    return fromUsername === myUsername
  }

  renderAddGroup() {
    const themeColor = this.props.themeColor
    return (
      this.ifSendeFromMe() ?
        <View style={styles.opWrapper}>
          <Image
            source={require('../images/timing.png')}
            style={{width: 15, height: 15, tintColor: themeColor}}/>
          <Text style={{fontSize: 16, color: themeColor}}>您的申请等待回复</Text>
        </View>
      :
        <View style={styles.opWrapper}>
          <TouchableOpacity
            style={[styles.opButton, {backgroundColor: '#ff0000', marginRight: 5}]}
            onPress={() => this.refuseGroupConfirm()}>
            <Text style={styles.opText}>拒绝</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.opButton, {backgroundColor: themeColor, marginLeft: 5}]}
            onPress={() => this.acceptGroup()}>
            <Text style={styles.opText}>同意</Text>
          </TouchableOpacity>
        </View>
    )
  }

  renderRefuseGroup() {
    const reason = this.props.notify.lastMessage.data._lcattrs.reason
    const themeColor = this.props.themeColor
    return (
      <View style={styles.notifyWrapper}>
        <View style={styles.notifyContentWrapper}>
          <Image
            source={require('../images/sorry.png')}
            style={[styles.notifyPic, {tintColor: themeColor}]}/>
            {
              this.ifSendeFromMe() ?
                <Text style={[styles.notifyContent, {color: themeColor}]}>您已拒绝ta的添加申请</Text>
              :
                <Text style={[styles.notifyContent, {color: themeColor}]}>您的添加申请已被拒绝</Text>
            }
        </View>
        <Text style={{color: themeColor, marginLeft: 20}}>
          理由：
          {
            reason ?
              reason
            :
              '未填写'
          }
        </Text>
      </View>
    )
  }

  renderAcceptGroup() {
    const themeColor = this.props.themeColor
    return (
      <View style={styles.notifyWrapper}>
        <View style={styles.notifyContentWrapper}>
          <Image
            source={require('../images/smile.png')}
            style={[styles.notifyPic, {tintColor: themeColor}]}/>
            {
              this.ifSendeFromMe() ?
                <Text style={[styles.notifyContent, {color: themeColor}]}>您已同意ta的添加申请</Text>
              :
                <Text style={[styles.notifyContent, {color: themeColor}]}>您的添加申请已被通过</Text>
            }
        </View>
      </View>
    )
  }

  renderBeKickedGroup() {
    const themeColor = this.props.themeColor
    const groupName = this.props.notify.lastMessage.data._lcattrs.groupName
    return (
      <View style={styles.notifyWrapper}>
        <View style={styles.notifyContentWrapper}>
          <Image
            source={require('../images/sorry.png')}
            style={[styles.notifyPic, {tintColor: themeColor}]}/>
            {
              this.ifSendeFromMe() ?
                <Text style={[styles.notifyContent, {color: themeColor}]}>您已从{groupName}踢出ta</Text>
              :
                <Text style={[styles.notifyContent, {color: themeColor}]}>您已被踢出{groupName}</Text>
            }
        </View>
      </View>
    )
  }

  renderStatus() {
    const messageType = this.props.notify.lastMessage.data._lcattrs.type
    const themeColor = this.props.themeColor
    switch(messageType) {
      case customMessageTypes.ADD_GROUP:
        return this.renderAddGroup()
        break
      case customMessageTypes.REFUSE_JOINGROUP:
        return this.renderRefuseGroup()
        break
      case customMessageTypes.ACCEPT_JOINGROUP:
        return this.renderAcceptGroup()
        break
      case customMessageTypes.BEKICKED_GROUP:
        return this.renderBeKickedGroup()
        break
      default:
        return (
          <Empty
            themeColor={themeColor}
            title='发生错误'/>
        )
    }
  }

  render() {
    const themeColor = this.props.themeColor
    const titleCommonStyle = {
      borderBottomColor: themeColor,
      color: themeColor
    }

    const {
      avatar: {url: avatarUrl},
      username,
      nick,
      gender,
      mobilePhoneNumber,
      email,
      birth,
      canBeSearched
    } = this.props.notify.user

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.avatarWrapper}>
          <CustomCachedImage
            component={CustomImage}
            source={{uri: avatarUrl}}
            indicator={Pie}
            style={styles.avatar}
            indicatorProps={{
              size: 80,
              borderWidth: 0,
              color: themeColor,
              unfilledColor: '#999'
          }}/>
          </View>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>用户名：{username}</Text>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>昵称：{nick}</Text>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>性别：{gender}</Text>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>手机号：{mobilePhoneNumber}</Text>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>邮箱：{email}</Text>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>生日：{birth}</Text>
          <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>能否被查找：{canBeSearched ? '是' : '否'}</Text>
          {
            this.renderStatus()
          }
        </View>
        <ModalComponent
          visible={this.state.showRefuseConfirm}
          modalContainerStyle={styles.modalContainerStyle}
          title='提示'
          themeColor={themeColor}
          titltStyle={{color: themeColor}}
          content={
            <View>
              <InputComponent
                placeholder='输入您的拒绝理由'
                onChangeText={value => this.setState({reason: value})}
                placeholderTextColor={themeColor}
                style={{borderBottomColor: 'transparent'}}
                inputBarStyle={{marginLeft: 5}}
                placeholderTextColor={themeColor}/>
            </View>
          }
          buttons={
            <ButtonComponent
              title='确定'
              buttonStyle={{backgroundColor: themeColor, marginTop: 0}}
              onPressButton={() => this.refuseGroup()}/>
          }
          onClose={() => this.setState({showRefuseConfirm: false})}/>
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container
  },
  content: {
    marginTop: 20,
    width: getScreenSize().width - 60
  },
  title: {
    fontSize: 20,
    borderBottomWidth: getPixel() * 3,
    paddingVertical: 10,
    textAlignVertical: 'center',
    fontFamily: 'fzhaofang'
  },
  avatarWrapper: {
    alignItems: 'center'
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  opWrapper: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center'
  },
  opButton: {
    flex: 1,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3
  },
  opText: {
    color: '#fff',
    fontSize: 16
  },
  notifyWrapper: {
    marginTop: 10
  },
  notifyContentWrapper: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  notifyPic: {
    width: 15,
    height: 15,
    marginRight: 5
  },
  notifyContent: {
    fontSize: 16
  }
})

export default connect(state => ({
  user: state.user.user
}), dispatch => ({
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(GroupNotifyDetailScreen)
