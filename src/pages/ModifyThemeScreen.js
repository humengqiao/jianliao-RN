import React, {Component} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import commonStyle from '../styles/common';
import {getScreenSize, getPixel} from '../utils/common';
import {setThemeColor} from '../store/actions/common';
import theme from '../config/theme';
import startApp, {HOME_SCREEN} from '../../setup';
const CELL_HEIGHT = 60;

class ModifyThemeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      themeColor: props.themeColor
    }
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
  }

  onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress' && event.id === 'modifyTheme') {
      if(this.state.themeColor !== this.props.themeColor) {
        this.props.setThemeColor(this.state.themeColor)
        this.props.navigator.pop()
      }
    }
  }

  componentWillReceiveProps(nextProps, nextState) {
    if(nextProps.themeColor !== this.props.themeColor) {
      const {themeColor, selectedIndex} = nextProps
      startApp(HOME_SCREEN, themeColor, selectedIndex)
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.themeColor !== this.props.themeColor) {
      return false
    }
    return true
  }

  getRenderCellIndex() {
    const themeColor = this.props.themeColor
    const index = theme.findIndex(item => item === themeColor)
    if(index !== -1) {
      return index
    }
    return 0
  }

  renderCell(item, index) {
    const themeColor = this.state.themeColor
    return (
      <TouchableOpacity
        style={[styles.themeWrapper, {borderColor: this.props.themeColor}]}
        onPress={() => this.setState({themeColor: item})}>
          <View style={{backgroundColor: item, width: 30, height: 30, borderRadius: 5}}/>
          {
            item === themeColor ?
              <Image
                source={require('../images/correct.png')}
                style={{width: 20, height: 20, tintColor: themeColor}}/>
            :
              null
          }
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.inner}
          data={theme}
          keyExtractor={(index) => index}
          initialNumToRender={10}
          ItemSeparatorComponent={() => <View style={[styles.sep, {backgroundColor: this.state.themeColor}]}></View>}
          ListEmptyComponent={() => <Text style={[styles.empty, {color: this.props.themeColor}]}>暂无数据</Text>}
          getItemLayout={(data, index) => ({length: CELL_HEIGHT, offset: CELL_HEIGHT * index, index})}
          initialScrollIndex={this.getRenderCellIndex()}
          renderItem={({item, index}) => this.renderCell(item, index)}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
  },
  inner: {
    flex: 1,
    width: getScreenSize().width - 20
  },
  themeWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: CELL_HEIGHT,
    alignItems: 'center',
    paddingHorizontal: 10
  },
  sep: {
    height: getPixel() * 5,
    backgroundColor: '#000'
  },
  empty: {
    fontSize: 18,
    marginTop: 10,
    textAlign: 'center'
  }
})

export default connect(state => ({
  themeColor: state.common.themeColor,
  selectedIndex: state.chatEntry.selectedIndex
}), dispatch => ({
  setThemeColor: bindActionCreators(setThemeColor, dispatch)
}))(ModifyThemeScreen)
