import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import commonStyle from '../styles/common';
import {getScreenSize} from '../utils/common';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import {checkFeedbackValidate} from '../utils/checkValidate';
import {postFeedback} from '../service/user';
import notifyUtil from '../utils/notifyUtil';
import LoadingIndicator from '../components/LoadingIndicator';
import errorTranslate from '../utils/leancloudErrorMsg';
const FEEDBACK_HEIGHT = 150

export default class FeedbackScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      name: '',
      phonenumber: '',
      qq: '',
      feedback: ''
    }
  }

  async submitFeedback() {
    const {name, phonenumber, qq, feedback} = this.state
    if(checkFeedbackValidate(name, phonenumber, qq, feedback)) {
      try {
        this.setState({loading: true})
        await postFeedback(name, phonenumber, qq, feedback)
        this.setState({loading: false})
        notifyUtil.toast('感谢你的反馈')
        this.props.navigator.pop()
      }catch(error) {
        this.setState({
          loading: false
        })
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.innerWrapper}>
          <InputComponent
            placeholder='留下您的名字'
            style={[styles.inputWrapper, {borderColor: this.props.themeColor}]}
            placeholderTextColor={this.props.themeColor}
            onChangeText={name => this.setState({
              name
            })}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <InputComponent
            placeholder='留下您的手机号码'
            keyboardType='numeric'
            maxLength={11}
            style={[styles.inputWrapper, {borderColor: this.props.themeColor}]}
            placeholderTextColor={this.props.themeColor}
            onChangeText={phonenumber => this.setState({
              phonenumber
            })}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <InputComponent
            placeholder='留下您的QQ'
            keyboardType='numeric'
            maxLength={10}
            style={[styles.inputWrapper, {borderColor: this.props.themeColor}]}
            placeholderTextColor={this.props.themeColor}
            onChangeText={qq => this.setState({
              qq
            })}/>
          <InputComponent
            placeholder='留下您的意见与建议'
            multiline={true}
            numberOfLines={10}
            style={[styles.inputWrapper, styles.feedbackWrapper, {borderColor: this.props.themeColor}]}
            inputBarStyle={{textAlignVertical: 'top'}}
            placeholderTextColor={this.props.themeColor}
            onChangeText={feedback => this.setState({
              feedback
            })}
            rightElement={() => <View style={{marginTop: 15}}>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}
            underlineColorAndroid='transparent'/>
          <ButtonComponent
            title='提交'
            buttonStyle={{width: getScreenSize().width - 20, backgroundColor: this.props.themeColor}}
            onPressButton={() => this.submitFeedback()}/>
        </View>
        <LoadingIndicator
          visible={this.state.loading}
          color={this.props.themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    width: getScreenSize().width,
    paddingHorizontal: 10,
    backgroundColor: '#fff'
  },
  innerWrapper: {
    width: '100%'
  },
  feedbackWrapper: {
    height: FEEDBACK_HEIGHT,
    alignItems: 'flex-start'
  },
  inputWrapper: {
    width: '100%'
  }
})
