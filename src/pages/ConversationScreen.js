import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  ScrollView,
  RefreshControl,
  DeviceEventEmitter
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as chatEntryActionCreator from '../store/actions/chatEntry';
import * as conversationActionCreator from '../store/actions/conversation';
import {createConversation} from '../service/conversation';
import commonStyle from '../styles/common';
import {getScreenSize} from '../utils/common';
import config from '../config';
import notifyUtil from '../utils/notifyUtil';
import {createClient, doGetuser} from '../service/user';
import {
  getContactNotifyContent,
  getGroupNotifyContent,
  getUnreadMessageCount,
  filterConversation
} from '../utils/conversation';
import {getChatAndConversationTime} from '../utils/format';
import ConversationCell from '../components/ConversationCell';
import * as conversationTypes from '../config/conversationType';
import * as customMessageTypes from '../config/customMessageType';
import badgeModule from '../libs/BadgeModule';
import ImagePicker from 'react-native-image-picker';
import Empty from '../components/Empty';
import Swipeout from 'react-native-swipeout';

var AV = require('leancloud-storage');

class ConversationScreen extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      contactNotify: filterConversation(props.contactNotify, props.hideConversationList),
      groupNotify: filterConversation(props.groupNotify, props.hideConversationList),
      conversation: filterConversation(props.conversation, props.hideConversationList)
    }
  }

  onNavigatorEvent(event) {
    if(event.id === 'bottomTabSelected') {
      let selectedTabIndex = event.selectedTabIndex
      this.props.chatEntryaction.selectedIndexChange(selectedTabIndex)
    }
  }

  componentDidUpdate() {
    this.getTotalBadge()
  }

  componentDidMount() {
    this.connectServer()
    this.getTotalBadge()
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.isLoggedIn) {
      const hideConversationList = nextProps.hideConversationList
      const filteredConversation = filterConversation(nextProps.conversation, hideConversationList)
      const filteredContactNotify = filterConversation(nextProps.contactNotify, hideConversationList)
      const filteredGroupNotify = filterConversation(nextProps.groupNotify, hideConversationList)
      this.setState({
        conversation: filteredConversation,
        contactNotify: filteredContactNotify,
        groupNotify: filteredGroupNotify
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(!nextProps.isLoggedIn) {
      return false
    }
    return true
  }

  getAllUserOfMyContact() {
    const contact = this.props.contact
    let convergeContact = []
    contact.forEach(item => {
      convergeContact = [...convergeContact, ...item.list]
    })
    return convergeContact
  }

  async connectServer() {
    try {
      const username = this.props.user.username
      if(!global.client) {
        global.client = await createClient(username)
      }
      this.fetchAll()
      this.setClientEventListeners()
    }catch(error) {
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  setClientEventListeners() {
    global.client.on('message', (message, conversation) => {
      this.receiveMessage(conversation.toFullJSON())
    })
  }

  receiveMessage(conversation) {
    if(
      conversation.type === conversationTypes.CONTACT_NOTIFY
        &&
      conversation.lastMessage.data._lcattrs.type === customMessageTypes.ACCEPT_CONTACT
        ||
      conversation.type === conversationTypes.GROUP_NOTIFY
        &&
      conversation.lastMessage.data._lcattrs.type === customMessageTypes.ACCEPT_JOINGROUP
    ) {
      DeviceEventEmitter.emit('refreshContact')
    }
    this.props.conversationAction.modifyConversation(conversation)
  }

  fetchAll(force) {
    this.syncConversationList(conversationTypes.CONTACT_NOTIFY, force)
    this.syncConversationList(conversationTypes.GROUP_NOTIFY, force)
    this.syncConversationList(conversationTypes.COMMON, force)
    this.syncHideConversationList()
  }

  syncConversationList(type, force = false) {
    const lastUpdateAt = this.props.lastUpdateAt
    const now = (new Date()).getTime()
    if(force || lastUpdateAt < now - config.updateConversationDuration * 1000) {   //会话过期判断(默认12小时再同步服务器，排除手动下拉刷新)
      this.props.conversationAction.startFetchConversationList(type)
    }
  }

  syncHideConversationList() {
    this.props.conversationAction.startFetchHideConversationList(this.props.user.username)
  }

  async setBadge(totalCount) {
    const themeColor = this.props.themeColor
    if(totalCount > 0) {
      await badgeModule.setBadge(totalCount)
    }else {
      await badgeModule.removeBadge()
    }
    this.props.navigator.setTabBadge({
      tabIndex: 0,
      badge: totalCount,
      badgeColor: themeColor
    })
  }

  getTotalBadge() {
    const {contactNotify, groupNotify, conversation} = this.state
    const totalCount = getUnreadMessageCount(contactNotify) +
                       getUnreadMessageCount(groupNotify) +
                       getUnreadMessageCount(conversation)
    // this.setBadge(totalCount)
  }

  getTargetUsename(members) {
    const myUsername = this.props.user.username
    return members.filter(item => item !== myUsername)[0]
  }

  jumpToContactNotifyPage() {
    const themeColor = this.props.themeColor
    const username = this.props.user.username
    this.props.navigator.showModal({
      screen: 'jianliao.ContactNotifyScreen',
      title: '联系人通知',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor,
        username
      }
    })
  }

  jumpToGroupNotifyPage() {
    const themeColor = this.props.themeColor
    const username = this.props.user.username
    this.props.navigator.showModal({
      screen: 'jianliao.GroupNotifyScreen',
      title: '群组通知',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor,
        username
      }
    })
  }

  jumpToChatPage(conversationTitle, conversation) {
    const themeColor = this.props.themeColor
    const conversationType = conversation.type
    if(conversation.unreadMessagesCount > 0) {
      this.props.conversationAction.startClearUnreadStatus(conversation, conversationTypes.COMMON)
    }
    let target
    if(conversationType === conversationTypes.PRIVATE) {
      target = this.getTargetUsename(conversation.members)
    }else if(conversationType === conversationTypes.GROUP) {
      target = conversation.groupId
    }

    this.props.navigator.showModal({
      screen: 'jianliao.ChatScreen',
      title: `与${conversationTitle}聊天中`,
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ],
        rightButtons: [
          {
            id: 'more',
            title: '更多',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        themeColor,
        target,
        conversationType
      }
    })
  }

  renderConversationCell(type) {
    const themeColor = this.props.themeColor
    const username = this.props.user.username
    switch(type) {
      case conversationTypes.CONTACT_NOTIFY:
        {
          const contactNotify = this.state.contactNotify
          let time
          if(contactNotify.length > 0) {
            time = getChatAndConversationTime(contactNotify[0].lastMessageAt)
          }
          const contactNotifyContent = getContactNotifyContent(contactNotify, username)
          const unreadMessageCount = getUnreadMessageCount(contactNotify)
          return (
            <ConversationCell
              title='联系人通知'
              avatar={require('../images/contact_notify.png')}
              avatarStyle={{tintColor: themeColor}}
              content={
                <Text
                  style={{color: themeColor}}
                  ellipsizeMode='middle'>
                    {contactNotifyContent}
                </Text>
              }
              time={time}
              cacheImage={false}
              notifyContent={unreadMessageCount}
              themeColor={themeColor}
              onPressCell={() => this.jumpToContactNotifyPage()}/>
          )
          break
        }
      case conversationTypes.GROUP_NOTIFY:
        {
          const groupNotify = this.state.groupNotify
          const time = (groupNotify[0] && getChatAndConversationTime(groupNotify[0].lastMessageAt)) || ''
          const groupNotifyContent = getGroupNotifyContent(groupNotify, username)
          const unreadMessageCount = getUnreadMessageCount(groupNotify)
          return (
            <ConversationCell
              title='群通知'
              avatar={require('../images/group_notify.png')}
              avatarStyle={{tintColor: themeColor}}
              content={
                <Text
                  style={{color: themeColor}}
                  ellipsizeMode='middle'>
                    {groupNotifyContent}
                </Text>
              }
              time={time}
              cacheImage={false}
              notifyContent={unreadMessageCount}
              themeColor={themeColor}
              onPressCell={() => this.jumpToGroupNotifyPage()}/>
          )
          break
        }
      case conversationTypes.COMMON:
        return this.renderCommonConversation()
    }
  }

  getConversationFieldValue(conversation, field) {
    const me = this.props.user
    const conversationType = conversation.type
    if(conversationType === conversationTypes.PRIVATE) {
      const contact = this.props.contact
      const targetUsername = conversation.members.filter(item => item !== me.username)[0]
      const contactUsers = []
      contact.forEach(item => {
        contactUsers = [...contactUsers, ...item.list]
      })
      const index = contactUsers.findIndex(item => item.username === targetUsername)
      if(index !== -1) {
        if(field === 'avatar') {
          return contactUsers[index][field].url
        }
        return contactUsers[index][field]
      }else {
        if(field === 'nick') {
          return '未知用户'
        }else if(field === 'avatar') {
          return config.defaultAvatar
        }
      }
    }else if(conversationType === conversationTypes.GROUP) {
      const group = this.props.group
      const groupId = conversation.groupId
      const index = group.list.findIndex(item => item.groupId === groupId)
      if(index !== -1) {
        if(field === 'avatar') {
          return group.list[index][field].url
        }
        return group.list[index][field]
      }else {
        if(field === 'nick') {
          return '未知群组'
        }else if(field === 'avatar') {
          return config.defaultAvatar
        }
      }
    }
  }

  getConversationContent(conversation) {
    if(conversation.type === conversationTypes.PRIVATE) {
      return conversation.lastMessage.data._lctext
    }else if(conversation.type === conversationTypes.GROUP) {  //获取群组会话显示文案
      const myUsername = this.props.user.username
      const senderUsername = conversation.lastMessage.from
      const content = conversation.lastMessage.data._lctext
      if(myUsername !== senderUsername) {
        if(conversation.lastMessage.data._lcattrs) {
          const sendUserNick = conversation.lastMessage.data._lcattrs.senderUserNick
          return `${sendUserNick}:${content}`
        }else {
          return `未知成员:${content}`
        }
      }else {
        return content
      }
    }
  }

  clearUnreadStatus(conversation) {
    this.props.conversationAction.startClearUnreadStatus(conversation, conversationTypes.COMMON)
  }

  removeConversation(conversation) {
    const username = this.props.user.username
    this.props.conversationAction.startRemoveConversation(conversation, username)
  }

  refreshConversation() {
    this.fetchAll(true)
  }

  renderCommonConversation(item) {
    const themeColor = this.props.themeColor
    if(!item.lastMessage) {
      return []
    }

    const fromUsername = item.lastMessage.from
    const conversationType = item.type
    const conversationTitle = this.getConversationFieldValue(item, 'nick')
    const avatarUrl = this.getConversationFieldValue(item, 'avatar')
    const content = this.getConversationContent(item)
    const lastUpdateAt = getChatAndConversationTime(item.updatedAt)
    return (
      <Swipeout
        right={[
          {text: '设为已读', backgroundColor: themeColor, type: 'default', underlayColor: '#999', onPress: () => this.clearUnreadStatus(item)},
          {text: '删除', backgroundColor: '#ff0000', type: 'delete', underlayColor: '#999', onPress: () => this.removeConversation(item)}
        ]}
        backgroundColor='#fff'
        buttonWidth={80}
        autoClose={true}
        close={true}>
        <ConversationCell
          key={item.id}
          title={conversationTitle}
          onPressCell={conversationId => this.jumpToChatPage(conversationTitle, item)}
          avatar={{uri: avatarUrl}}
          content={
            <Text
              style={{color: themeColor}}
              ellipsizeMode='middle'>
                {content}
            </Text>
          }
          time={lastUpdateAt}
          cacheImage={!avatarUrl===config.defaultAvatar}
          notifyContent={item.unreadMessagesCount}
          themeColor={themeColor}/>
      </Swipeout>
    )
    return commonConversation
  }

  render() {
    const themeColor = this.props.themeColor
    const conversation = this.state.conversation
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={{alignItems: 'center'}}
        refreshControl={
          <RefreshControl
            onRefresh={() => this.refreshConversation()}
            refreshing={this.props.loading}
            colors={[themeColor]}/>
        }>
        {
          this.renderConversationCell(conversationTypes.CONTACT_NOTIFY)
        }
        {
          this.renderConversationCell(conversationTypes.GROUP_NOTIFY)
        }
        <FlatList
          data={conversation}
          keyExtractor={(item, index) => index}
          ListEmptyComponent={<Empty title='暂无会话' titleStyle={commonStyle.empty} themeColor={themeColor}/>}
          renderItem={({item}) => this.renderCommonConversation(item)}/>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: getScreenSize().width
  }
})

export default connect(state => ({
  isLoggedIn: state.user.isLoggedIn,
  loading: state.common.loading,
  themeColor: state.common.themeColor,
  user: state.user.user,
  contact: state.relationship.contact,
  group: state.relationship.group,
  conversation: state.conversation.conversation,
  contactNotify: state.conversation.contactNotify,
  groupNotify: state.conversation.groupNotify,
  lastUpdateAt: state.conversation.lastUpdateAt,
  hideConversationList: state.conversation.hideConversationList
}), dispatch => ({
  chatEntryaction: bindActionCreators(chatEntryActionCreator, dispatch),
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(ConversationScreen)
