import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import {getScreenSize} from '../utils/common';
import {formatLeancloudErrorMsg} from '../utils/format';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import {getCaptcha, checkCaptcha, getResetPassCheckCode, checkResetCheckCode} from '../service/common';
import LoadingIndicator from '../components/LoadingIndicator';
import notifyUtil from '../utils/notifyUtil';
import errorTranslate from '../utils/leancloudErrorMsg';

export default class ResetPassConfirmScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      code: '',
      captcha: '',
      captchaUrl: '',
      newPass: ''
    }
  }

  componentDidMount() {
    this.sendCheckCode()
    this.showCaptcha()
  }

  async sendCheckCode() {
    try {
      await getResetPassCheckCode(this.props.phoneNumber)
      notifyUtil.toast('发送验证码成功', 'long')
    }catch(error) {
      notifyUtil.toast(errorTranslate(error), 'long')
    }
  }

  async showCaptcha() {
    try {
      this.captcha = await getCaptcha(100, 50)
      this.setState({
        captchaUrl: this.captcha.url
      })
    }catch(error) {
      notifyUtil.toast(errorTranslate(error), 'long')
    }
  }

  async submitReset() {
    if(!this.state.code.replace(/\s/g, '')) {
      notifyUtil.toast('再看看，少了什么？', 'long')
      return
    }

    if(!this.state.newPass.replace(/\s/g, '')) {
      notifyUtil.toast('密码不要了？', 'long')
      return
    }

    this.setState({
      loading: true
    })

    try {
      await checkResetCheckCode(this.state.code, this.state.newPass)
      this.setState({
        loading: false
      })

      notifyUtil.toast('密码重置成功', 'long')
      this.props.navigator.resetTo({
        screen: 'jianliao.LoginScreen',
        animated: true,
        animationType: 'slide-horizontal',
        navigatorStyle: {
          navBarHidden: true
        }
      })
    }catch(error) {
      this.setState({
        loading: false
      })
      notifyUtil.toast(errorTranslate(error), 'long')
    }
  }

  async reSendCode() {
    if(this.props.showCaptcha) {
      if(!this.state.captcha.replace(/\s/g, '')) {
        notifyUtil.toast('别调皮，你忘了输入图形验证码了', 'long')
        return
      }
      if(!/.{4}/.test(this.state.captcha)) {
        notifyUtil.toast('图形验证码只能是4位哦', 'long')
        return
      }

      this.setState({
        loading: true
      })

      try {
        await checkCaptcha(this.captcha, this.state.captcha)
        const result = await getResetPassCheckCode(this.props.phoneNumber)
        this.setState({
          loading: false
        })
        notifyUtil.toast('发送成功', 'long')
      }catch(error) {
        this.setState({
          loading: false
        })
        this.showCaptcha()
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }else {
      this.setState({
        loading: true
      })
      try {
        await getCheckCode(this.props.phoneNumber)
        this.setState({
          loading: false
        })
        notifyUtil.toast('发送成功', 'long')
      }catch(error) {
        this.setState({
          loading: false
        })
        this.showCaptcha()
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  renderCaptcha() {
    if(this.props.showCaptcha) {
      return (
        <View>
          <View style={styles.captchaWrapper}>
            {
              this.state.captchaUrl ?
                <Image
                  source={{uri: this.state.captchaUrl}}
                  style={styles.captcha}/>
                :
                  null
            }
            <TouchableOpacity onPress={() => this.showCaptcha()}>
              <Text style={{fontSize: 16, marginLeft: 10, color: this.props.themeColor}}>看不清楚？</Text>
            </TouchableOpacity>
          </View>
          <InputComponent
            placeholder='输入图形验证码(重发短信时)'
            keyboardType='email-address'
            maxLength={4}
            placeholderTextColor={this.props.themeColor}
            style={{borderColor: this.props.themeColor}}
            onChangeText={captcha => {
              this.setState({
                captcha
              })
            }}/>
        </View>
      )
    }
  }

  renderContent() {
      return (
        <View>
          {this.renderCaptcha()}
          <View>
            <InputComponent
              placeholder='请输入验证码'
              keyboardType='email-address'
              placeholderTextColor={this.props.themeColor}
              style={{borderColor: this.props.themeColor}}
              onChangeText={code => {
                this.setState({
                  code
                })
              }}
              maxLength={6}
              rightElement={() =>
                <TouchableOpacity onPress={() => this.reSendCode()}>
                  <Text style={{fontSize: 14, color:this.props.themeColor}}>没收到？</Text>
                </TouchableOpacity>
              }/>
              <InputComponent
                placeholder='请输入新密码'
                keyboardType='email-address'
                placeholderTextColor={this.props.themeColor}
                style={{borderColor: this.props.themeColor}}
                onChangeText={newPass => {
                  this.setState({
                    newPass
                  })
                }}
                rightElement={() =>
                  <Text style={{color: this.props.themeColor}}>必填</Text>
                }/>
            <ButtonComponent
              onPressButton={() => this.submitReset()}
              buttonStyle={{backgroundColor: this.props.themeColor}}
              title='确定'/>
          </View>
          <LoadingIndicator
            visible={this.state.loading}
            color={this.props.themeColor}/>
        </View>
      )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderContent()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: getScreenSize().width * 0.8,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 80
  },
  captchaWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  captcha: {
    width: 100,
    height: 50
  }
})
