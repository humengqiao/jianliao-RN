import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {startModifyGroup} from '../store/actions/relationship';
import commonStyle from '../styles/common';
import LoadingIndicator from '../components/LoadingIndicator';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import ImagePicker from 'react-native-image-picker';
import notifyUtil from '../utils/notifyUtil';

class ModifyGroupInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      nick: props.group.nick,
      avatarUrl: props.group.avatar.url
    }
  }

  modiyGroupInfo(){
    const {nick, avatar: {url: avatarUrl}, objectId: id}= this.props.group
    if(this.state.nick === nick && this.state.avatarUrl === avatarUrl) {
      this.props.navigator.pop()
      return
    }

    if(this.state.nick !== nick && this.state.avatarUrl !== avatarUrl) {
      this.props.modifyGroupInfoAction(id, nick, this.pic)
    }else if(this.state.nick !== nick) {
      this.props.modifyGroupInfoAction(id, this.state.nick)
    }else if(this.state.avatarUrl !== avatarUrl) {
      this.props.modifyGroupInfoAction(id, undefined, this.pic)
    }
  }

  showSelectPic() {
    const options = {
      title: '选择图片',
      cancelButtonTitle: '取消',
      takePhotoButtonTitle: '拍照',
      chooseFromLibraryButtonTitle: '图库',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
      permissionDenied: {
        title: '失败',
        text: '您没有设备访问权限',
        reTryTitle: '重试',
        okTitle: '确定'
      }
    }

    ImagePicker.showImagePicker(options, response => {
      if(response.error) {
        Alert.alert('提示', `发生错误:${response.error}`, [
          {
            text: '确定'
          }
        ])
      }else if(!response.didCancel){
        this.pic = response
        this.setState({avatarUrl: response.uri})
      }
    })
  }

  render() {
    const themeColor = this.props.themeColor
    const defaultNick = this.props.group.nick
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.showSelectPic()}>
          <Image
            style={styles.avatar}
            source={{uri: this.state.avatarUrl}}/>
          <Text style={[styles.alert, {color: themeColor}]}>点击修改群组头像</Text>
        </TouchableOpacity>
        <InputComponent
          placeholder='输入您要修改的新群组名称'
          onChangeText={value => this.setState({nick: value})}
          style={{borderBottomColor: themeColor, marginTop: 10}}
          placeholderTextColor={themeColor}
          defaultValue={defaultNick}/>
        <ButtonComponent
          title='修改'
          buttonStyle={{backgroundColor: themeColor}}
          onPressButton={() => this.modiyGroupInfo()}/>
        <LoadingIndicator
          visible={this.props.loading}
          color={themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    paddingTop: 50
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
    borderColor: 'red',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  alert: {
    marginTop: 5
  }
})

export default connect(state => ({
  loading: state.common.loading
}), dispatch => ({
  modifyGroupInfoAction: bindActionCreators(startModifyGroup, dispatch)
}))(ModifyGroupInfo)
