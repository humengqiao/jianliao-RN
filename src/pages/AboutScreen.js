import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import DeviceInfo from 'react-native-deviceinfo';

export default class AboutScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View>
        <Text style={[styles.title, {color: this.props.themeColor}]}>简聊RN</Text>
        <Text style={[styles.desc, {color: this.props.themeColor}]}>简聊RN是一款IM APP，基于React Native开发，即时通信与存储使用leancloud，混合原生模块，提供单聊，群聊，支持发送文本，语音，图片，位置等消息。</Text>
        <Text style={[styles.version, {color: this.props.themeColor}]}>版本:{DeviceInfo.getVersion()}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: '500',
    marginTop: 40
  },
  desc: {
    marginHorizontal: 10,
    marginTop: 10
  },
  version: {
    textAlign: 'center',
    fontSize: 16,
    marginTop: 20
  }
})
