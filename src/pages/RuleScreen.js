import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  Share,
  Alert,
  StyleSheet
} from 'react-native';
import ruleText from '../config/rule';
import notifyUtil from '../utils/notifyUtil';

export default class RuleScreen extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
  }

  onNavigatorEvent(event) {
    if(event.id === 'share') {
      Share.share({
        message: `
            《简聊RN用户使用协议》
            ${ruleText}
        `,
        title: '《简聊RN》用户使用协议'
      }, {
        dialogTitle: '《简聊RN》用户使用协议',
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ],
        tintColor: '#999'
      })
      .catch(error => notifyUtil.toast(error.toString(), 'short'))
    }else if(event.id === 'help') {
      Alert.alert('帮助', '不多说，很快上手')
    }
  }

  render() {
    return (
      <View>
        <Text style={styles.title}>《简聊RN用户使用协议》</Text>
        <Text>{ruleText}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 20
  }
})
