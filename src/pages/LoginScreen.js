import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  ActivityIndicator,
  Keyboard
} from 'react-native';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActionCreator from '../store/actions/user';
import * as chatEntryActionCreator from '../store/actions/chatEntry';
import {formatLeancloudErrorMsg} from '../utils/format';
import config from '../config';
import alert from '../config/alert';
import commonStyle from '../styles/common';
import notifyUtil from '../utils/notifyUtil';
import LoadingIndicator from '../components/LoadingIndicator';
import startApp, {HOME_SCREEN} from '../../setup';
import {CachedImage} from 'react-native-img-cache';
import {getScreenSize} from '../utils/common';

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showPassword: true,
      username: '',
      password: '',
      avatar: config.defaultAvatar
    }
  }

  componentDidMount() {
    this.setLoginUserToInput()
  }

  componentWillReceiveProps(nextProps, nextState) {
    const selectedIndex = nextProps.selectedIndex
    const themeColor = nextProps.themeColor
    if(nextProps.isLoggedIn) {
      startApp(HOME_SCREEN, themeColor, selectedIndex)
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.isLoggedIn) {
      return false
    }
    return true
  }

  setLoginUserToInput() {
    if(this.props.loginedUsers.length > 0) {
      let {username, avatar: {url}} = this.props.loginedUsers[0]
      this.setState({
        username,
        avatar: url
      })
    }
  }

  renderRightButton(icon, onPressHandler) {
    return (
      <TouchableOpacity onPress={() => onPressHandler()}>
        <Image source={icon} style={[styles.rightIcon, {tintColor: this.props.themeColor}]}/>
      </TouchableOpacity>
    )
  }

  onPressShowMoreUserList() {
    this.props.navigator.showLightBox({
      screen: 'jianliao.LoginUsersScreen',
      style: {
        backgroundBlur: 'dark',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        tapBackgroundToDismiss: true
      },
      passProps: {
        onSelected: (user) => {
          this.onSelectedUserList(user)
        },
        themeColor: this.props.themeColor
      }
    })
  }

  onSelectedUserList(user) {
    let {username, avatar: {url: avatarUrl}} = user
    this.setState({
      username,
      avatar: avatarUrl
    })
  }

  onPressShowPassword() {
    let showPassword = !this.state.showPassword
    this.setState({
      showPassword
    })
  }

  goRegisterPage() {
    Keyboard.dismiss()
    this.props.navigator.push({
      screen: 'jianliao.RegisterScreen',
      title: '注册',
      navigatorStyle: {
        screenBackgroundColor: '#fff',
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
      },
      animationType: 'slide-up',
      passProps: {
        themeColor: this.props.themeColor
      }
    })
  }

  goResetPass() {
    Keyboard.dismiss()
    this.props.navigator.push({
      screen: 'jianliao.ResetPassScreen',
      title: '重置密码',
      navigatorStyle: {
        screenBackgroundColor: '#fff',
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
      },
      animationType: 'slide-up',
      passProps: {
        themeColor: this.props.themeColor
      }
    })
  }

  matchUserAvatar(username) {
    let index = this.props.loginedUsers.findIndex(item => item.username === username)
    if(index !== -1) {
      let avatar = this.props.loginedUsers[index].avatar.url
      this.setState({
        avatar
      })
    }else {
      this.setState({
        avatar: config.defaultAvatar
      })
    }
  }

  setUsernameChange(username) {
    this.matchUserAvatar(username)
    this.setState({
      username
    })
  }

  setPasswordChange(password) {
    this.setState({
      password
    })
  }

  login() {
    Keyboard.dismiss()
    if(!this.state.username.replace(/\s/g, '') || !this.state.password.replace(/\s/g, '')) {
      notifyUtil.toast(alert.loginErrorEmpty, 'long')
      return
    }

    this.props.userAction.startLogin(this.state.username, this.state.password)
  }

  render() {
    const togglePasswordIcon = !this.state.showPassword ?
        require('../images/eye_close.png')
      :
        require('../images/eye_open.png')
    const avatarComp = this.state.avatar === config.defaultAvatar ? //因为CachedImage不支持android assets目录图片，所以做这个适配
        <Image
          source={{uri: this.state.avatar}}
          style={styles.avatar}
          resizeMode='cover'/>
      :
        <CachedImage
          source={{uri: this.state.avatar}}
          style={styles.avatar}
          resizeMode='cover'/>

    return (
      <View style={styles.container}>
        <View style={styles.avatarWrapper}>
          {avatarComp}
        </View>
        <View style={styles.inputWrapper}>
          <View>
            <InputComponent
              style={[styles.input, {borderColor: this.props.themeColor}]}
              inputBarStyle={styles.inputBarStyle}
              placeholder='请输入您的用户名'
              keyboardType='email-address'
              secureTextEntry={false}
              maxLength={12}
              defaultValue={this.state.username}
              placeholderTextColor={this.props.themeColor}
              onChangeText={(value) => this.setUsernameChange(value)}
              rightElement={() => this.renderRightButton(require('../images/users.png'), () => this.onPressShowMoreUserList())}/>
          </View>
          <View>
            <InputComponent
              style={[styles.input, {borderColor: this.props.themeColor}]}
              inputBarStyle={styles.inputBarStyle}
              placeholder='请输入您的密码'
              secureTextEntry={this.state.showPassword}
              placeholderTextColor={this.props.themeColor}
              onChangeText={(value) => this.setPasswordChange(value)}
              rightElement={() => this.renderRightButton(togglePasswordIcon, () => this.onPressShowPassword())}/>
          </View>
        </View>
        <ButtonComponent
          activeOpacity={0.7}
          onPressButton={() => this.login()}
          style={styles.submit}
          buttonStyle={{backgroundColor: this.props.themeColor}}
          title='登 录'/>
        <View style={styles.operationWrapper}>
          <TouchableOpacity onPress={() => this.goRegisterPage()}>
            <Text style={{color: this.props.themeColor}}>注册</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.goResetPass()}>
            <Text style={{color: this.props.themeColor}}>忘记密码</Text>
          </TouchableOpacity>
        </View>
        <LoadingIndicator
          visible={this.props.loading}
          color={this.props.themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    marginTop: 60
  },
  avatarWrapper: {
    alignItems: 'center',
    height: config.avatarHeight,   //优化，给图片容器固定高度，防止rerender时图片组件瞬间被替换时闪一下
  },
  avatar: {
    width: config.avatarWidth,
    height: config.avatarHeight,
    borderRadius: config.avatarWidth >= config.avatarHeight ? config.avatarWidth / 2 : config.avatarHeight / 2
  },
  inputWrapper: {
    marginTop: 20
  },
  input: {
    flexDirection: 'row'
  },
  inputBarStyle: {
    fontSize: 16
  },
  rightIcon: {
    width: 20,
    height: 20
  },
  operationWrapper: {
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: getScreenSize().width * 0.8
  }
})

export default connect(state => ({
  selectedIndex: state.chatEntry.selectedIndex,
  loginedUsers: state.user.loginedUsers,
  isLoggedIn: state.user.isLoggedIn,
  loading: state.common.loading,
  themeColor: state.common.themeColor
}), dispatch =>({
  userAction: bindActionCreators(userActionCreator, dispatch),
  chatEntry: bindActionCreators(chatEntryActionCreator, dispatch)
}))(LoginScreen)
