import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert
} from 'react-native';
import {connect} from 'react-redux';
import commonStyle from '../styles/common';
import {bindActionCreators} from 'redux';
import {getScreenSize, getPixel} from '../utils/common';
import ActionSheet from 'react-native-actionsheet';
import LoadingIndicator from '../components/LoadingIndicator';
import {removeUserFromBlacklist} from '../service/relationship';
import notifyUtil from '../utils/notifyUtil';
import {moveFromBlacklist as moveFromBlacklistAction} from '../store/actions/relationship';
import {CustomCachedImage} from 'react-native-img-cache';
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';

class BlacklistInfoDetailScreen extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      loading: false,
      actionSheetOptions: []
    }
  }

  onNavigatorEvent(event) {
    const themeColor = this.props.themeColor

    let actionSheetOptions
    if(event.type === 'NavBarButtonPress' && event.id === 'more') {
      actionSheetOptions = [
        <View style={{flexDirection: 'row'}}>
          <Image
            source={require('../images/delete_user.png')}
            style={{width: 20, height: 20, marginRight: 5, tintColor: themeColor}}/>
          <Text style={{color: themeColor, fontSize: 16}}>从黑名单删除</Text>
        </View>,
        <Text style={{color: themeColor, fontSize: 16}}>取消</Text>
      ]

      this.setState({
        actionSheetOptions
      }, () => {
        this.ActionSheet.show()
      })
    }
  }

  jumpToAddUserPage() {
    const {themeColor, blacklist: user} = this.props
    this.props.navigator.push({
      screen: 'jianliao.AddUserScreen',
      title: '联系人添加',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ],
        rightButtons: [
          {
            id: 'send',
            title: '发送',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        user,
        themeColor
      }
    })
  }

  showRemoveFromBlackListConfirm() {
    Alert.alert(
      '提示',
      '您确定要从黑名单删除该联系人吗？',
      [
        {text: '取消', onPress: () => {}, style: 'cancel'},
        {text: '确定', onPress: () => this.onRemoveFromBlackList()},
      ],
      {cancelable: false}
    )
  }

  async onRemoveFromBlackList() {
    const myId = this.props.id
    this.setState({loading: true})
    try {
      const {objectId: userId, username} = this.props.blacklist
      await removeUserFromBlacklist(myId, userId)
      this.props.moveFromBlacklistAction(username)
      this.setState({loading: false})
      notifyUtil.toast('从黑名单删除成功')
      this.props.navigator.dismissModal()
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  async onPressItem(index) {
    if(index === this.state.actionSheetOptions.length -1) {
      return
    }

    if(index === 0) {
      this.showRemoveFromBlackListConfirm()
    }
  }

  renderButton() {
    const themeColor = this.props.themeColor
    return (
      <TouchableOpacity
        onPress={() => this.jumpToAddUserPage()}
        style={[styles.button, {backgroundColor: themeColor}]}>
        <Text style={styles.buttonText}>添加联系人</Text>
      </TouchableOpacity>
    )
  }

  renderUserInfo() {
    const themeColor = this.props.themeColor
    const titleCommonStyle = {
      borderBottomColor: themeColor,
      color: themeColor
    }

    const {
      username,
      nick,
      gender,
      mobilePhoneNumber,
      email,
      birth,
      canBeSearched
    } = this.props.blacklist

    return (
      <View style={styles.content}>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>用户名：{username}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>昵称：{nick}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>性别：{gender}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>手机号：{mobilePhoneNumber}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>邮箱：{email}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>生日：{birth}</Text>
        <Text style={[styles.title, titleCommonStyle]} numberOfLines={1}>能否被查找：{canBeSearched ? '是' : '否'}</Text>
        {
          this.renderButton()
        }
      </View>
    )
  }

  render() {
    const avatarUrl = this.props.blacklist.avatar.url
    const themeColor = this.props.themeColor
    return (
      <ScrollView
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={true}
        overScrollMode='always'>
        <CustomCachedImage
          component={CustomImage}
          source={{uri: avatarUrl}}
          indicator={Pie}
          style={styles.avatar}
          indicatorProps={{
            size: 80,
            borderWidth: 0,
            color: themeColor,
            unfilledColor: '#999'
        }}/>
        {
          this.renderUserInfo()
        }
        <ActionSheet
          ref={el => this.ActionSheet = el}
          title={<Text style={{fontSize: 18, color: themeColor}}>提示</Text>}
          tintColor={themeColor}
          options={this.state.actionSheetOptions}
          cancelButtonIndex={this.state.actionSheetOptions.length - 1}
          onPress={index => this.onPressItem(index)}/>
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    paddingTop: 40,
    paddingHorizontal: 10
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  content: {
    marginTop: 20,
    width: getScreenSize().width - 60
  },
  title: {
    fontSize: 20,
    borderBottomWidth: getPixel() * 3,
    paddingVertical: 10,
    textAlignVertical: 'center',
    fontFamily: 'fzhaofang'
  },
  button: {
    height: 40,
    marginTop: 10,
    borderRadius: getPixel() * 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#fff',
    fontSize: 18
  }
})

export default connect(state => ({
  id: state.user.user.id
}), dispatch => ({
  moveFromBlacklistAction: bindActionCreators(moveFromBlacklistAction, dispatch)
}))(BlacklistInfoDetailScreen)
