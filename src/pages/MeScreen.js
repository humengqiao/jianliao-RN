import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Image,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  DatePickerAndroid
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as chatEntryActionCreator from '../store/actions/chatEntry';
import * as userActionCreator from '../store/actions/user';
import commonStyle from '../styles/common';
import {getPixel, getScreenSize, genBirth} from '../utils/common';
import config from '../config';
import ButtonComponent from '../components/ButtonComponent';
import ImagePicker from 'react-native-image-picker';
import {getCurrentUser, doLogout} from '../service/user';
import {CustomCachedImage} from 'react-native-img-cache';
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
import LoadingIndicator from '../components/LoadingIndicator';
import startApp, {LOGIN_SCREEN} from '../../setup';
import notifyUtil from '../utils/notifyUtil';
import {formatLeancloudErrorMsg} from '../utils/format';
import {postVerifyEmail, getCheckCode} from '../service/common';
import ActionSheet from 'react-native-actionsheet';
import PersonalPageCell from '../components/PersonalPageCell';
import errorTranslate from '../utils/leancloudErrorMsg';

class MeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      avatar: ''
    }

    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
  }

  componentWillMount() {
    this.setGenderActionSheetConfig()
  }

  componentWillReceiveProps(nextProps, nextState) {
    if(!nextProps.isLoggedIn) {
      startApp(LOGIN_SCREEN)
    }else if(nextProps.user.gender !== this.props.user.gender) {
      this.setGenderActionSheetConfig(nextProps.user.gender)
    }
  }

  shouldComponentUpdate(nextProps, nextState) {  //防止redux state更新后改变state时avatar.url取不到值
    if(nextProps.isLoggedIn) {
      return true
    }
    return false
  }

  setGenderActionSheetConfig(gender) {
    const calGender = gender ? gender : this.props.user.gender
    const themeColor = this.props.themeColor
    this.actionSheetOptions = [
      <View style={{flexDirection: 'row'}}>
        {
          calGender === '男' ?
            <Image
              source={require('../images/correct.png')}
              style={{width: 20, height: 20, marginRight: 5, tintColor: themeColor}}/>
          :
            null
        }
        <Text style={{color: themeColor}}>男</Text>
      </View>,
      <View style={{flexDirection: 'row'}}>
        {
          calGender === '女' ?
            <Image
              source={require('../images/correct.png')}
              style={{width: 20, height: 20, marginRight: 5, tintColor: themeColor}}/>
          :
            null
        }
        <Text style={{color: themeColor}}>女</Text>
      </View>,
      <Text style={{color: themeColor, fontSize: 16}}>取消</Text>
    ]
  }

  onNavigatorEvent(event) {
    if(event.id === 'bottomTabSelected') {
      const selectedTabIndex = event.selectedTabIndex
      this.props.chatEntryAction.selectedIndexChange(selectedTabIndex)
    }
  }

  onRefreshToUpdateUser() {
    const username = this.props.user.username
    this.props.userAction.startRefreshUser('username', username)
  }

  onPressAvatar() {
    const options = {
      title: '选择图片',
      cancelButtonTitle: '取消',
      takePhotoButtonTitle: '拍照',
      chooseFromLibraryButtonTitle: '图库',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
      permissionDenied: {
        title: '失败',
        text: '您没有设备访问权限',
        reTryTitle: '重试',
        okTitle: '确定'
      }
    }

    ImagePicker.showImagePicker(options, response => {
      if(response.error) {
        Alert.alert('提示', `发生错误:${response.error}`, [
          {
            text: '确定'
          }
        ])
      }else if(!response.didCancel){
        this.updateAvatarForMe(response)
      }
    })
  }

  async updateAvatarForMe(pic) {
    const username = this.props.user.username
    const data = {
      username,
      pic
    }

    this.props.userAction.startUpdateAvatar(data)
  }

  logoutConfirm() {
    Alert.alert('提示', '您确定要退出吗?', [
      {text: '确定', onPress: () => this.logout()},
      {text: '取消', style: 'cancel'}
    ],
    {
      cancelable: true
    })
  }

  logout() {
    this.props.userAction.startLogout()
  }

  jumpToUpdateInfoPage(type) {
    this.props.navigator.showModal({
      screen: 'jianliao.UpdateInfoScreen',
      title: '修改个人资料',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      },
      passProps: {
        type,
        themeColor: this.props.themeColor
      }
    })
  }

  jumpToSlientConversationPage() {

  }

  async jumpToVerifyPage(type, verified, numberOrEmail) {
    if(type === '') {
      return
    }

    if(verified) {
      notifyUtil.toast(`您已验证${type === 'verifyEmail' ? '邮箱' : '手机号码'}`, 'long')
      return
    }

    if(type === 'verifyEmail') {
      try {
        await postVerifyEmail(numberOrEmail)
        notifyUtil.toast('发送邮件成功')
      }catch(error) {
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }else if(type === 'verifyPhone'){
      try {
        await getCheckCode(numberOrEmail, true)
        notifyUtil.toast('发送短信成功')
        this.props.navigator.push({
          screen: 'jianliao.VerifyScreen',
          title: '验证手机号码',
          animationType: 'slide-up',
          navigatorStyle: {
            navBarTitleTextCentered: true,
            navBarBackgroundColor: this.props.themeColor,
            navBarTextColor: '#fff',
            screenBackgroundColor: '#fff'
          },
          passProps: {
            type,
            numberOrEmail,
            showCaptcha: config.enableCaptchaVerify,
            themeColor: this.props.themeColor,
            from: 'MeScreen'
          },
          navigatorButtons: {
            leftButtons: [
              {
                id: 'back',
                buttonColor: '#fff'
              }
            ]
          }
        })
      }catch(error) {
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  showGenderSelect() {
    this.ActionSheet.show()
  }

  setGender(index) {
    let newGender
    if(index === 0) {
      newGender = '男'
    }else if(index === 1) {
      newGender = '女'
    }

    const {username, gender} = this.props.user
    if((index === 0 || index === 1) && newGender !== gender) {
      this.props.userAction.startUpdateInfo({
        username,
        type: 'gender',
        info: newGender
      })
    }
  }

  async showBirthSelect() {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: new Date(config.birthYear, config.birthMonth, config.birthDay),
        minDate: new Date(1950, 1, 1),
        maxDate: new Date(),
        mode: 'calendar'
      })

      if(action !== DatePickerAndroid.dismissedAction) {
        const newBirth = genBirth(year, month, day)
        const {username, birth} = this.props.user
        if(newBirth !== birth) {
          this.props.userAction.startUpdateInfo({
            username,
            type: 'birth',
            info: newBirth
          })
        }
      }
    }catch({message}) {
      notifyUtil.toast(message, 'long')
    }
  }

  jumpToSettingsPage() {
    this.props.navigator.showModal({
      screen: 'jianliao.SettingsScreen',
      title: '设置',
      animationType: 'slide-up',
      navigatorStyle: {
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        screenBackgroundColor: '#fff'
      },
      navigatorButtons: {
        leftButtons: [
          {
            id: 'back',
            buttonColor: '#fff'
          }
        ]
      }
    })
  }

  render() {
    const {
        avatar: {url: avatarUrl, updatedAt, name: avatarName},
        username,
        nick,
        email,
        emailVerified,
        mobilePhoneNumber,
        mobilePhoneVerified,
        gender,
        birth
      } = this.props.user
    const themeColor = this.props.themeColor

    return (
      <View style={styles.container}>
        <ScrollView
          contentContainerStyle={{backgroundColor: '#fff', paddingHorizontal: 10}}
          refreshControl={
            <RefreshControl
              colors={[themeColor, '#ccc']}
              refreshing={this.props.refreshing}
              onRefresh={() => this.onRefreshToUpdateUser()}/>
          }
          showsVerticalScrollIndicator={true}
          overScrollMode='always'>
          <TouchableOpacity onPress={() => this.onPressAvatar()}>
            <View style={[styles.avatarWrapper, {borderBottomColor: themeColor}]}>
              <View>
                <CustomCachedImage
                  component={CustomImage}
                  source={{uri: avatarUrl}}
                  indicator={Pie}
                  indicatorProps={{
                    size: 80,
                    borderWidth: 0,
                    color: themeColor,
                    unfilledColor: '#999'
                  }}
                  style={styles.avatar}/>
                {
                  avatarName !== config.defaultAvatarName ?
                    <Text style={{marginLeft: 20, marginBottom: 5, color: themeColor}}>最后更新:{updatedAt}</Text>
                  :
                    null
                }
              </View>
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            </View>
          </TouchableOpacity>
          <PersonalPageCell
            onPressCell={() => this.jumpToUpdateInfoPage('nick')}
            title='昵称'
            content={nick}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
              }/>
          <PersonalPageCell
           title='用户名'
           content={username}
           titleStyle={{color: themeColor}}
           contentStyle={{color: themeColor}}
           style={{borderBottomColor: themeColor}}
           rightIconStyle={{marginRight: 30}}/>
          <PersonalPageCell
            title={
              <Text>邮箱<Text style={{fontSize: 14}}>{!emailVerified ? '(未验证)' : '(已验证)'}</Text></Text>
            }
            onPressCell={() => this.jumpToVerifyPage('verifyEmail', emailVerified, email)}
            content={email}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            }/>
          <PersonalPageCell
            title={
              <Text>手机号<Text style={{fontSize: 14}}>{!mobilePhoneVerified ? '(未验证)' : '(已验证)'}</Text></Text>
            }
            onPressCell={() => this.jumpToVerifyPage('verifyPhone', mobilePhoneVerified, mobilePhoneNumber)}
            content={mobilePhoneNumber}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            }/>
          <PersonalPageCell
            title='性别'
            onPressCell={() => this.showGenderSelect()}
            content={gender}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            }/>
          <PersonalPageCell
            title='生日'
            onPressCell={() => this.showBirthSelect()}
            content={birth === '' ? '未设置' : birth}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            }/>
          <PersonalPageCell
            title='修改密码'
            onPressCell={() => this.jumpToUpdateInfoPage('password')}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            }/>
          <PersonalPageCell
            title='设置'
            onPressCell={() => this.jumpToSettingsPage()}
            titleStyle={{color: themeColor}}
            contentStyle={{color: themeColor}}
            style={{borderBottomColor: themeColor}}
            rightIcon={
              <Image
                source={require('../images/right_arrow.png')}
                style={[commonStyle.rightIcon, {tintColor: themeColor}]}/>
            }/>
          <ButtonComponent
            onPressButton={() => this.logoutConfirm()}
            title='退出登录'
            buttonStyle={[styles.logoutButton, {backgroundColor: themeColor}]}/>
        </ScrollView>
        <LoadingIndicator
          visible={this.props.loading}
          color={themeColor}/>
        <ActionSheet
          ref={el => this.ActionSheet = el}
          title={<Text style={{fontSize: 18, color: themeColor}}>选择性别</Text>}
          tintColor={themeColor}
          options={this.actionSheetOptions}
          cancelButtonIndex={2}
          onPress={index => this.setGender(index)}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    backgroundColor: '#ccc',
    width: getScreenSize().width
  },
  avatarWrapper: {
    height: 110,
    paddingBottom: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: getScreenSize().width - 20,
    borderBottomWidth: 5 * getPixel(),
    backgroundColor: '#fff'
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginLeft: 20,
    marginTop: 20
  },
  username: {
    marginRight: 28
  },
  logoutButton: {
     marginBottom: 50,
     width: getScreenSize().width - 20,
     marginHorizontal: 10
  }
})

export default connect(state => ({
  user: state.user.user,
  isLoggedIn: state.user.isLoggedIn,
  loading: state.common.loading,
  refreshing: state.user.refreshing,
  themeColor: state.common.themeColor
}), dispatch => ({
  chatEntryAction: bindActionCreators(chatEntryActionCreator, dispatch),
  userAction: bindActionCreators(userActionCreator, dispatch)
}))(MeScreen)
