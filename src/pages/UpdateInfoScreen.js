import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActionCreator from '../store/actions/user';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import commonStyle from '../styles/common';
import notifyUtil from '../utils/notifyUtil';
import LoadingIndicator from '../components/LoadingIndicator';
import alert from '../config/alert';

class UpdateInfoScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      [props.type]: props.type !== 'password' ? props.user[props.type] : '',
      rePass: ''
    }
  }

  setChangeValue(value) {
    this.setState({
      [this.props.type]: value
    })
  }

  getModifyTitle() {
    const type = this.props.type
    switch(type) {
      case 'nick':
        return '昵称'
        break
      case 'username':
        return '用户名'
        break
      case 'password':
        return '密码'
        break
    }
  }

  onSubmit() {
    const type = this.props.type
    if(!this.state[type].replace(/\s/g, '')) {
      notifyUtil.toast(`${this.getModifyTitle()}不能为空`)
      return
    }

    if(type === 'password') {
      if(!this.state.rePass.replace(/\s/g, '')) {
        notifyUtil.toast(alert.registerErrorPasswordEmpty)
        return
      }

      if(this.state[type] !== this.state.rePass) {
        notifyUtil.toast(alert.registerErrorPasswordNoMatchRepass)
        return
      }
    }

    const data = this.state[type]
    const username = this.props.user.username
    this.props.userAction.startUpdateInfo({
      username,
      type,
      info: data
    })
  }

  renderRepass() {
    if(this.props.type === 'password') {
      return (
        <InputComponent
         placeholder='输入您的确认密码'
         placeholderTextColor={this.props.themeColor}
         style={{borderColor: this.props.themeColor}}
         onChangeText={rePass => this.setState({rePass})}/>
      )
    }
  }

  render() {
    const keyboardType = this.props.type === 'nick' ? 'default' : 'email-address'
    const defaultValue = this.props.type !== 'password' ? this.props.user[this.props.type] : ''
    return (
      <View style={[commonStyle.container, styles.container]}>
        <InputComponent
          onChangeText={value => this.setChangeValue(value)}
          placeholder={`输入您的新${this.getModifyTitle()}`}
          defaultValue={defaultValue}
          placeholderTextColor={this.props.themeColor}
          style={{borderColor: this.props.themeColor}}
          keyboardType={keyboardType}/>
        {
          this.renderRepass()
        }
        <ButtonComponent
          title='确定'
          buttonStyle={{backgroundColor: this.props.themeColor}}
          onPressButton={() => this.onSubmit()}/>
        <LoadingIndicator
          visible={this.props.loading}
          color={this.props.themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50
  }
})

export default connect(state => ({
  user: state.user.user,
  loading: state.common.loading
}), dispatch => ({
  userAction: bindActionCreators(userActionCreator, dispatch)
}))(UpdateInfoScreen)
