import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Modal,
  TextInput,
  Alert
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getScreenSize, getPixel, genUUID} from '../utils/common';
import notifyUtil from '../utils/notifyUtil';
import SortableList from 'react-native-sortable-list';
import LoadingIndicator from '../components/LoadingIndicator';
import {doGetuser} from '../service/user';
import {fetchContactList, modifyContact} from '../service/relationship';
import {startModifyContact} from '../store/actions/relationship';
import alert from '../config/alert';
const CELL_HEIGHT = 50

class DivideManageScreen extends Component {
  constructor(props) {
    super(props)
    this.divideRefs = {}
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      divide: [],
      loading: true,
      visible: false,
      divideName: '',
      openType: ''
    }
  }

  onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress' && event.id === 'done') {
      this.saveContactChange()
    }
  }

  componentWillMount() {
    this.fetchDivideList()
  }

  async saveContactChange() {
    const id = this.props.id
    const localDivide = this.state.divide
    const cloudDivide = this.getCloudDivideFromLocalDivide()
    this.props.modifyContactAction(id, cloudDivide, localDivide)
  }

  getCloudDivideFromLocalDivide() {
    const cloudDivide = []
    const divide = this.state.divide
    divide.forEach((item, index) => {
      const temp = {
        ...item,
        list: item.list.map(item => {
          return item.objectId
        })
      }
      cloudDivide.push(temp)
    })
    return cloudDivide
  }

  async fetchDivideList() {  //redux里的contact不能做任何改变，否则报错，只能重新获取数据
    const id = this.props.id
    try {
      const result = await doGetuser('_id', id, ['contact'])
      const {contact} = result.toJSON()
      const wrappedContact = contact.map(item => {
        return fetchContactList(item)
      })
      const divide = await Promise.all(wrappedContact)
      this.setState({
        divide,
        loading: false
      })
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  showAddDivideContainer() {
    this.setState({
      visible: true,
      openType: 'add'
    })
  }

  addDivide() {
    const divideName = this.state.divideName
    if(!divideName.replace(/\s/g, '')) {
      return
    }
    const uuid = genUUID()
    const newDivide = {
      label: divideName,
      name: uuid,
      list: []
    }
    const divide = [newDivide, ...this.state.divide]
    this.setState({
      divide,
      visible: false
    })
  }

  showModifyDivideNameContainer(index) {
    this.modifyDivideIndex = index
    const divideName = this.state.divide[index].label
    this.setState({
      visible: true,
      openType: 'modify',
      divideName
    })
  }

  modifyDivideName() {
    const divideName = this.state.divideName
    const index = this.modifyDivideIndex
    if(!divideName.replace(/\s/g, '')) {
      return
    }
    const divide = [...this.state.divide]
    divide[index].label = divideName
    this.setState({
      divide,
      visible: false
    })
  }

  removeDivide(index) {
    if(this.state.divide.length === 1) {
      notifyUtil.toast(alert.atLeastKeepOneDivide, 'long')
      return
    }

    const users = this.state.divide[index].list
    const divide = [...this.state.divide]
    divide.splice(index, 1)
    divide[0].list = divide[0].list.concat(users)
    this.setState({
      divide,
      visible: false
    })
  }

  getSortableData() {
    let temp = {}
    const divide = this.state.divide
    divide.forEach(item => {
      temp[item.name] = item
    })
    return temp
  }

  processDivideOrder(order) {
    const newDivide = []
    const divide = this.state.divide
    for(let i = 0, len = order.length;i < len;i++) {
      for(let j = 0, len = divide.length;j < len;j++) {
        if(order[i] === divide[j].name) {
          newDivide.push(divide[j])
          break
        }
      }
    }
    this.setState({
      divide: newDivide
    })
  }

  renderDivideItem({data: divide, index, key}) {
    const themeColor = this.props.themeColor
    return (
      <View
        style={[styles.divideItem, {borderBottomColor: themeColor}]}
        key={index}
        ref={el => this.divideRefs[key] = el}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity
            onPressIn={() => this.removeDivide(index)}>
            <Image
              source={require('../images/decrease.png')}
              style={[{width: 30, height: 30, marginRight: 20}, {tintColor: themeColor}]}/>
          </TouchableOpacity>
          <TouchableOpacity onPressIn={() => this.showModifyDivideNameContainer(index)}>
            <Text style={{color: themeColor, fontSize: 16}}>{divide.label}</Text>
          </TouchableOpacity>
        </View>
        <Image
          source={require('../images/menu.png')}
          style={[{width: 20, height: 20, marginRight: 10}, {tintColor: themeColor}]}/>
      </View>
    )
  }

  render() {
    const themeColor = this.props.themeColor
    const divideName = this.state.openType === 'add' ? '' : (this.state.openType === 'modify' ? this.state.divide[this.modifyDivideIndex].label : '')
    const data = this.state.divide
    const sortableData = this.getSortableData()
    return (
      <View style={{flex: 1}}>
        {
          this.state.divide.length > 0 ?
            <View style={styles.container}>
              <TouchableOpacity
                style={[styles.addDivide, {borderBottomColor: themeColor}]}
                onPress={() => this.showAddDivideContainer()}>
                <Text style={[{color: themeColor}, styles.addDivideTitle]}>添加分组</Text>
                <Image
                  source={require('../images/add.png')}
                  style={{width: 30, height: 30, tintColor: themeColor}}/>
              </TouchableOpacity>
              <SortableList
                contentContainerStyle={{height: getScreenSize().height}}
                data={sortableData}
                renderRow={this.renderDivideItem.bind(this)}
                onActivateRow={key => {
                  this.divideRefs[key].setNativeProps({backgroundColor: '#999'})
                }}
                onReleaseRow={key => {
                  this.divideRefs[key].setNativeProps({backgroundColor: '#fff'})
                }}
                onChangeOrder={nextOrder => {
                  this.processDivideOrder(nextOrder)
                }}/>
              <Modal
                onRequestClose={() => {}}
                visible={this.state.visible}>
                <View style={styles.modalWrapper}>
                  <View style={styles.contentWrapper}>
                    <View style={styles.content}>
                      <Text style={[styles.title, {color: themeColor}]}>请输入新的分组名称</Text>
                      <View style={styles.inputWrapper}>
                        <TextInput
                          maxLength={10}
                          underlineColorAndroid='transparent'
                          placeholder='输入新分组名称'
                          placeholderTextColor={themeColor}
                          selectionColor={themeColor}
                          style={{color: themeColor}}
                          autoFocus={true}
                          defaultValue={divideName}
                          onChangeText={value => this.setState({
                            divideName: value
                          })}/>
                      </View>
                    </View>
                    <View style={[styles.buttonsWrapper, {borderTopColor: themeColor}]}>
                      <TouchableOpacity
                        style={[styles.button, {borderRightColor: themeColor, borderRightWidth: getPixel() * 5}]}
                        onPress={() => this.setState({visible: false})}>
                        <Text style={{color: themeColor}}>取消</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                          if(this.state.openType === 'add') {
                            this.addDivide()
                          }else if(this.state.openType === 'modify') {
                            this.modifyDivideName()
                          }
                        }}>
                        <Text style={{color: themeColor}}>确定</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
            </View>
          :
            null
        }
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  addDivide: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 20,
    height: CELL_HEIGHT,
    borderBottomWidth: getPixel() * 5
  },
  addDivideTitle: {
    fontSize: 16,
    marginRight: 5
  },
  divideItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: CELL_HEIGHT,
    borderBottomWidth: getPixel() * 5
  },
  modalWrapper: {
    width: getScreenSize().width,
    height: getScreenSize().height,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentWrapper: {
    width: '80%',
    justifyContent: 'space-between',
    height: 170,
    backgroundColor: '#fff',
    borderRadius: 5,
    marginBottom: 100
  },
  content: {
    marginHorizontal: 15,
    marginTop: 20
  },
  inputWrapper: {
    height: 40,
    paddingLeft: 5,
    backgroundColor: '#ccc',
    borderRadius: 5
  },
  title: {
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 10
  },
  buttonsWrapper: {
    flexDirection: 'row',
    borderTopWidth: getPixel() * 5,
    height: CELL_HEIGHT
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: CELL_HEIGHT
  }
})

export default connect(null, dispatch => ({
  modifyContactAction: bindActionCreators(startModifyContact, dispatch)
}))(DivideManageScreen)
