import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  DeviceEventEmitter
} from 'react-native';
import commonStyle from '../styles/common';
import config from '../config';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import {checkCreateGroup} from '../utils/checkValidate';
import ImagePicker from 'react-native-image-picker';
import {createGroup} from '../service/group';
import LoadingIndicator from '../components/LoadingIndicator';
import notifyUtil from '../utils/notifyUtil';

export default class CreateGroupScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      groupId: '',
      nick: '',
      avatarUrl: config.defaultGroupAvatar
    }
  }

  modifyAvatar() {
    const options = {
      title: '选择图片',
      cancelButtonTitle: '取消',
      takePhotoButtonTitle: '拍照',
      chooseFromLibraryButtonTitle: '图库',
      storageOptions: {
        skipBackup: true,
        path: 'images',
        cameraRoll: true,
        waitUntilSaved: true
      },
      permissionDenied: {
        title: '失败',
        text: '您没有设备访问权限',
        reTryTitle: '重试',
        okTitle: '确定'
      }
    }

    ImagePicker.showImagePicker(options, response => {
      if(response.error) {
        Alert.alert('提示', `发生错误:${response.error}`, [
          {
            text: '确定'
          }
        ])
      }else if(!response.didCancel){
        this.setState({avatarUrl: response.uri})
        this.pic = response
      }
    })
  }

  async onCreateGroup() {
    if(checkCreateGroup(this.state.groupId, this.state.nick)) {
      try {
        this.setState({loading: true})
        const user = this.props.user
        const {groupId, nick} = this.state
        const pic = this.pic
        await createGroup(groupId, nick, user, pic)
        this.setState({loading: false})
        notifyUtil.toast('创建成功')
        DeviceEventEmitter.emit('refreshContact')
        this.props.navigator.dismissModal()
      }catch(error) {
        this.setState({loading: false})
        notifyUtil.toast(error.toString(), 'long')
      }
    }
  }

  render() {
    const themeColor = this.props.themeColor
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.avatarWrapper}
          onPress={() => this.modifyAvatar()}>
          <Image
            source={{uri: this.state.avatarUrl}}
            style={styles.avatar}/>
          <Text style={[styles.alert, {color: themeColor}]}>点击修改头像</Text>
        </TouchableOpacity>
        <InputComponent
          placeholder='请输入群组名'
          style={{borderBottomColor: themeColor}}
          placeholderTextColor={themeColor}
          keyboardType='email-address'
          maxLength={10}
          onChangeText={value => this.setState({groupId: value})}/>
        <InputComponent
          placeholder='请输入群组昵称'
          style={{borderBottomColor: themeColor}}
          placeholderTextColor={themeColor}
          maxLength={8}
          onChangeText={value => this.setState({nick: value})}/>
        <ButtonComponent
          title='创建'
          buttonStyle={{backgroundColor: themeColor}}
          onPressButton={() => this.onCreateGroup()}/>
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    paddingTop: 30
  },
  avatarWrapper: {
    alignItems: 'center'
  },
  avatar: {
    width: 80,
    height: 80,
    borderRadius: 40
  },
  alert: {
    marginTop: 5,
    fontSize: 16
  }
})
