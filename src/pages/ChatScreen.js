import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Keyboard,
  NativeAppEventEmitter,
  NativeModules
} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as conversationActionCreator from '../store/actions/conversation';
import {queryConversation} from '../service/conversation';
import {createClient, doGetuser} from '../service/user';
import * as conversationTypes from '../config/conversationType';
import ChatList from '../components/ChatList';
import ChatInput from '../components/ChatInput';
import notifyUtil from '../utils/notifyUtil';
import {
  sendTextMessage,
  sendImageMessage,
  sendAudioMessage
} from '../service/sendMessage';
import config from '../config';
import AMapLocation from 'react-native-smart-amap-location';
import {MusicBarLoader, TextLoader} from 'react-native-indicator';
import {getScreenSize} from "../utils/common";

class ChatScreen extends Component {
  constructor(props) {
    super(props)
    this.scrolToBottom = true
    this.state = {
      loadingChat: false,
      done: false,
      canPull: true,
      record: false,
      playRecord: false,
      duration: 0,
      chatList: [],
      playAudio: false
    }
  }

  componentWillMount() {
    this.fetchConversation()
    this.getCurrentLocation()
  }

  componentDidMount() {
    Keyboard.addListener('keyboardDidShow', () => {
      this.chatContent && this.chatContent.scrollToEnd()
    })
  }

  componentWillUnmount() {
    this.chatContent = null
    this.scrolToBottom = null
    this.conversation = null
    this.allContact = null
    this.messageIterator = null
    this.location = null
    AMapLocation.cleanUp()
  }

  getCurrentLocation() {
    AMapLocation.init(null)
    NativeAppEventEmitter.addListener('amap.location.onLocationResult', result => this.onLocationResult(result))
    AMapLocation.getReGeocode()
  }

  onLocationResult(result) {
    if(result) {
      const {province, district} = result
      this.location = province + district
    }
  }

  async fetchConversation() {
    const {conversationType, target} = this.props
    if(!global.client) {
      global.client = await createClient(this.props.user.username)
    }
    const conversation = await queryConversation(conversationType, target)
    if(conversation.length > 0) {
      this.conversation = conversation[0]
      this.messageIterator = this.conversation.createMessagesIterator({limit: config.fetchChatMessageCount})
      this.fetchChatList()
      this.getAllOfMyUsers()
    }
    this.setMessageListener()
  }

  setMessageListener() {
    if(this.conversation) {
      this.conversation.on('message', async message => {
        const receiveMessage = await this.processMessageWithUserInfo(message.toFullJSON())
        this.state.chatList.push(receiveMessage)
        this.setState({
          chatList: [...this.state.chatList]
        }, () => {
          this.judgeToScrollBottom()
          this.props.conversationAction.startClearUnreadStatus(this.conversation.toFullJSON(), conversationTypes.COMMON)
        })
      })
    }else {
      global.client.on('message', async (message, conversation) => {
        const conversationType = conversation.get('type')
        if(conversationType === conversationTypes.GROUP) {
          const groupId = conversation.get('groupId')
          if(groupId === this.props.target) {
            const receiveMessage = await this.processMessageWithUserInfo(message.toFullJSON())
            this.state.chatList.push(receiveMessage)
            this.setState({
              chatList: [...this.state.chatList]
            }, () => {
              this.judgeToScrollBottom()
            })
            this.conversation = conversation
            this.props.conversationAction.startClearUnreadStatus(conversation.toFullJSON(), conversationTypes.COMMON)
          }
        }else if(conversationType === conversationTypes.PRIVATE) {
          if(conversation.creator === this.props.target) {
            const receiveMessage = await this.processMessageWithUserInfo(message.toFullJSON())
            this.state.chatList.push(receiveMessage)
            this.setState({
              chatList: [...this.state.chatList]
            }, () => {
              this.judgeToScrollBottom()
            })
            this.conversation = conversation
            this.props.conversationAction.startClearUnreadStatus(conversation.toFullJSON(), conversationTypes.COMMON)
          }
        }
      })
    }
  }

  async fetchChatList() {
    if(this.messageIterator) {
      this.setState({loadingChat: true})
      const result = await this.messageIterator.next()
      if(!this.state.done) {
        const conversationType = this.props.conversationType
        const wrappedChatList = result.value.map((item, index, arr) => {
          return new Promise(async resolve => {
            const messageToJSON = item.toFullJSON()
            const user = await this.fetchSenderUserInfo(messageToJSON.from, conversationType)
            if(index < arr.length - 1) {
              if(item.timestamp.getTime() <= arr[index + 1].timestamp.getTime() - 1000 * 60 * config.showChatMessageDelay) {
                messageToJSON.showTime = true
              }
            }

            if(arr.length === 1) {
              messageToJSON.showTime = true
            }

            if(index === 0) {
              messageToJSON.showTime = true
            }

            messageToJSON.user = {
              username: user.username,
              nick: user.nick,
              avatar: user.avatar,
              email: user.email,
              gender: user.gender,
              canBeSearched: user.canBeSearched,
              birth: user.canBeSearched
            }
            resolve(messageToJSON)
          })
        })
        const chatList = await Promise.all(wrappedChatList)
        const done = result.done
        const newChatlist = chatList.concat(this.state.chatList)
        this.setState({
          chatList: [...newChatlist],
          done,
          loadingChat: false
        })
      }else {
        this.setState({loadingChat: false})
      }
    }
  }

  getAllOfMyUsers() {
    const users = []
    this.props.contact.forEach(divide => {
      divide.list.forEach(user => {
        users.push(user)
      })
    })
    this.allContact = [...users]
  }

  async fetchSenderUserInfo(username, conversationType) {
    if(conversationType === conversationTypes.PRIVATE || conversationType === conversationTypes.GROUP) {
      if(username === this.props.user.username) {
        return this.props.user
      }

      if(!this.allContact) {
        this.getAllOfMyUsers()
      }

      const index = this.allContact.findIndex(item => item.username === username)
      if(index !== -1) {
        return this.allContact[index]
      }else {
        try {
          const result = await doGetuser('username', username, ['username', 'nick', 'email', 'mobilePhoneNumber', 'avatar', 'gender', 'canBeSearched', 'birth'])
          return result.toJSON()
        }catch(error) {
          return
        }
      }
    }
  }

  getDirection(senderUsername) {
    return senderUsername !== this.props.user.username ? 'left' : 'right'
  }

  async createConversation() {
    const target = this.props.target
    if(this.props.conversationType === conversationTypes.PRIVATE) {
      return global.client.createConversation({
        members: [target],
        name: '',
        transient: false,
        unique: false,
        type: conversationTypes.PRIVATE
      })
    }else if(this.props.conversationType === conversationTypes.GROUP) {
      const group = this.props.group
      const index = group.list.findIndex(item => item.groupId === target)
      if(index !== -1) {
        const memberIds = group.list[index].members
        if(!this.allContact) {
          this.getAllOfMyUsers()
        }
        const memberUsernames = []
        const notInMyContactIds = []
        memberIds.forEach(id => {
          const index = this.allContact.findIndex(user => user.objectId === id)
          if(index !== -1) {
            memberUsernames.push(this.allContact[index].username)
          }else {
            notInMyContactIds.push(id)
          }
        })
        let members = []
        if(notInMyContactIds.length > 0) {
          const wrappedUserIds = notInMyContactIds.map(item => {
            return new Promise(async resolve => {
              try {
                const result = await doGetuser('_id', item, ['username'])
                resolve(result.get('username'))
              }catch(error) {
                resolve()
              }
            })
          })

          const usernames = await Promise.all(wrappedUserIds)
          const filterUsernames = usernames.filter(item => item)
          members = memberUsernames.concat(filterUsernames)
        }else {
          members = memberUsernames
        }
        return global.client.createConversation({
          members,
          name: '',
          transient: false,
          unique: false,
          type: conversationTypes.GROUP,
          groupId: target
        })
      }
    }
  }

  async processMessageWithUserInfo(message) {
    if(this.state.chatList.length === 0) {
      message.showTime = true
    }else if(message.timestamp - 1000 * 60 * config.showChatMessageDelay > this.state.chatList[this.state.chatList.length - 1].timestamp) {
      message.showTime = true
    }
    const sendUserInfo = await this.fetchSenderUserInfo(message.from, this.props.conversationType)
    message.user = {
      ...sendUserInfo
    }
    return message
  }

  async sendTextMessage(text) {
    if(text.replace(/\s/g, '')) {
      try {
        if(!this.conversation) {
          this.conversation = await this.createConversation()
        }
        const result = await sendTextMessage(text, this.conversation, {
          senderUserNick: this.props.user.nick
        })

        const sendMessage = await this.processMessageWithUserInfo(result.toFullJSON())
        this.state.chatList.push(sendMessage)
        this.setState({
          chatList: [...this.state.chatList]
        }, () => {
          this.scrollToBottomHandler && clearTimeout(this.scrollToBottomHandler)
          this.scrollToBottomHandler = setTimeout(() => {
            this.judgeToScrollBottom()
          }, 200)
        })
        this.props.conversationAction.modifyConversation(this.conversation.toFullJSON())
      }catch(error) {
        console.log(error)
        notifyUtil.toast(error.toString(), 'long')
      }
    }
  }

  async sendImageMessage(pic) {
    try {
      if(!this.conversation) {
        this.conversation = await this.createConversation()
      }
      const result = await sendImageMessage(pic, this.conversation, {
        senderUserNick: this.props.user.nick,
        senderUserName: this.props.user.username,
        location: this.location
      })
      const sendMessage = await this.processMessageWithUserInfo(result.toFullJSON())
      this.state.chatList.push(sendMessage)
      this.setState({
        chatList: [...this.state.chatList]
      }, () => {
        this.scrollToBottomHandler && clearTimeout(this.scrollToBottomHandler)
        this.scrollToBottomHandler = setTimeout(() => {
          this.judgeToScrollBottom()
        }, 200)
      })
      this.props.conversationAction.modifyConversation(this.conversation.toFullJSON())
    }catch(error) {
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  async sendAudioMessage(audioPath) {
    try {
      if(!this.conversation) {
        this.conversation = await this.createConversation()
      }

      const duration = this.state.duration
      const result = await sendAudioMessage(audioPath, this.conversation, {
        senderUserNick: this.props.user.nick,
        senderUserName: this.props.user.username
      }, duration)
      const sendMessage = await this.processMessageWithUserInfo(result.toFullJSON())
      this.state.chatList.push(sendMessage)
      this.setState({
        chatList: [...this.state.chatList]
      }, () => {
        this.scrollToBottomHandler && clearTimeout(this.scrollToBottomHandler)
        this.scrollToBottomHandler = setTimeout(() => {
          this.judgeToScrollBottom()
        }, 200)
      })
      this.props.conversationAction.modifyConversation(this.conversation.toFullJSON())
    }catch(error) {
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  judgeToScrollBottom() {
    if(this.scrolToBottom) {
      this.chatContent.scrollToEnd()
    }
  }

  judgeCanScroll(event) {
    const offsetY = event.nativeEvent.contentOffset.y
    const scrollViewHeight = event.nativeEvent.layoutMeasurement.height
    const contentSizeHeight = event.nativeEvent.contentSize.height
    if(offsetY + scrollViewHeight <= contentSizeHeight - 60) {
      this.scrolToBottom = false
    }else {
      this.scrolToBottom = true
    }
  }

  renderChatList(message, index) {
    const themeColor = this.props.themeColor
    const conversationType = this.props.conversationType
    const views = this.state.chatList.map((message, index) => {
      const username = message.from
      const direction = this.getDirection(username)
      return (
        <ChatList
          key={index}
          direction={direction}
          themeColor={themeColor}
          message={message}
          conversationType={conversationType}
          onAudioPlayChange={status => {
            this.setState({
              playAudio: status
            })
          }}/>
      )
    })

    return views
  }

  renderRecordIndicator() {
    const themeColor = this.props.themeColor
    return (
      this.state.record ?
        <View style={styles.recordIndicator}>
          <MusicBarLoader
            color={themeColor}
            barWidth={5}
            barHeight={50}/>
          {
            this.state.duration > 0 ?
              <TextLoader
                text={`${parseInt(this.state.duration)}秒`}
                textStyle={{color: themeColor, fontSize: 16}}/>
            :
              null
          }
        </View>
      :
        null
    )
  }

  renderAudioPlayIndicator() {
    const themeColor = this.props.themeColor
    return (
      this.state.playAudio || this.state.playRecord ?
        <View style={styles.audioIndicator}>
            <MusicBarLoader
              color={themeColor}
            />
            <TextLoader
              text='播放语音中'/>
        </View>
      :
        null
    )
  }

  render() {
    const themeColor = this.props.themeColor
    return (
      <View style={styles.container}>
        <View style={{flex: 1, paddingTop: 5}}>
          <ScrollView
            ref={el => this.chatContent = el}
            contentContainerStyle={{paddingBottom: 5}}
            showsVerticalScrollIndicator={true}
            overScrollMode='always'
            onContentSizeChange={() => this.judgeToScrollBottom()}
            onMomentumScrollEnd={event => this.judgeCanScroll(event)}
            refreshControl={
              <RefreshControl
                refreshing={this.state.loadingChat}
                onRefresh={() => this.fetchChatList()}
                colors={[themeColor]}/>
            }>
              {
                this.renderChatList()
              }
          </ScrollView>
        </View>
        <ChatInput
          containerStyle={{backgroundColor: themeColor}}
          themeColor={themeColor}
          playRecord={this.state.playRecord}
          onSendTextMessage={text => this.sendTextMessage(text)}
          onSendImageMessage={pic => this.sendImageMessage(pic)}
          onSendAudioMessage={audioPath => this.sendAudioMessage(audioPath)}
          onRecordDuration={duration => this.setState({duration})}
          onRecord={status => this.setState({record: status})}
          stopPlayRecord={() => this.audioObj && this.audioObj.stop()}/>
        {
          this.renderRecordIndicator()
        }
        {
          this.renderAudioPlayIndicator()
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  pullWrapper: {
    height: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: -20
  },
  loading: {
    height: 20,
    width: 20
  },
  recordIndicator: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)'
  },
  audioIndicator: {
    position: 'absolute',
    width: 200,
    height: 50,
    top: getScreenSize().height / 2 - 60,
    left: getScreenSize().width / 2 - 13
  }
})

export default connect(state => ({
  user: state.user.user,
  contact: state.relationship.contact,
  group: state.relationship.group
}), dispatch => ({
  conversationAction: bindActionCreators(conversationActionCreator, dispatch)
}))(ChatScreen)
