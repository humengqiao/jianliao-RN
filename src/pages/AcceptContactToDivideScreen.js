import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity
} from 'react-native';
import {connect} from 'react-redux';
import commonStyle from '../styles/common';
import Empty from '../components/Empty';
import {getPixel} from '../utils/common';

class AcceptContactToDivideScreen extends Component {
  constructor(props) {
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this))
    this.state = {
      selectDivideIndex: 0,
      contacts: this.getAllDivides(this.props.contact)
    }
  }

  onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress' && event.id === 'done') {
      const selectDivide = this.state.contacts[this.state.selectDivideIndex]
      this.props.acceptContact({
        label: selectDivide.label,
        name: selectDivide.name
      })
    }
  }

  selectDivide(divide, index) {
    if(index !== this.state.selectDivideIndex) {
      this.state.contacts[this.state.selectDivideIndex].memberCount --
      this.setState({selectDivideIndex: index})
    }
  }

  renderDivide(divide, index) {
    const themeColor = this.props.themeColor
    const currentIndex = this.state.selectDivideIndex
    return (
      <TouchableOpacity
        style={[styles.divideWrapper, {borderBottomColor: themeColor}]}
        onPress={() => this.selectDivide(divide, index)}>
        <Text style={{color: themeColor}}>
          {divide.label}
          (
            {
              currentIndex === index ?
                ++ divide.memberCount
              :
                divide.memberCount
            }
              人
          )
        </Text>
        {
          this.state.selectDivideIndex === index ?
            <Image
              source={require('../images/correct.png')}
              style={[styles.selectIcon, {tintColor: themeColor}]}/>
          :
            null
        }
      </TouchableOpacity>
    )
  }

  getAllDivides(contatcs) {
    const divideNames = []
    contatcs.map(item => {
      divideNames.push({
        label: item.label,
        name: item.name,
        memberCount: item.list.length
      })
    })
    return divideNames
  }

  render() {
    const themeColor = this.props.themeColor
    const contacts = this.state.contacts
    return (
      <View style={styles.container}>
        <FlatList
          data={contacts}
          keyExtractor={(item, index) => index}
          ListEmptyComponent={<Empty title='暂无分组' titleStyle={commonStyle.empty} themeColor={themeColor}/>}
          renderItem={({item, index}) => this.renderDivide(item, index)}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10
  },
  divideWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 10,
    borderBottomWidth: getPixel() * 5
  },
  selectIcon: {
    width: 30,
    height: 30
  }
})

export default connect(state => ({
  contact: state.relationship.contact
}), null)(AcceptContactToDivideScreen)
