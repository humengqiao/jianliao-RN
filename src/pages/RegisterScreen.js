import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  ActivityIndicator,
  DatePickerAndroid,
  TouchableOpacity,
  TouchableHighlight,
  Keyboard
} from 'react-native';
import {getScreenSize, genBirth} from '../utils/common';
import {formatLeancloudErrorMsg} from '../utils/format';
import {checkRegisterValidate} from '../utils/checkValidate';
import InputComponent from '../components/InputComponent';
import ButtonComponent from '../components/ButtonComponent';
import {doRegister} from '../service/user';
import alertErrorText from '../config/alert';
import config from '../config';
import commonStyle from '../styles/common';
import notifyUtil from '../utils/notifyUtil';
import ImagePicker from 'react-native-image-picker';
import LoadingIndicator from '../components/LoadingIndicator';
import errorTranslate from '../utils/leancloudErrorMsg';

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      phoneStatusText: config.phoneStatusText,
      username: '',
      password: '',
      rePassword: '',
      birth: config.selectBirthTitle,
      nick: '',
      email: '',
      phoneNumber: '',
      agreeRule: true
    }
  }

  async selectDate() {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: new Date(config.birthYear, config.birthMonth, config.birthDay),
        minDate: new Date(1950, 1, 1),
        maxDate: new Date(),
        mode: 'spinner'
      })

      if (action !== DatePickerAndroid.dismissedAction) {
        const birth = genBirth(year, month, day)
        this.setState({
          birth
        })
      }
    }catch({code, message}) {
      notifyUtil.toast(message, 'long')
    }
  }

  async register() {
    Keyboard.dismiss()
    let {username, password, rePassword, nick, phoneNumber, email, birth} = this.state
    if(checkRegisterValidate(username, password, rePassword, nick, phoneNumber, email, birth)) {
      if(!this.state.agreeRule) {
        notifyUtil.toast(alertErrorText.agreeRule, 'long')
        return
      }

      birth = birth === config.selectBirthTitle ? '' : birth
      this.setState({
        loading: true
      })
      try {
        await doRegister(username, password, nick, email, birth, phoneNumber)
        this.setState({
          loading: false
        })
        notifyUtil.toast('注册成功', 'long')
        this.props.navigator.showModal({
          screen: 'jianliao.VerifyScreen',
          title: '验证手机号码',
          animationType: 'slide-up',
          navigatorStyle: {
            navBarTitleTextCentered: true,
            navBarBackgroundColor: this.props.themeColor,
            navBarTextColor: '#fff',
            screenBackgroundColor: '#fff'
          },
          navigatorButtons: {
            leftButtons: [
              {
                id: 'back',
                buttonColor: '#fff'
              }
            ]
          },
          passProps: {
            showCaptcha: config.enableCaptchaVerify,
            type: 'verifyPhone',
            numberOrEmail: phoneNumber,
            themeColor: this.props.themeColor,
            from: 'RegisterScreen'
          }
        })
      }catch(error) {
        this.setState({
          loading: false
        })
        notifyUtil.toast(errorTranslate(error), 'long')
      }
    }
  }

  toggleAgreeRule() {
    const agreeRule = !this.state.agreeRule
    this.setState({
      agreeRule
    })
  }

  jumpToRulePage() {
    this.props.navigator.push({
      screen: 'jianliao.RuleScreen',
      title: '用户使用协议',
      navigatorStyle: {
        screenBackgroundColor: '#fff',
        navBarTitleTextCentered: true,
        navBarBackgroundColor: this.props.themeColor,
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
      },
      animationType: 'slide-up',
      navigatorButtons: {
        fab: {
          collapsedId: 'ruleMenuCollapsed',
          collapsedIcon: require('../images/menu.png'),
          expendedId: 'ruleMenuExpended',
          expendedIcon: require('../images/menu_vertical.png'),
          backgroundColor: this.props.themeColor,
          actions: [
            {
              id: 'help',
              icon: require('../images/help.png'),
              backgroundColor: this.props.themeColor
            },
            {
              id: 'share',
              icon: require('../images/share.png'),
              backgroundColor: this.props.themeColor
            }
          ]
        }
      }
    })
  }

  render() {
    const checkIcon = this.state.agreeRule ? require('../images/checked.png') : require('../images/check.png')

    return(
      <View style={styles.container}>
        {
          this.state.existUser ? <View></View> : null
        }
        <View style={styles.inputWrapper}>
          <InputComponent
            placeholder='请输入您的用户名'
            placeholderTextColor={this.props.themeColor}
            keyboardType='email-address'
            maxLength={12}
            autoFocus={true}
            style={{borderColor: this.props.themeColor}}
            onChangeText={username => {
              this.setState({
                username
              })
            }}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <InputComponent
            placeholder='请输入您的密码'
            placeholderTextColor={this.props.themeColor}
            keyboardType='email-address'
            style={{borderColor: this.props.themeColor}}
            onChangeText={password => {
              this.setState({
                password
              })
            }}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <InputComponent
            placeholder='请输入您的确认密码'
            placeholderTextColor={this.props.themeColor}
            style={{borderColor: this.props.themeColor}}
            keyboardType='email-address'
            onChangeText={rePassword => {
              this.setState({
                rePassword
              })
            }}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <InputComponent
            placeholder='请输入您的昵称'
            placeholderTextColor={this.props.themeColor}
            style={{borderColor: this.props.themeColor}}
            onChangeText={nick => {
              this.setState({
                nick
              })
            }}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <InputComponent
            placeholder='请输入您的邮箱'
            placeholderTextColor={this.props.themeColor}
            style={{borderColor: this.props.themeColor}}
            keyboardType='email-address'
            onChangeText={email => {
              this.setState({
                email
              })
            }}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
          <TouchableOpacity onPress={() => this.selectDate()}>
            <View style={[styles.birthItem, {borderBottomColor: this.props.themeColor}]}>
              <Text style={[styles.birth, {color: this.state.birth === config.selectBirthTitle ? this.props.themeColor : '#000'}]}>{this.state.birth}</Text>
              <Image
                source={require('../images/datepicker.png')}
                style={{width: 15, height: 15, tintColor: this.props.themeColor}}/>
            </View>
          </TouchableOpacity>
          <InputComponent
            placeholder='请输入您的手机号码'
            placeholderTextColor={this.props.themeColor}
            style={{borderColor: this.props.themeColor}}
            keyboardType='numeric'
            maxLength={11}
            onChangeText={phoneNumber => {
              this.setState({
                phoneNumber
              })
            }}
            rightElement={() => <View>
              <Text style={{fontSize: 12, color: this.props.themeColor}}>必填</Text>
            </View>}/>
        </View>
        <ButtonComponent
          activeOpacity={0.7}
          onPressButton={() => this.register()}
          buttonStyle={{backgroundColor: this.props.themeColor}}
          delayLongPress={300}
          disabled={this.state.loading}
          title='注 册'/>
        <View style={styles.rulesWrapper}>
          <TouchableOpacity onPress={() => this.toggleAgreeRule()}>
            <Image
              source={checkIcon}
              style={[styles.checkIcon, {tintColor: this.state.agreeRule ? this.props.themeColor : '#000'}]}/>
          </TouchableOpacity>
          <Text style={{color: this.props.themeColor}}>同意</Text>
          <TouchableOpacity
            style={styles.rule}
            onPress={() => this.jumpToRulePage()}>
            <Text style={{color: this.props.themeColor}}>《用户使用协议》</Text>
          </TouchableOpacity>
        </View>
        <LoadingIndicator
          visible={this.state.loading}
          color={this.props.themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  inputWrapper: {
    marginTop: 50,
    alignItems: 'center'
  },
  birthItem: {
    borderBottomWidth: 2,
    width: getScreenSize().width * 0.8,
    height: 50,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  birth: {
     fontSize: 16,
     marginLeft: 5
  },
  rulesWrapper: {
    flexDirection: 'row',
    height: 30,
    width: getScreenSize().width * 0.8,
    alignItems: 'center'
  },
  checkIcon: {
    width: 15,
    height: 15,
    marginRight: 5
  },
  indicator: {
    position: 'absolute',
    left: getScreenSize().width / 2 - 18,
    top: 200
  }
})
