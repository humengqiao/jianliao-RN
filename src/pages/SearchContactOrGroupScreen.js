import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Keyboard
} from 'react-native';
import InputComponent from '../components/InputComponent';
import LoadingIndicator from '../components/LoadingIndicator';
import notifyUtil from '../utils/notifyUtil';
import {searchContact} from '../service/user';
import {searchGroup} from '../service/group';

export default class SearchContactScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      keywords: ''
    }
  }

  judgeInMyContact(targetId, myContact) {
    let flag = false
    myContact.forEach(item => {
      const index = item.list.findIndex(item => item.objectId === targetId)
      if(index !== -1) {
        flag = true
      }
    })
    return flag
  }

  jumpToDetailPage(searchResult) {
    const type = this.props.type
    const themeColor = this.props.themeColor
    if(type === 'contact') {
      const {target: contact, inMyContact, isMe} = searchResult
      this.props.navigator.push({
        screen: 'jianliao.ContactInfoDetailScreen',
        title: '联系人资料详情',
        animationType: 'slide-up',
        navigatorStyle: {
          navBarTitleTextCentered: true,
          navBarBackgroundColor: themeColor,
          navBarTextColor: '#fff',
          screenBackgroundColor: '#fff'
        },
        navigatorButtons: {
          leftButtons: [
            {
              id: 'back',
              buttonColor: '#fff'
            }
          ]
        },
        passProps: {
          themeColor,
          contact,
          canSendMessage: inMyContact,
          isMe,
          origin: '搜索'
        }
      })
    }else if(type === 'group') {
      const {target: group, inMyGroup} = searchResult
      this.props.navigator.push({
        screen: 'jianliao.GroupInfoDetailScreen',
        title: '群组资料详情',
        animationType: 'slide-up',
        navigatorStyle: {
          navBarTitleTextCentered: true,
          navBarBackgroundColor: themeColor,
          navBarTextColor: '#fff',
          screenBackgroundColor: '#fff'
        },
        navigatorButtons: {
          leftButtons: [
            {
              id: 'back',
              buttonColor: '#fff'
            }
          ]
        },
        passProps: {
          themeColor,
          group,
          canSendMessage: inMyGroup,
          origin: '搜索'
        }
      })
    }
  }

  async doSearch() {
    if(this.state.keywords.replace(/\s/g, '') === '') {
      return
    }

    Keyboard.dismiss()
    const keywords = this.state.keywords
    const myId = this.props.myId
    this.setState({loading: true})
    try {
      let result
      if(this.props.type === 'contact') {
        result = await searchContact(keywords, myId)
      }else if(this.props.type === 'group') {
        result = await searchGroup(keywords, myId)
      }

      if(!result) {
        Alert.alert(
          '提示',
          `没有找到${this.props.type === 'contact' ? '联系人' : '群组'}`,
          [
            {text: '确定', onPress: () => {}}
          ]
        )
        this.setState({loading: false})
        return
      }

      this.setState({loading: false})
      this.jumpToDetailPage(result)
    }catch(error) {
      this.setState({loading: false})
      notifyUtil.toast(error.toString(), 'long')
    }
  }

  render() {
    const themeColor = this.props.themeColor
    const placeholder = this.props.type === 'contact' ? '输入您要搜索的联系人' : '输入您要搜索的群组'
    return (
      <View style={styles.container}>
        <View style={styles.inner}>
          <InputComponent
            placeholder={placeholder}
            onChangeText={value => this.setState({keywords: value})}
            style={{borderBottomColor: themeColor}}
            placeholderTextColor={themeColor}
            autoFocus={true}/>
          <TouchableOpacity
            style={styles.searchButton}
            onPress={() => this.doSearch()}>
            <Text style={[styles.searchText, {color: themeColor}]}>搜索</Text>
          </TouchableOpacity>
        </View>
        <LoadingIndicator
          visible={this.state.loading}
          color={themeColor}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inner: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  searchButton: {
    marginLeft: 10,
    marginBottom: 10
  },
  searchText: {
    fontSize: 16
  }
})
