import config from '../config';
import alertErrorText from '../config/alert';
import notifyUtil from '../utils/notifyUtil';

export function checkRegisterValidate(username, password, rePassword, nick, phoneNumber, email, birth) {
  if(username.replace(/\s+/g, '') === '') {
    notifyUtil.toast(alertErrorText.registerErrorUserNameEmpty)
    return false
  }else if(/\s/g.test(username) || /^_/.test(username.replace(/\s+/g, '')) || /[^\d\w]/.test(username.replace(/\s+/g, ''))){
    notifyUtil.toast(alertErrorText.registerErrorUserNameFormat)
    return false
  }else if(username.length < config.minUsernameLength || username.length > config.maxUsernameLength) {
    notifyUtil.toast(alertErrorText.registerErrorUserNameLength)
    return false
  }else if(password.replace(/\s+/g, '') === ''){
    notifyUtil.toast(alertErrorText.registerErrorPasswordEmpty)
    return false
  }else if(password === username) {
    notifyUtil.toast(alertErrorText.registerErrorPasswordSameWithUsername)
    return false
  }else if(password !== rePassword) {
    notifyUtil.toast(alertErrorText.registerErrorPasswordNoMatchRepass)
    return false
  }else if(nick.replace(/\s+/g, '') === '') {
    notifyUtil.toast(alertErrorText.registerErrorNickEmpty)
    return false
  }else if(email.replace(/\s+/g, '') === ''){
    notifyUtil.toast(alertErrorText.registerErrorEmailEmpty)
    return false
  }else if(!/^[A-Za-z0-9\u4e00-\u9fa5]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(email)) {
    notifyUtil.toast(alertErrorText.registerErrorEmailNotValidate)
    return false
  }else if(phoneNumber.replace(/\s+/g, '') === '') {
    notifyUtil.toast(alertErrorText.registerErrorPhoneEmpty)
    return false
  }else if(!/\d{11}/.test(phoneNumber) || !/^1[34578]\d{9}$/.test(phoneNumber)) {
    notifyUtil.toast(alertErrorText.registerErrorPhoneNotValidate)
    return false
  }
  return true
}

export function checkFeedbackValidate(name, phonenumber, qq, feedback) {
  if(!name.replace(/\s/g, '')) {
    notifyUtil.toast('您未填写姓名', 'long')
    return false
  }

  if(!phonenumber.replace(/\s/g, '')) {
    notifyUtil.toast('您未填写手机号码', 'long')
    return false
  }

  if(/[^\d]/g.test(phonenumber)) {
    notifyUtil.toast('手机号码不能有非数字', 'long')
    return false
  }

  if(qq.replace(/\s/g, '') && /[^\d]/g.test(qq)) {
    notifyUtil.toast('QQ号码不能有非数字', 'long')
    return false
  }

  if(!feedback.replace(/\s/g, '')) {
    notifyUtil.toast('您未填写反馈意见', 'long')
    return false
  }
  return true
}

export function checkCreateGroup(groupId, nick) {
  if(!groupId.replace(/\s/g, '')) {
    notifyUtil.toast('群组名不能为空', 'long')
    return false
  }

  if(!nick.replace(/\s/g, '')) {
    notifyUtil.toast('群组昵称不能为空', 'long')
    return false
  }

  if(/[^\w]/g.test(groupId)) {
    notifyUtil.toast('群组名只能为数组或字母', 'long')
    return false
  }

  return true
}
