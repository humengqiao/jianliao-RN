import * as customMessageTypes from '../config/customMessageType';
import * as conversationTypes from '../config/conversationType';

export function getContactNotifyContent(contactNotify, username) {
  if(contactNotify.length === 0) {
    return '暂无联系人通知'
  }


  let newestContactNotify
  for(let i = 0, len = contactNotify.length;i < len;i++) {   //bug，有时候发出的消息lastMessage没有内容，加个判断防止报错
    if(contactNotify[i].lastMessage) {
      newestContactNotify = contactNotify[i]
      break
    }
  }

  if(!newestContactNotify) {
    return '暂无联系人通知'
  }

  const {type: messageType, senderUserNick} = newestContactNotify.lastMessage.data._lcattrs

  switch(messageType) {
    case customMessageTypes.ADD_CONTACT:
      return newestContactNotify.lastMessage.from === username ? '您的添加请求等待回复' : `${senderUserNick}申请添加您`
      break
    case customMessageTypes.ACCEPT_CONTACT:
      return newestContactNotify.lastMessage.from === username ? `您已同意${senderUserNick}的添加申请` : `${senderUserNick}已同意您的添加申请`
      break
    case customMessageTypes.REFUSE_CONTACT:
      return newestContactNotify.lastMessage.from === username ? `您已拒绝${senderUserNick}的添加申请` : `${senderUserNick}已拒绝您的添加申请`
      break
    default:
      return ''
  }
}

export function getGroupNotifyContent(groupNotify, username) {
  if(groupNotify.length === 0) {
    return '暂无群通知'
  }

  const newestGroupNotify = groupNotify[0]
  const from = newestGroupNotify.lastMessage.from
  const {type: messageType, senderUserNick, targetUserNick, groupName} = newestGroupNotify.lastMessage.data._lcattrs
  switch(messageType) {
    case customMessageTypes.ADD_GROUP:
      return newestGroupNotify.lastMessage.from === username ? `您申请加入${groupName}` : `${senderUserNick}申请加入${groupName}`
      break
    case customMessageTypes.ACCEPT_JOINGROUP:
      return newestGroupNotify.lastMessage.from === username ? `您同意${targetUserNick}加入${groupName}` : `${senderUserNick}同意您加入${groupName}`
      break
    case customMessageTypes.REFUSE_JOINGROUP:
      return newestGroupNotify.lastMessage.from === username ? `您拒绝${targetUserNick}加入${groupName}` : `${senderUserNick}拒绝您加入${groupName}`
      break
    case customMessageTypes.BEKICKED_GROUP:
      return newestGroupNotify.lastMessage.from === username ? `您已从${groupName}踢出${targetUserNick}` : `您已被${senderUserNick}踢出${groupName}`
      break
    case customMessageTypes.SELFEXIT_GROUP:
      return `${processSenderUserNick}已退出${groupName}`
    default:
      return ''
  }
}

export function getUnreadMessageCount(conversations) {
  return conversations.reduce((totalCount, item) => {
    return totalCount += item.unreadMessagesCount
  }, 0)
}

export function filterConversation(conversationList, hideConversationList) {
  if(hideConversationList.length === 0) {
    return conversationList
  }

  const filterConversation = []
  conversationList.forEach(item => {
    const index = hideConversationList.findIndex(hideItem => hideItem === item.id)
    if(index === -1) {
      filterConversation.push(item)
    }
  })
  return filterConversation
}

export function getContactMessageTypeContent(messageType, creator, myUsername) {
  switch(messageType) {
    case customMessageTypes.ADD_CONTACT:
      return myUsername === creator ? '等待ta回复' : '未处理'
      break
    case customMessageTypes.ACCEPT_CONTACT:
      return '已同意'
      break
    case customMessageTypes.REFUSE_CONTACT:
      return '已拒绝'
      break
    default:
      return ''
  }
}

export function getGroupMessageTypeContent(messageType, creator, myUsername) {
  switch(messageType) {
    case customMessageTypes.ADD_GROUP:
      return myUsername === creator ? '等待ta回复' : '未处理'
      break
    case customMessageTypes.ACCEPT_JOINGROUP:
      return '已同意'
      break
    case customMessageTypes.REFUSE_JOINGROUP:
      return '已拒绝'
      break
    default:
      return ''
  }
}
