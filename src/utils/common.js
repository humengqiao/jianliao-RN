import {
  Dimensions,
  PixelRatio,
  ToastAndroid
} from 'react-native';
import config from '../config';
import alertErrorText from '../config/alert';
import {formatDateFromObj} from './format';
import DeviceInfo from 'react-native-deviceinfo';
import notifyUtil from '../utils/notifyUtil';

export function genFetchPostParams(params) {
  let queryStr = ''
  for(let key in params) {
    queryStr += `${key}=${params[key]}&`
  }
  return queryStr.substr(0, queryStr.length - 1)
}

export function getScreenSize() {
  return {width, height} = Dimensions.get('window')
}

export function getPixel() {
  return 1 / PixelRatio.get()
}

export function countDown(seconds, fn, interval = 1000) {
  return new Promise((resolve, reject) => {
    let handler = setInterval(() => {
      if(seconds * 1000 < 0) {
        clearInterval(handler)
        resolve()
      }else {
        fn(seconds)
        seconds--
      }
    }, interval)
  })
}

export function delay(ms){
  return new Promise(resolve => setTimeout(resolve, ms))
}

export function getDeviceId() {
  return DeviceInfo.getUniqueID()
}

export function genBirth(year, month, day) {
  return `${year}/${(month + 1) < 10 ? '0' + (month + 1) : (month + 1)}/${day < 10 ? '0' + day : day}`
}

export function genUUID(len = 8, radix = 16) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    var uuid = [], i;
    radix = radix || chars.length;

    if (len) {
      // Compact form
      for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
    } else {
      // rfc4122, version 4 form
      var r;

      // rfc4122 requires these characters
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
      uuid[14] = '4';

      // Fill in random data.  At i==19 set the high bits of clock sequence as
      // per rfc4122, sec. 4.1.5
      for (i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | Math.random()*16;
          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }

    return uuid.join('');
}
