import AV from 'leancloud-storage';

export function signatureFactory(clientId) {
  return AV.Cloud.rpc('loginSign', {clientId})
}

export function conversationSignatureFactory(conversationId, clientId, targetIds, action) {
  return AV.Cloud.rpc('startConversation', {
    conversationId: conversationId,
    clientId: clientId,
    members: targetIds,
    action: action
  })
}
