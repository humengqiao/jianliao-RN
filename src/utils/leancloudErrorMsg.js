export default function getErrorMsg(error) {
  switch(error.code) {
    case 0:
      return 'WebSocket 正常关闭，可能发生在服务器重启，或本地网络异常的情况。SDK 会自动重连，无需人工干预'
      break
    case 1:
      return '服务器内部错误或者参数错误，一般是因为传入了错误的参数，或者没有在本文档里明确定义的运行时错误，都会以代码 1 指代'
      break
    case 100:
      return '无法建立 TCP 连接到 LeanCloud 服务器，通常是因为网络故障，或者我们服务器故障引起的，我们的服务器状态可以查看 健康状态检查'
      break
    case 101:
      return '查询的 Class 不存在，或者要关联的 Pointer 对象不存在'
      break
    case 103:
      return '非法的 Class 名称，Class 名称大小写敏感，并且必须以英文字母开头，有效的字符仅限在英文字母、数字以及下划线'
      break
    case 104:
      return '缺少 objectId，通常是在查询的时候没有传入 objectId，或者 objectId 非法。objectId 只能为字母、数字组成的字符串'
      break
    case 105:
      return '无效的 key 名称，也就是 Class 的列名无效，列名必须以英文字母开头，有效的字符仅限在英文字母、数字以及下划线'
      break
    case 106:
      return '无效的 Pointer 格式，Pointer必须为形如 {className: \'Post\', objectId: \'xxxxxx\'} 的 JSON 对象'
      break
    case 107:
      return '无效的 JSON 对象，解析 JSON 数据失败'
      break
    case 108:
      return '此 API 仅供内部使用'
      break
    case 109:
      return ' 共享的 Class 无权限执行此操作，请检查 Class 共享的权限设置'
      break
    case 111:
      return ' 推送订阅的频道无效，频道名称必须不是空字符串，只能包含英文字母、数字以及下划线，并且只能以英文字母开头'
      break
    case 113:
      return 'Class 中的某个字段设定成必须，保存的对象缺少该字段'
      break
    case 114:
      return ' iOS 推送存储的 deviceToken 无效，如何存储 installation 请阅读 消息推送开发指南'
      break
    case 116:
      return '要存储的对象超过了大小限制，我们限制单个对象的最大大小在 16 M'
      break
    case 117:
      return '更新的 Key 是只读属性，无法更新'
      break
    case 119:
      return '该操作无法从客户端发起。请检查该错误由哪个操作引起，然后进入 应用控制台 > 设置 > 应用选项 启用相应的选项'
      break
    case 120:
      return '查询结果无法从缓存中找到，SDK 在使用从查询缓存的时候，如果发生缓存没有命中，返回此错误'
      break
    case 121:
      return 'JSON 对象中 key 的名称不能包含 $ 和 . 符号'
      break
    case 122:
      return '无效的文件名称，文件名称只能是英文字母、数字和下划线组成，并且名字长度限制在 1 到 36 之间'
      break
    case 123:
      return 'ACL 格式错误，如果您是使用 SDK 提供的 AVACL 类，理论上这不应该发生，正确的 ACL 格式请参考 REST API'
      break
    case 124:
      return '请求超时，超过一定时间（默认 10 秒）没有返回，通常是因为网络故障或者该操作太耗时引起的'
      break
    case 125:
      return '电子邮箱地址无效'
      break
    case 126:
      return '无效的用户 Id，可能用户不存在'
      break
    case 127:
      return '手机号码无效'
      break
    case 128:
      return ' 无效的 Relation 数据，通常是因为添加或者删除的 Relation 数据为空或者过多（单次超过 1000 个）'
      break
    case 137:
      return '违反 class 中的唯一性索引约束（unique），尝试存储重复的值'
      break
    case 139:
      return '角色名称非法，角色名称只能以英文字母、数字或下划线组成'
      break
    case 140:
      return '超过应用额度，请升级到商用版或联系我们处理'
      break
    case 141:
      return '云引擎调用超时'
      break
    case 142:
      let index1 = error.message.indexOf(': ')
      let index2 = error.message.lastIndexOf(' [')
      let processErrorText = error.message.substring(index1 + 1, index2)
      return processErrorText
      break
    case 145:
      return '本设备没有启用支付功能'
      break
    case 150:
      return '转换数据到图片失败'
      break
    case 154:
      return '超过应用阈值限制'
      break
    case 160:
      return '账户余额不足'
      break
    case 200:
      return '没有提供用户名，或者用户名为空'
      break
    case 201:
      return '没有提供密码，或者密码为空'
      break
    case 202:
      return '用户名已经被占用'
      break
    case 203:
      return '电子邮箱地址已经被占用'
      break
    case 204:
      return '没有提供电子邮箱地址'
      break
    case 205:
      return '找不到电子邮箱地址对应的用户'
      break
    case 206:
      return '没有提供 session，无法修改用户信息，这通常是因为没有登录的用户想修改信息。修改用户信息必须登录，除非在云引擎里使用 master key 来更改'
      break
    case 207:
      return '只能通过注册创建用户，不允许第三方登录'
      break
    case 208:
      return '第三方帐号已经绑定到一个用户，不可绑定到其他用户'
      break
    case 209:
      return '您已被禁止登录'
      break
    case 210:
      return '用户名和密码不匹配'
      break
    case 211:
      return '找不到用户'
      break
    case 212:
      return '请提供手机号码'
      break
    case 213:
      return '手机号码对应的用户不存在'
      break
    case 214:
      return '手机号码已经被注册'
      break
    case 215:
      return '未验证的手机号码'
      break
    case 216:
      return '未验证的邮箱地址'
      break
    case 217:
      return '无效的用户名，不允许空白用户名'
      break
    case 218:
      return '无效的密码，不允许空白密码'
      break
    case 219:
      return '登录失败次数超过限制，请稍候再试，或者通过忘记密码重设密码'
      break
    case 250:
      return '连接的第三方账户没有返回用户唯一标示 id'
      break
    case 251:
      return '无效的账户连接，一般是因为 access token 非法引起的'
      break
    case 252:
      return '无效的微信授权信息'
      break
    case 300:
      return 'CQL 语法错误'
      break
    case 301:
      return '新增对象失败，通常是数据格式问题'
      break
    case 302:
      return '无效的 GeoPoint 类型，请确保经度在 -180 到 180 之间，纬度在 -90 到 90 之间'
      break
    case 303:
      return '插入数据库失败，一般是数据格式或者内部错误'
      break
    case 304:
      return ' 数据操作错误，一般是语法错误或者内部异常'
      break
    case 305:
      return '根据 where 条件更新或者删除对象不起作用，通常是因为条件不满足'
      break
    case 401:
      return '未经授权的访问，没有提供 App id，或者 App id 和 App key 校验失败，请检查配置'
      break
    case 403:
      return '操作被禁止'
      break
    case 429:
      return '超过应用的流控限制'
      break
    case 430:
      return '超过 REST API 上传文件流控限制'
      break
    case 431:
      return '超过云引擎 hook 调用流控限制'
      break
    case 502:
      return '服务器维护中'
      break
    case 503:
      return '应用被临时禁用或者进入只读状态，通常是进行运维或者故障处理操作'
      break
    case 511:
      return '该请求 API 暂时不可用，请稍后重试'
      break
    case 524:
      return 'Web 服务器与后端应用服务器通讯失败'
      break
    case 529:
      return '当前 IP 超过并发限制'
      break
    case 600:
      return '无效的短信签名'
      break
    case 601:
      return '发送短信过于频繁'
      break
    case 602:
      return '发送短信或者语音验证码失败'
      break
    case 603:
      return '无效的短信验证码'
      break
    case 604:
      return '找不到自定义的短信模板'
      break
    case 605:
      return '短信模板未审核'
      break
    case 606:
      return '渲染短信模板失败'
      break
    case 700:
      return '无效的查询或者排序字段，请确认查询或者排序的字段在表中存在'
      break
    case 800:
      return '验证码不正确'
      break
    case 1006:
      return 'WebSocket 连接非正常关闭，通常见于路由器配置对长连接限制的情况'
      break
    case 4100:
      return '应用不存在或应用禁用了实时通信服务'
      break
    case 4101:
      return ' 同一个设备重复登录推送服务'
      break
    case 4102:
      return ' 登录签名验证失败'
      break
    case 4103:
      return 'Client Id 格式错误，超过 64 个字符'
      break
    case 4105:
      return 'Session 没有打开就发送消息，或执行其他操作'
      break
    case 4107:
      return '读超时，云端长时间没有收到客户端的数据，切断连接'
      break
    case 4108:
      return '登录超时，连接后长时间没有完成 session open'
      break
    case 4109:
      return '包过长。消息大小超过 5 KB，请缩短消息或者拆分消息'
      break
    case 4110:
      return ' 设置安全域名后，当前登录的域名与安全域名不符合'
      break
    case 4111:
      return '您的账号已在其它设备登录'
      break
    case 4112:
      return ' Session 过期，请重新登录'
      break
    case 4113:
      return '应用容量超限，当天登录用户数已经超过应用设定的最大值'
      break
    case 4114:
      return '客户端发送的序列化数据服务器端无法解析'
      break
    case 4115:
      return '客户端被 REST API 管理接口强制下线'
      break
    case 4116:
      return '应用单位时间内发送消息量超过限制，消息被拒绝'
      break
    case 4200:
      return '服务器内部错误'
      break
    case 4201:
      return '通过 API 发送消息超时'
      break
    case 4301:
      return '上游 API 调用异常'
      break
    case 4302:
      return '对话相关操作签名错误'
      break
    case 4303:
      return '发送消息，或邀请等操作对应的对话不存在'
      break
    case 4304:
      return '对话成员已满，不能再添加'
      break
    case 4305:
      return '对话操作被应用的云引擎 Hook 拒绝'
      break
    case 4306:
      return '更新对话操作失败'
      break
    case 4307:
      return '该对话为只读，不能更新或增删成员'
      break
    case 4308:
      return '该对话禁止当前用户发送消息'
      break
    case 4309:
      return '更新对话的请求被拒绝，当前用户不在对话中'
      break
    case 4310:
      return '查询对话失败'
      break
    case 4311:
      return '拉取对话消息记录失败'
      break
    case 4312:
      return '拉取对话消息记录被拒绝，当前用户不在对话中'
      break
    case 4313:
      return '该功能仅对系统对话有效'
      break
    case 4314:
      return '该功能仅对普通对话有效'
      break
    case 4315:
      return '当前用户被加入此对话的黑名单，无法发送消息'
      break
    case 4316:
      return '该功能仅对暂态对话有效'
      break
    case 4317:
      return '该操作要求用户是对话成员'
      break
    case 4318:
      return '对话操作超出了 API 请求限制'
      break
    case 4401:
      return '发送消息的对话不存在，或当前用户不在对话中'
      break
    case 4402:
      return '发送的消息被应用的云引擎 Hook 拒绝'
      break
    case 4403:
      return '没有操作目标消息的权限'
      break
    case 4404:
      return '找不到被操作的消息'
      break
  }
}
