import {ToastAndroid} from 'react-native';

export default class notifyUtil {
  static toastDuration = [ToastAndroid.SHORT, ToastAndroid.LONG]
  static toastPosition = [ToastAndroid.TOP, ToastAndroid.CENTER, ToastAndroid.BOTTOM]

  static toast(text, duration = 'short', position = 'center') {
    if(duration !== 'short' && duration !== 'long') {
      throw new Error('duration参数只能为\'short\'或\'long\'')
    }

    if(!['top', 'center', 'bottom'].includes(position)) {
      throw new Error('postion参数只能为[\'top\', \'center\', \'bottom\']')
    }

    let toastDurationIndex = ['short', 'long'].findIndex(item => item === duration)
    let toastPositionIndex = ['top', 'center', 'bottom'].findIndex(item => item === position)
    ToastAndroid.showWithGravity(text, notifyUtil.toastDuration[toastDurationIndex], notifyUtil.toastPosition[toastPositionIndex])
  }
}
