import moment from 'moment';

export function formatDateFromObj(dateObj) {
  return moment(dateObj).format('YYYY/MM/DD HH:mm:ss')
}

export function getChatAndConversationTime(timestamp){
	const nowDay=(new Date()).getDate()
	const nowMonth=((new Date()).getMonth())+1
	const nowYear=(new Date()).getFullYear()
	const receiveDay=(new Date(timestamp)).getDate()
	const receiveMonth=(new Date(timestamp)).getMonth()+1
	const receiveYear=(new Date(timestamp)).getFullYear()
	const receiveDateObj=new Date(timestamp);
	const hour=receiveDateObj.getHours()<10?('0'+receiveDateObj.getHours()):receiveDateObj.getHours()
	const minute=receiveDateObj.getMinutes()<10?('0'+receiveDateObj.getMinutes()):receiveDateObj.getMinutes()
	let timeText=''
  const diffInDay = (arr,day) => {
  	return arr.find(function(item,index,arr) {
  		return item === day
  	}) === undefined ? false : true
  }
  
	if(nowDay-receiveDay===0&&nowMonth===receiveMonth){
		timeText=((((new Date(timestamp)).getHours())<10?'0'+((new Date(timestamp)).getHours()):((new Date(timestamp)).getHours())))+':'+((((new Date(timestamp)).getMinutes())<10?'0'+((new Date(timestamp)).getMinutes()):((new Date(timestamp)).getMinutes())));
	}else if(nowDay-receiveDay===1&&nowMonth===receiveMonth||
			nowDay===1&&diffInDay([-30,-29,-28,-27],nowDay-receiveDay)&&nowMonth>receiveMonth&&nowYear===receiveYear||
			nowDay===1&&diffInDay([-30,-29,-28,-27],nowDay-receiveDay)&&nowMonth<receiveMonth&&nowYear>receiveYear){
		timeText=`昨天 ${hour}:${minute}`
	}else if(nowDay-receiveDay===2&&nowMonth===receiveMonth||
			nowDay===1&&diffInDay([-29,-28,-27,-26],nowDay-receiveYear)&&nowMonth>receiveMonth&&nowYear===receiveYear||
			nowDay===2&&diffInDay([-28,-27,-26,-25],nowDay-receiveYear)&&nowMonth>receiveMonth&&nowYear===receiveYear||
			nowDay===1&&diffInDay([-29,-28,-27,-26],nowDay-receiveYear)&&nowMonth<receiveMonth&&nowYear>receiveYear||
			nowDay===2&&diffInDay([-28,-27,-26,-25],nowDay-receiveYear)&&nowMonth<receiveMonth&&nowYear>receiveYear
	){
		timeText=`前天 ${hour}:${minute}`
	}else if(((new Date()).getTime()-timestamp)/1000>=259200&&((new Date()).getTime()-timestamp)/1000<=604800){
		var weeks=['星期日','星期一','星期二','星期三','星期四','星期五','星期六']
		timeText=`${weeks[(new Date(timestamp)).getDay()]} ${hour}:${minute}`
	}else if(nowYear===receiveYear){
		return receiveMonth+'-'+receiveDay+' '+hour+':'+minute
	}else{
		return receiveYear+'-'+receiveMonth+'-'+receiveDay+' '+hour+':'+minute
	}
	return timeText
}

export function formatUserFromServer(user) {
  return {
    id: user.objectId,
    username: user.username,
    nick: user.nick,
    birth: user.birth,
    canBeSearched: user.canBeSearched,
    deviceId: user.deviceId,
    email: user.email,
    emailVerified: user.emailVerified,
    gender: user.gender,
    mobilePhoneNumber: user.mobilePhoneNumber,
    mobilePhoneVerified: user.mobilePhoneVerified,
    onlyCurrentDeviceLogin: user.onlyCurrentDeviceLogin,
    updatedAt: formatDateFromObj(new Date(user.updatedAt)),
    avatar: {
      url: user.avatar.url,
      updatedAt: formatDateFromObj(new Date(user.avatar.updatedAt)),
      name: user.avatar.name
    }
  }
}
