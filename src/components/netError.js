import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import {ViewPropTypes} from 'react-native';
import * as connectStatus from '../config/connectStatus';
import netStatusText from '../config/netStatusText';
import propTypes from 'prop-types';

export default class NetError extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    netStatus: propTypes.object.isRequired,
    containerStyle: ViewPropTypes.style,
    textStyle: Text.propTypes.style,
    rightIcon: propTypes.element
  }

  static defaultProps = {
    containerStyle: StyleSheet.create({}),
    textStyle: StyleSheet.create({}),
    rightIcon: null
  }

  render() {
    let errorText
    const netStatus = this.props.netStatus.status
    if(netStatus !== connectStatus.SCHEDULE && netStatus !== connectStatus.RETRY) {
      errorText = netStatusText[netStatus]
    }else {
      errorText = this.props.netStatus.desc
    }

    let isVisible
    if(netStatus && (netStatus !== connectStatus.ONLINE && netStatus !== connectStatus.RECONNECT)) {
      isVisible = true
    }else {
      isVisible = false
    }

    return (
      isVisible ?
        <View style={[styles.container, this.props.containerStyle]}>
          <Text style={[styles.errorText, this.props.textStyle]}>{errorText}</Text>
          {
            this.props.rightIcon ?
              <View style={styles.rightIconContainer}>
                {
                  this.props.rightIcon
                }
              </View>
            :
              null
          }
        </View>
      :
        null
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 50,
    backgroundColor: '#FF8247',
    paddingLeft: 10,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  errorText: {
    color: '#fff',
    flex: 1
  },
  rightIconContainer: {
    width: 15,
    height: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10
  }
})
