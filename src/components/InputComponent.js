import React, {Component} from 'react';
import propTypes from 'prop-types';
import {ViewPropTypes} from 'react-native';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet
} from 'react-native';
import {getScreenSize} from '../utils/common';

export default class InputComponent extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    style: ViewPropTypes.style,
    inputBarStyle: Text.propTypes.style,
    placeholder: propTypes.string.isRequired,
    underlineColorAndroid: propTypes.string,
    secureTextEntry: propTypes.bool,
    returnKeyLabe: propTypes.string,
    keyboardType: propTypes.string,
    defaultValue: propTypes.string,
    maxLength: propTypes.number,
    autoFocus: propTypes.bool,
    onChangeText: propTypes.func.isRequired,
    rightElement: propTypes.func,
    editable: propTypes.bool,
    multiline: propTypes.bool,
    numberOfLines: propTypes.number,
    onLayout: propTypes.func,
    placeholderTextColor: propTypes.string
  }

  static defaultProps = {
    underlineColorAndroid: 'transparent',
    style: StyleSheet.create({}),
    inputBarStyle: StyleSheet.create({}),
    returnKeyLabel: '确定',
    keyboardType: 'default',
    defaultValue: '',
    maxLength: null,
    autoFocus: false,
    secureTextEntry: false,
    editable: true,
    multiline: false,
    numberOfLines: 1,
    placeholderTextColor: '#999'
  }

  render() {
    return(
      <View style={[styles.inputItem, this.props.style]}>
        <TextInput
          style={[styles.inputBar, this.props.inputBarStyle]}
          secureTextEntry={this.props.secureTextEntry}
          placeholder={this.props.placeholder}
          underlineColorAndroid={this.props.underlineColorAndroid}
          returnKeyLabel={this.props.returnKeyLabel}
          keyboardType={this.props.keyboardType}
          defaultValue={this.props.defaultValue}
          maxLength={this.props.maxLength}
          autoFocus={this.props.autoFocus}
          editable={this.props.editable}
          multiline={this.props.multiline}
          numberOfLines={this.props.numberOfLines}
          placeholderTextColor={this.props.placeholderTextColor}
          onChangeText={value => this.props.onChangeText(value)}/>
          {
            this.props.rightElement ? this.props.rightElement() : null
          }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  inputItem: {
    flexDirection: 'row',
    width: getScreenSize().width * 0.8,
    height: 50,
    alignItems: 'center',
    borderColor: '#999',
    borderBottomWidth: 2,
    paddingTop: 5
  },
  inputBar: {
    flex: 1,
    height: '100%',
    fontSize: 16
  }
})
