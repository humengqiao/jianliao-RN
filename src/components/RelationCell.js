import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import {getPixel} from '../utils/common';
import {CustomCachedImage} from "react-native-img-cache";
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
import Empty from '../components/Empty';
import commonStyle from '../styles/common';

export default class RelationCell extends Component {
  constructor(props) {
    super(props)
  }

  onLongPressTitle() {
    this.cellTitle.measureInWindow((x, y) => {
      this.props.onLongPress(y)
    })
  }

  jumpToInfoDetailPage(cell) {
    const themeColor = this.props.themeColor
    const type = this.props.type
    const divideName = this.props.divide.name
    let jumpTarget = '', title = ''
    if(type === 'contact') {
      jumpTarget = 'jianliao.ContactInfoDetailScreen'
      title = '联系人资料详情'
    }else if(type === 'group') {
      jumpTarget = 'jianliao.GroupInfoDetailScreen'
      title = '群组资料详情'
    }else if(type === 'blacklist') {
      jumpTarget = 'jianliao.BlacklistInfoDetailScreen'
      title = '黑名单资料详情'
    }

    if(jumpTarget) {
      this.props.navigator.showModal({
        screen: jumpTarget,
        title,
        animationType: 'slide-up',
        navigatorStyle: {
          navBarTitleTextCentered: true,
          navBarBackgroundColor: themeColor,
          navBarTextColor: '#fff',
          screenBackgroundColor: '#fff'
        },
        navigatorButtons: {
          leftButtons: [
            {
              id: 'back',
              buttonColor: '#fff'
            }
          ],
          rightButtons: [
            {
              id: 'more',
              title: '更多',
              buttonColor: '#fff'
            }
          ]
        },
        passProps: {
          themeColor,
          type,
          canSendMessage: true,
          [type]: cell,
          divideName
        }
      })
    }
  }

  renderRelationCell(cell) {
    const themeColor = this.props.themeColor
    return (
      <TouchableHighlight
        onPress={() => this.jumpToInfoDetailPage(cell)}
        underlayColor='#ccc'
        activeOpacity={0.5}>
        <View style={[styles.relationCell, {borderBottomColor: themeColor}]}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <CustomCachedImage
              component={CustomImage}
              source={{uri: cell.avatar.url}}
              indicator={Pie}
              indicatorProps={{
                size: 80,
                borderWidth: 0,
                color: themeColor,
                unfilledColor: 'rgba(200, 200, 200, 0.2)'
              }}
              style={styles.avatar}/>
            <Text style={[styles.nick, {color: themeColor}]}>{cell.nick}</Text>
          </View>
          <Image
            source={require('../images/right_arrow.png')}
            style={[styles.cellArrowRight, {tintColor: themeColor}]}/>
        </View>
      </TouchableHighlight>
    )
  }

  render() {
    const {divide, themeColor} = this.props
    const icon = divide.expand ? require('../images/arrow_up.png') : require('../images/arrow_down.png')
    return (
      <View>
        <TouchableOpacity
          style={styles.divideTitleWrapper}
          ref={el => this.cellTitle = el}
          onPress={() => this.props.onPressTitle()}
          onLongPress={() => this.onLongPressTitle()}>
          <Text style={{color: themeColor}}>{divide.label}</Text>
          <View style={styles.cellRight}>
            <Text style={{color: themeColor}}>{divide.list.length}</Text>
            <Image
              source={icon}
              style={[styles.cellArrow, {tintColor: themeColor}]}/>
          </View>
        </TouchableOpacity>
        {
          divide.expand ?
            <FlatList
              data={divide.list}
              keyExtractor={(item, index) => index}
              ListEmptyComponent={
                <Empty
                  title='该分组暂无成员'
                  titleStyle={commonStyle.empty}
                  themeColor={themeColor}/>
              }
              renderItem={({item}) => this.renderRelationCell(item)}/>
          :
            null
        }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  divideTitleWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 40
  },
  cellArrow: {
    width: 15,
    height: 15,
    marginLeft: 10
  },
  cellRight: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  relationCell: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomWidth: getPixel() * 3
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25
  },
  nick: {
    fontSize: 16,
    marginLeft: 10
  },
  cellArrowRight: {
    width: 20,
    height: 20
  },
  empty: {
    fontSize: 14,
    marginLeft: 30
  }
})
