import React, {Component} from 'react';
import propTypes from 'prop-types';
import {
  View,
  Text,
  TouchableHighlight,
  Image,
  StyleSheet
} from 'react-native';
import {ViewPropTypes} from 'react-native';
import {getScreenSize} from '../utils/common';

export default class ButtonComponent extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    buttonStyle: ViewPropTypes.style,
    onPressButton: propTypes.func.isRequired,
    underlayColor: propTypes.string,
    activeOpacity: propTypes.number,
    delayLongPress: propTypes.number,
    disabled: propTypes.bool,
    title: propTypes.string.isRequired
  }

  static defaultProps = {
    buttonStyle: StyleSheet.create({}),
    underlayColor: '#fff',
    activeOpacity: 0.7,
    delayLongPress: 300,
    disabled: false
  }

  render() {
    return(
      <TouchableHighlight
        style={[styles.container, this.props.buttonStyle]}
        onPress={this.props.onPressButton}
        underlayColor={this.props.underlayColor}
        activeOpacity={this.props.activeOpacity}
        delayLongPress={this.props.delayLongPress}
        disabled={this.props.disabled}>
        <Text style={[styles.title, this.props.titleStyle]}>{this.props.title}</Text>
      </TouchableHighlight>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: getScreenSize().width * 0.8,
    marginLeft: 'auto',
    marginRight: 'auto',
    flexDirection: 'row',
    marginTop: 15,
    height: 40,
    backgroundColor: '#999',
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 18,
    color: '#fff',
    fontWeight: '400'
  }
})
