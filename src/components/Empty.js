import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

export default class Empty extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {themeColor, title} = this.props
    return (
      <View style={[styles.container, this.props.emptyContainerStyle]}>
        <Text style={[styles.content, this.props.titleStyle, {color: themeColor}]}>{title}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 5
  },
  content: {
    fontSize: 18,
    textAlign: 'center'
  }
})
