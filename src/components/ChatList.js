import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableHighlight
} from 'react-native';
import {getScreenSize} from '../utils/common';
import {CustomCachedImage} from "react-native-img-cache";
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
import commonStyle from '../styles/common';
import * as invariantMessageTypes from '../config/invariantMessageType';
import {getChatAndConversationTime} from '../utils/format';
const Sound = require('react-native-sound');
const AVATAR_WIDTH = 60
const IMAGE_HEIGHT = 150
let sound
Sound.setCategory('Playback');

export default class ChatList extends Component {
  constructor(props) {
    super(props)
  }

  componentWillUnmount() {
    sound && sound.stop()
    sound = null
  }

  parseMessageContent(messageContent) {
    switch(messageContent._lctype) {
      case invariantMessageTypes.TEXT:
        return this.renderTextMessage(messageContent._lctext)
        break
      case invariantMessageTypes.IMAGE:
        return this.renderImageMessage(messageContent)
        break
      case invariantMessageTypes.AUDIO:
        return this.renderAudioMessage(messageContent)
        break
    }
  }

  renderTextMessage(text) {
    return (
      <TouchableHighlight>
        <Text style={styles.contentText}>{text}</Text>
      </TouchableHighlight>
    )
  }

  onPlayAudio(audioUrl, duration) {
    sound && sound.stop()
    this.props.onAudioPlayChange(false)
    sound = new Sound(audioUrl, undefined, e => {
        if(e) {
            console.log('error', e)
            return
        }else {
            this.props.onAudioPlayChange(true)
            sound.play(() => {
                this.props.onAudioPlayChange(false)
            })
        }
    }, {loadSync:true})
  }

  renderImageMessage(message) {
    const imageUrl = message._lcfile.url
    const location = message._lcattrs.location
    const themeColor = this.props.themeColor
    const originalWidth = message._lcfile.metaData.width
    const originalHeight = message._lcfile.metaData.height
    const width = originalWidth < commonStyle.commonWidth - AVATAR_WIDTH - 20 ? originalWidth : commonStyle.commonWidth - AVATAR_WIDTH - 20
    const height = originalWidth < commonStyle.commonWidth - AVATAR_WIDTH - 20 ? originalHeight : IMAGE_HEIGHT
    return (
      <TouchableHighlight>
        <View style={{alignItems: 'flex-end'}}>
          <CustomCachedImage
            component={CustomImage}
            source={{uri: imageUrl}}
            indicator={Pie}
            indicatorProps={{
              size: 80,
              borderWidth: 0,
              color: themeColor,
              unfilledColor: '#999'
            }}
            style={{width, height, marginRight: 10, borderRadius: 3}}/>
          <Text style={styles.location}>
            {
              location ?
                `地点：${location}`
              :
                null
            }
          </Text>
        </View>
      </TouchableHighlight>
    )
  }

  renderAudioMessage(message) {
    const duration = parseInt(message._lcattrs.duration)
    const audioUrl = message._lcfile.url
    const width = duration * 10 < 300 ? duration * 10 : 300
    let durationText = ''
    if(parseInt(duration / 60) === 0) {
      durationText = `${duration % 60} "`
    }else {
      durationText = `${duration / 60} ' ${duration % 60} "`
    }
    return (
        <TouchableOpacity onPress={() => this.onPlayAudio(audioUrl, duration)}>
            <View style={{flexDirection: 'row', height: 30, minWidth: 20, alignItems: 'center'}}>
                <View style={{width: width}}>
                    <Image
                       style={styles.audio}
                       source={require('../images/voice.png')}
                      />
                </View>
                <Text>{durationText}</Text>
            </View>
        </TouchableOpacity>
    )
  }

  renderChatOnLeft() {
    const themeColor = this.props.themeColor
    const {
      user: {
        avatar: {url: avatarUrl},
        nick
      }
    } = this.props.message
    const content = this.parseMessageContent(this.props.message.data)

    return (
      <View style={styles.inner}>
        <TouchableOpacity>
          <CustomCachedImage
            component={CustomImage}
            source={{uri: avatarUrl}}
            indicator={Pie}
            indicatorProps={{
              size: 80,
              borderWidth: 0,
              color: themeColor,
              unfilledColor: '#999'
            }}
            style={[styles.avatar, this.props.avatarStyle, {marginRight: 10}]}/>
        </TouchableOpacity>
        <View>
          <View style={styles.nickWrapper}>
            <Text style={{fontSize: 14, color: themeColor}}>{nick}</Text>
          </View>
          <View style={[styles.content, {backgroundColor: themeColor, marginLeft: 10}]}>
            {
              content
            }
          </View>
          <View style={[styles.leftAngle, {borderRightColor: themeColor}]}/>
        </View>
      </View>
    )
  }

  renderChatOnRight() {
    const themeColor = this.props.themeColor
    const {
      user: {
        avatar: {url: avatarUrl},
        nick
      }
    } = this.props.message
    const content = this.parseMessageContent(this.props.message.data)

    return (
      <View style={[styles.inner, {justifyContent: 'flex-end'}]}>
        <View style={{alignItems: 'flex-end'}}>
          <View style={styles.nickWrapper}>
            <Text style={{fontSize: 14, color: themeColor}}>{nick}</Text>
          </View>
          <View style={[styles.content, {backgroundColor: themeColor, marginRight: 10}]}>
            {
              content
            }
          </View>
          <View style={[styles.rightAngle, {borderLeftColor: themeColor}]}/>
        </View>
        <TouchableOpacity>
          <CustomCachedImage
            component={CustomImage}
            source={{uri: avatarUrl}}
            indicator={Pie}
            indicatorProps={{
              size: 80,
              borderWidth: 0,
              color: themeColor,
              unfilledColor: '#999'
            }}
            style={[styles.avatar, this.props.avatarStyle, {marginLeft: 10}]}/>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    const direction = this.props.direction === 'left' ? 'flex-start' : 'flex-end'
    const {showTime, timestamp} = this.props.message
    const themeColor = this.props.themeColor
    return (
      <View style={{alignItems: 'center'}}>
        <Text style={{color: themeColor}}>
          {
            showTime ?
              getChatAndConversationTime(timestamp)
            :
              null
          }
        </Text>
        <View style={[styles.container, {justifyContent: direction}]}>
          {
            this.props.direction === 'left' ?
              this.renderChatOnLeft()
            :
              this.renderChatOnRight()
          }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: getScreenSize().width,
    flexDirection: 'row',
    paddingHorizontal: 5
  },
  inner: {
    width: commonStyle.commonWidth,
    flexDirection: 'row'
  },
  nickWrapper: {
    height: 20,
    marginBottom: 5
  },
  content: {
    maxWidth: commonStyle.commonWidth - AVATAR_WIDTH - 20,
    padding: 10,
    borderRadius: 5
  },
  contentText: {
    flexBasis: 100,
    color: '#fff',
    fontSize: 16
  },
  avatar: {
    width: AVATAR_WIDTH,
    height: 60,
    borderRadius: AVATAR_WIDTH / 2
  },
  leftAngle: {
    position: 'absolute',
    left: -8,
    top: 30,
    borderWidth: 10,
    borderColor: 'transparent'
  },
  rightAngle: {
    position: 'absolute',
    right: -8,
    top: 30,
    borderWidth: 10,
    borderColor: 'transparent'
  },
  location: {
    color:'#fff',
    fontSize: 12,
    marginTop: 5
  },
  audio: {
    width: 20,
    height: 20
  }
})
