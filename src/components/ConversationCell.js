import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet
} from 'react-native';
import propTypes from 'prop-types';
import {getScreenSize, getPixel} from '../utils/common';
import {CustomCachedImage} from "react-native-img-cache";
import CustomImage from 'react-native-image-progress';
import {Pie} from 'react-native-progress';
const CEll_HEIGHT = 80

export default class ConversationCell extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    title: propTypes.string.isRequired,
    avatar: propTypes.any.isRequired,
    content: propTypes.element,
    time: propTypes.string,
    notifyContent: propTypes.oneOfType([propTypes.string, propTypes.number]),
    themeColor: propTypes.string.isRequired,
    cacheImage: propTypes.bool,
    onPressCell: propTypes.func.isRequired
  }

  static defaultProps = {
    content: '',
    time: '',
    unreadMessageCount: 0,
    cacheImage: true
  }

  render() {
    const themeColor = this.props.themeColor
    const {
      avatar,
      cacheImage,
      title,
      content,
      time,
      notifyContent
    } = this.props

    return (
      <TouchableOpacity
        style={[styles.cell, {borderBottomColor: themeColor}]}
        onPress={() => this.props.onPressCell()}>
        <View style={styles.main}>
          {
            cacheImage ?
              <CustomCachedImage
                component={CustomImage}
                source={avatar}
                indicator={Pie}
                indicatorProps={{
                  size: 80,
                  borderWidth: 0,
                  color: themeColor,
                  unfilledColor: '#999'
                }}
                style={[styles.avatar, this.props.avatarStyle]}/>
            :
              <Image
                source={avatar}
                style={[styles.avatar, this.props.avatarStyle]}/>
          }
          <View style={styles.contentWrapper}>
            <Text style={[styles.title, {color: themeColor}]}>{title}</Text>
            {content}
          </View>
        </View>
        <View style={styles.timeAndUnReadCount}>
          <Text style={{color: themeColor}}>{time}</Text>
          {
            notifyContent ?
              (
                typeof notifyContent === 'number' ?
                  <View style={[styles.notifyContentWrapper, {backgroundColor: notifyContent > 0 ? themeColor : 'transparent'}]}>
                    <Text style={styles.notifyContent}>{notifyContent}</Text>
                  </View>
                :
                  <View style={styles.notifyContentWrapper}>
                    <Text style={styles.notifyContent}>{notifyContent}</Text>
                  </View>
              )
            :
              null
          }

        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  cell: {
    width: getScreenSize().width,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    height: CEll_HEIGHT,
    borderBottomWidth: getPixel() * 5
  },
  main: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
    marginRight: 10
  },
  contentWrapper: {
    flex: 1
  },
  timeAndUnReadCount: {
    alignItems: 'center',
    marginLeft: 5
  },
  title: {
    fontSize: 18,
    color: '#000',
    marginBottom: 5
  },
  notifyContentWrapper: {
    borderRadius: 10,
    marginTop: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  notifyContent: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 12,
    paddingHorizontal: 5,
    paddingVertical: 3
  }
})
