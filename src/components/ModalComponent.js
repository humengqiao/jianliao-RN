import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback
} from 'react-native';
import commonStyle from '../styles/common';
import {getPixel} from '../utils/common';

export default class ModalComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      show: true
    }
  }

  onTapContainer() {
    this.props.onClose()
  }

  render() {
    return (
      <Modal
        onRequestClose={() => {}}
        transparent={true}
        visible={this.props.visible}>
        <TouchableWithoutFeedback
          onPress={() => this.onTapContainer()}>
          <View style={styles.container}>
            <View style={styles.boxWrapper}>
              <View style={[styles.titleWrapper, {borderBottomColor: this.props.themeColor}]}>
                <Text style={[styles.title, this.props.titltStyle]}>{this.props.title}</Text>
              </View>
              <View style={styles.contentWrapper}>
                {
                  this.props.content
                }
              </View>
              <View style={[styles.opWrapper, {backgroundColor: this.props.themeColor}]}>
                {
                  this.props.buttons
                }
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...commonStyle.container,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center'
  },
  boxWrapper: {
    width: commonStyle.commonWidth,
    minHeight: 100,
    backgroundColor: '#fff',
    borderRadius: 3,
    overflow: 'hidden'
  },
  titleWrapper: {
    height: 50,
    borderBottomWidth: getPixel() * 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 16
  },
  opWrapper: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3
  }
})
