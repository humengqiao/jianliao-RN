import React, {Component} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet
} from 'react-native';
import propTypes from 'prop-types';
import {ViewPropTypes} from 'react-native';

export default class LoadingIndicator extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    visible: propTypes.bool.isRequired,
    indicatorStyle: ViewPropTypes.style,
    size: propTypes.oneOf(['small', 'large']),
    color: propTypes.string
  }

  static defaultProps = {
    indicatorStyle: StyleSheet.create({}),
    size: 'large',
    color: '#999'
  }

  render() {
    return (
      this.props.visible ?
        <View style={[styles.container, this.props.indicatorStyle]}>
          <ActivityIndicator
           size={this.props.size}
           color={this.props.color}/>
        </View>
      :
        null
    )
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
