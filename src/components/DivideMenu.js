import React, {Component} from 'react';
import {ViewPropTypes} from 'react-native';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  StyleSheet
} from 'react-native';
import propTypes from 'prop-types';
import {getScreenSize} from '../utils/common';
const menuWidth = 80
const menuHeight = 40
const arrowBorderWidth = 10

export default class DivideMenu extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    y: propTypes.number,
    visible: propTypes.bool,
    menuBgStyle: ViewPropTypes.style,
    menuBoxStyle: ViewPropTypes.style,
    arrowStyle: ViewPropTypes.style,
    content: propTypes.element.isRequired
  }

  static defaultProps = {
    y: 0,
    visible: false,
    menuBgStyle: StyleSheet.create({}),
    menuBoxStyle: StyleSheet.create({}),
    arrowStyle: StyleSheet.create({})
  }

  render() {
    const y = this.props.y - (arrowBorderWidth + menuHeight)
    return (
      <Modal
        onRequestClose={() => {}}
        transparent={true}
        animationType='fade'
        visible={this.props.visible}>
        <TouchableWithoutFeedback
          onPress={() => this.props.onClose()}>
          <View style={[styles.container, this.props.menuBgStyle]}>
            <View
              style={[styles.menuWrapper, {top: y},  this.props.menuBoxStyle]}>
              {this.props.content}
            </View>
            <View style={[styles.arrow, {top: y + menuHeight}, this.props.arrowStyle]}/>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: getScreenSize().width,
    backgroundColor: 'transparent'
  },
  menuWrapper: {
    position: 'absolute',
    left: getScreenSize().width / 2 - menuWidth / 2,
    width: menuWidth,
    height: menuHeight,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    borderRadius: 5,
    alignItems: 'center'
  },
  arrow: {
    position: 'absolute',
    borderColor: 'transparent',
    borderTopColor: 'rgba(0, 0, 0, 0.5)',
    borderWidth: arrowBorderWidth,
    left: getScreenSize().width / 2 - arrowBorderWidth,
    zIndex: 10
  }
})
