import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import {ViewPropTypes} from 'react-native';
import propTypes from 'prop-types';
import {getScreenSize, getPixel} from '../utils/common';
import {themeColor} from '../styles/common';

export default class PersonalPageCell extends Component {
  constructor(props) {
    super(props)
  }

  static propTypes = {
    onPressCell: propTypes.func,
    style: ViewPropTypes.style,
    titleStyle: Text.propTypes.style,
    title: propTypes.oneOfType([propTypes.string, propTypes.element]),
    content: propTypes.string,
    rightIcon: propTypes.element,
    contentStyle: Text.propTypes.style
  }

  static defaultProps = {
    onPressCell: () => {},
    content: '',
    style: StyleSheet.create({}),
    titleStyle: StyleSheet.create({}),
    contentStyle: StyleSheet.create({}),
    rightIcon: null
  }

  render() {
    return (
      <TouchableOpacity onPress={() => this.props.onPressCell()}>
        <View style={[styles.common, this.props.style]}>
          <Text style={[styles.commonTitle, this.props.titleStyle]}>
            {this.props.title}
          </Text>
          <View style={[styles.rightContent, this.props.rightIconStyle]}>
            <Text style={this.props.contentStyle}>{this.props.content}</Text>
            {
              this.props.rightIcon
            }
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  common: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 5 * getPixel(),
    borderBottomColor: themeColor,
    backgroundColor: '#fff',
    height: 50,
    paddingLeft: 10
  },
  commonTitle: {
    fontSize: 17,
    fontWeight: '400'
  },
  rightContent: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
