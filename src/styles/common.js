import {getScreenSize} from '../utils/common';
export const themeColor = '#ff0000'

export default {
  container: {
    alignItems: 'center',
    flex: 1
  },
  rightIcon: {
    width: 30,
    height: 30,
    tintColor: themeColor
  },
  commonWidth: getScreenSize().width * 0.9,
  loading: {
    width: 20,
    height: 20
  },
  empty: {
    textAlign: 'center',
    fontSize: 18,
    marginTop: 5
  }
}
