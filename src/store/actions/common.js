import * as TYPES from '../actionTypes/common';

export function setLoading(flag) {
  return {
    type: TYPES.SETLOADING,
    loading: flag
  }
}

export function setThemeColor(themeColor) {
  return {
    type: TYPES.SETTHEMECOLOR,
    themeColor
  }
}
