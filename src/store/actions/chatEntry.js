import * as TYPES from '../actionTypes/chatEntry';

export function netStatusChange(status) {
  return {
    type: TYPES.CHANGE_NETSTATUS,
    netStatus: status
  }
}

export function selectedIndexChange(index) {
  return {
    type: TYPES.CHANGE_SELECTEDINDEX,
    selectedIndex: index
  }
}
