import * as TYPES from '../actionTypes/relationship';

export function startFetchContact(id) {
  return {
    type: TYPES.STARTFETCHCONTACT,
    id
  }
}

export function startFetchGroup(id) {
  return {
    type: TYPES.STARTFETCHGROUP,
    id
  }
}

export function startFetchBlackList(id) {
  return {
    type: TYPES.STARTFETCHBLACKLIST,
    id
  }
}

export function startModifyContact(id, cloudDivide, localDivide) {
  return {
    type: TYPES.STARTMODIFYCONTACT,
    id,
    cloudDivide,
    localDivide
  }
}

export function setContactList(contactList) {
  return {
    type: TYPES.SETCONTACTLIST,
    contactList
  }
}

export function setGroupList(groupList) {
  return {
    type: TYPES.SETGROUPLIST,
    groupList
  }
}

export function setBlackList(blackList) {
  return {
    type: TYPES.SETBLACKLIST,
    blackList
  }
}

export function clearRelationship() {
  return {
    type: TYPES.CLEARRELATIONSHIP
  }
}

export function toggleExpand(type, divideId) {
  return {
    type: TYPES.TOGGLEEXPAND,
    divideType: type,
    divideId
  }
}

export function patchModifyContact(contact) {
  return {
    type: TYPES.PATCHMODIFYCONTACT,
    contact
  }
}

export function removeUser(username, divideName) {
  return {
    type: TYPES.REMOVEUSER,
    username,
    divideName
  }
}

export function moveToBlacklist(user, divideName) {
  return {
    type: TYPES.MOVETOBLACKLIST,
    user,
    divideName,
  }
}

export function moveFromBlacklist(username) {
  return {
    type: TYPES.MOVEFROMBLACKLIST,
    username
  }
}

export function exitGroup(groupId) {
  return {
    type: TYPES.EXITGROUP,
    groupId
  }
}

export function startModifyGroup(id, nick, pic) {
  return {
    type: TYPES.STARTMODIFYGROUP,
    id,
    nick,
    pic
  }
}

export function patchModifyGroup(group) {
  return {
    type: TYPES.PATCHMODIFYGROUP,
    group
  }
}
