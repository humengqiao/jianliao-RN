import * as TYPES from '../actionTypes/conversation';

export function startFetchConversationList(conversationType) {
  return {
    type: TYPES.STARTFETCHCONVERSATION,
    conversationType
  }
}

export function patchConversation(conversation, lastUpdateAt, conversationType) {
  return {
    type: TYPES.SETCONVERSATION,
    conversation,
    lastUpdateAt,
    conversationType
  }
}

export function clearConversation() {
  return {
    type: TYPES.CLEARCONVERSATION
  }
}

export function modifyConversation(conversation) {
  return {
    type: TYPES.MODIFYCONVERSATION,
    conversation
  }
}

export function startClearUnreadStatus(conversation, conversationType) {
  return {
    type: TYPES.STARTCLEARUNREADSTATUS,
    conversation,
    conversationType
  }
}

export function setConversationUnreadCount(conversationId, count, conversationType) {
  return {
    type: TYPES.SETCONVERSATIONUNREADCOUNT,
    conversationId,
    count,
    conversationType
  }
}

export function startRemoveConversation(conversation, username, flag) {
  return {
    type: TYPES.STARTREMOVECONVERSATION,
    conversation,
    username,
    flag
  }
}

export function removeConversation(conversationId, flag) {
  return {
    type: TYPES.REMOVECONVERSATION,
    conversationId,
    flag
  }
}

export function startFetchHideConversationList(username) {
  return {
    type: TYPES.STARTFETCHHIDECONVERSATIONLIST,
    username
  }
}

export function setHideConversationList(hideConversationList) {
  return {
    type: TYPES.SETHIDECONVERSATIONLIST,
    hideConversationList
  }
}
