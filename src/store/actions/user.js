import * as TYPES from '../actionTypes/user';

export function login(user) {
  return {
    type: TYPES.USER_LOGIN,
    user
  }
}

export function startLogin(username, password) {
  return {
    type: TYPES.STARTLOGIN,
    username,
    password
  }
}

export function addToUsers(user) {
  return {
    type: TYPES.USERS_ADD,
    user
  }
}

export function removeFromUsers(username) {
  return {
    type: TYPES.USERS_REMOVE,
    username
  }
}

export function startLogout() {
  return {
    type: TYPES.STARTLOGOUT
  }
}

export function logout() {
  return {
    type: TYPES.USER_LOGOUT
  }
}

export function removeAll() {
  return {
    type: TYPES.REMOVE_ALL
  }
}

export function startUpdateAvatar(data) {
  return {
    type: TYPES.STARTUPDATEAVATAR,
    data
  }
}

export function patchAvatarUpdate(data) {
  return {
    type: TYPES.UPDATE_AVATAR,
    data
  }
}

export function startUpdateInfo(data) {
  return {
    type: TYPES.STARTUPDATEINFO,
    data
  }
}

export function patchInfoUpdate(data) {
  return {
    type: TYPES.UPDATEINFO,
    data
  }
}

export function refreshUserIndicator(refreshing) {  //下拉刷新状态控制
  return {
    type: TYPES.REFRESHUSERINDICATOR,
    refreshing
  }
}

export function startRefreshUser(key, value) { //对应云数据库属性名和属性值
  return {
    type: TYPES.STARTREFRESHUSER,
    key,
    value
  }
}

export function refreshUser(user) {
  return {
    type: TYPES.REFRESHUSER,
    user
  }
}
