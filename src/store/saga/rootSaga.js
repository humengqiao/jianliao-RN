import {fork, all} from 'redux-saga/effects';
import watchUserAsync from './user';
import watchRelationshipAsync from './relationship';
import watchConversationAsync from './conversation';

export default function* rootSaga() {
  yield all([
    fork(watchUserAsync),
    fork(watchRelationshipAsync),
    fork(watchConversationAsync)
  ])
}
