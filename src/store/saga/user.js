import {put, call, takeLatest, takeEvery} from 'redux-saga/effects';
import * as TYPES from '../actionTypes/user';
import {setLoading} from '../actions/common';
import {updateAvatar, getCurrentUser, doGetuser, doLogout, createClient, doLogin} from '../../service/user';
import {patchAvatarUpdate, patchInfoUpdate, logout, refreshUserIndicator, refreshUser, login, addToUsers} from '../actions/user';
import {clearRelationship} from '../actions/relationship';
import {clearConversation} from '../actions/conversation';
import notifyUtil from '../../utils/notifyUtil';
import {formatDateFromObj, formatUserFromServer} from '../../utils/format';
import errorTranslate from '../../utils/leancloudErrorMsg';
import alert from '../../config/alert';
import {getDeviceId} from '../../utils/common';

function* updateAvatarAsync(action) {
  const username = action.data.username
  const pic = action.data.pic
  yield put(setLoading(true))
  try {
    const result = yield call(updateAvatar, pic)
    const {avatar, updatedAt} = result.toJSON()
    const data = {
      username,
      pic: {
        url: avatar.url,
        updatedAt: formatDateFromObj(new Date(updatedAt)),
        name: avatar.name
      }
    }
    yield put(setLoading(false))
    yield put(patchAvatarUpdate(data))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(errorTranslate(error), 'long')
  }
}

function* updateInfoAsync(action) {
  const username = action.data.username
  const type = action.data.type
  const info = action.data.info
  yield put(setLoading(true))
  try {
    const user = yield call(getCurrentUser)
    user.set(type, info)
    yield user.save()
    yield put(setLoading(false))
    if(type === 'password') {
      yield put(logout())
      notifyUtil.toast('修改成功，请重新登录', 'long')
    }else {
      yield put(patchInfoUpdate({
        username,
        type,
        info
      }))
      notifyUtil.toast('修改成功', 'long')
    }
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(errorTranslate(error), 'long')
  }
}

function* refreshUserAsync(action) {
  const {key, value} = action
  try {
    yield put(refreshUserIndicator(true))
    const result = yield call(doGetuser, key, value)
    let user = result.toJSON()
    yield put(refreshUserIndicator(false))
    yield put(refreshUser(formatUserFromServer(user)))
  }catch(error) {
    yield put(refreshUserIndicator(false))
    notifyUtil.toast(errorTranslate(error), 'long')
  }
}

function* logoutAsync() {
  try {
    yield put(setLoading(true))
    yield call(doLogout)
    yield put(setLoading(false))
    yield put(logout())
    global.client = null
    yield put(clearRelationship())
    yield put(clearConversation())
  }catch(error) {
    yield put(setLoading(false))
    yield put(logout())
    global.client = null
    yield put(clearRelationship())
    yield put(clearConversation())
    notifyUtil.toast(errorTranslate(error), 'long')
  }
}

function* loginAsync(action) {
  const {username, password} = action
  try {
    yield put(setLoading(true))
    const result = yield call(doLogin, username, password)
    let user = result.toJSON()
    if(user.onlyCurrentDeviceLogin && (user.deviceId !== getDeviceId())) {
      yield put(setLoading(false))
      notifyUtil.toast(alert.onlyCurrentDeviceLogin, 'long')
      return
    }
    yield put(setLoading(false))
    yield put(login(formatUserFromServer(user)))
    yield put(addToUsers(formatUserFromServer(user)))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(errorTranslate(error), 'long')
  }
}

export default function* watchUserAsync() {
  yield takeLatest(TYPES.STARTUPDATEAVATAR, updateAvatarAsync)
  yield takeEvery(TYPES.STARTUPDATEINFO, updateInfoAsync)
  yield takeLatest(TYPES.STARTREFRESHUSER, refreshUserAsync)
  yield takeEvery(TYPES.STARTLOGOUT, logoutAsync)
  yield takeEvery(TYPES.STARTLOGIN, loginAsync)
}
