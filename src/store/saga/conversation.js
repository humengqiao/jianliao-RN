import {takeEvery, takeLatest, put, call} from 'redux-saga/effects';
import * as TYPES from '../actionTypes/conversation';
import {
  fetchConversation,
  fetchContactNotify,
  fetchGroupNotify,
  hideConversation
} from '../../service/conversation';
import {
  patchContactNotify,
  patchGroupNotify,
  patchConversation,
  setConversationUnreadCount,
  removeConversation,
  setHideConversationList
} from '../actions/conversation';
import {setLoading} from '../actions/common';
import {doGetuser} from '../../service/user';
import notifyUtil from '../../utils/notifyUtil';

function* fetchConversationListAsync(action) {
  yield put(setLoading(true))
  try {
    const conversationType = action.conversationType
    const conversationList = yield fetchConversation(conversationType)
    const conversationToJSON = conversationList.map(item => item.toFullJSON())
    yield put(setLoading(false))
    yield put(patchConversation(conversationToJSON, (new Date()).getTime(), conversationType))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* clearUnreadStatusAsync(action) {
  const {conversation, conversationType} = action
  try {
    const conversationToObject = yield global.client.parseConversation(conversation)
    yield conversationToObject.read()
    yield put(setConversationUnreadCount(conversationToObject.id, 0, conversationType))
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* removeConversationAsync(action) {
  const {conversation, username, flag} = action
  try {
    const result = yield hideConversation(conversation.id, username, flag)
    yield put(removeConversation(conversation.id, flag))
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* fetchHideConversationListAsync(action) {
  try {
    const username = action.username
    const result = yield doGetuser('username', username, ['hideConversationList'])
    const hideConversationList = result.toJSON().hideConversationList
    yield put(setHideConversationList(hideConversationList))
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

export default function* watchConversationAsync() {
  yield takeEvery(TYPES.STARTFETCHCONVERSATION, fetchConversationListAsync)
  yield takeLatest(TYPES.STARTCLEARUNREADSTATUS, clearUnreadStatusAsync)
  yield takeEvery(TYPES.STARTREMOVECONVERSATION, removeConversationAsync)
  yield takeEvery(TYPES.STARTFETCHHIDECONVERSATIONLIST, fetchHideConversationListAsync)
}
