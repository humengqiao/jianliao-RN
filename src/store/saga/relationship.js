import {takeEvery, takeLatest, put, call} from 'redux-saga/effects';
import * as TYPES from '../actionTypes/relationship';
import {doGetuser} from '../../service/user';
import {
  fetchContactList,
  fetchGroupList,
  fetchBlackList,
  modifyContact,
  modifyGroupInfoToServer
} from '../../service/relationship';
import {
  setContactList,
  setGroupList,
  setBlackList,
  patchModifyContact,
  patchModifyGroup
} from '../actions/relationship';
import notifyUtil from '../../utils/notifyUtil';
import {setLoading} from '../actions/common';

function* fetchContactAsync(action) {
  const id = action.id
  yield put(setLoading(true))
  try {
    const {contact} = (yield call(doGetuser, '_id', id, ['contact'])).toJSON()
    const willFetchContacts = contact.map(item => {
      return fetchContactList(item)
    })
    const contactList = yield Promise.all(willFetchContacts)
    yield put(setLoading(false))
    yield put(setContactList(contactList))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* fetchGroupAsync(action) {
  const id = action.id
  yield put(setLoading(true))
  try {
    const {group} = (yield call(doGetuser, '_id', id, ['group'])).toJSON()
    const groupList = yield fetchGroupList(group)
    yield put(setLoading(false))
    yield put(setGroupList(groupList))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* fetchBlackListAsync(action) {
  const id = action.id
  yield put(setLoading(true))
  try {
    const {blacklist} = (yield call(doGetuser, '_id', id, ['blacklist'])).toJSON()
    const blackList = yield fetchBlackList(blacklist)
    yield put(setLoading(false))
    yield put(setBlackList(blackList))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* modifyContactAsync(action) {
  const {id, cloudDivide, localDivide} = action
  try {
    const result = yield modifyContact(id, cloudDivide)
    yield put(patchModifyContact(localDivide))
    notifyUtil.toast('设置成功')
  }catch(error) {
    notifyUtil.toast(error.toString(), 'long')
  }
}

function* modifyGroupAsync(action) {
  const {id, nick, pic} = action
  yield put(setLoading(true))
  try {
    const result = yield modifyGroupInfoToServer(id, nick, pic)
    yield put(setLoading(false))
    notifyUtil.toast('更新群资料成功')
    yield put(patchModifyGroup(result.toJSON()))
  }catch(error) {
    yield put(setLoading(false))
    notifyUtil.toast(error.toString(), 'long')
  }
}

export default function* watchRelationshipAsync() {
  yield takeLatest(TYPES.STARTFETCHCONTACT, fetchContactAsync)
  yield takeLatest(TYPES.STARTFETCHGROUP, fetchGroupAsync)
  yield takeLatest(TYPES.STARTFETCHBLACKLIST, fetchBlackListAsync)
  yield takeLatest(TYPES.STARTMODIFYCONTACT, modifyContactAsync)
  yield takeLatest(TYPES.STARTMODIFYGROUP, modifyGroupAsync)
}
