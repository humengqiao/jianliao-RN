import * as TYPES from '../actionTypes/conversation';
import * as conversationTypes from '../../config/conversationType';

const initialState = {
  conversation: [],
  contactNotify: [],
  groupNotify: [],
  hideConversationList: [],
  lastUpdateAt: 0  //会话最后更新时间
}

export default function conversation(state = initialState, action) {
  switch(action.type) {
    case TYPES.SETCONVERSATION:
      {
        const {conversation, lastUpdateAt, conversationType} = action
        let target
        switch(conversationType) {
          case conversationTypes.CONTACT_NOTIFY:
            target = 'contactNotify'
            break
          case conversationTypes.GROUP_NOTIFY:
            target = 'groupNotify'
            break
          case conversationTypes.COMMON:
            target = 'conversation'
            break
        }

        if(!target) {
          return state
        }

        return {
          ...state,
          [target]: conversation,
          lastUpdateAt
        }
      }
      break
    case TYPES.SETHIDECONVERSATIONLIST:
      const hideConversationList = action.hideConversationList
      return {
        ...state,
        hideConversationList
      }
      break
    case TYPES.CLEARCONVERSATION:
      return initialState
      break
    case TYPES.MODIFYCONVERSATION:
      {
        const conversation = action.conversation
        const conversationType = conversation.type
        let target, originalConversation
        if(conversationType === conversationTypes.PRIVATE || conversationType === conversationTypes.GROUP) {
          target = 'conversation'
          originalConversation = [...state.conversation]
        }else if(conversationType === conversationTypes.CONTACT_NOTIFY) {
          target = 'contactNotify'
          originalConversation = [...state.contactNotify]
        }else if(conversationType === conversationTypes.GROUP_NOTIFY) {
          target = 'groupNotify'
          originalConversation = [...state.groupNotify]
        }else {
          return state
        }

        const index = originalConversation.findIndex(item => item.id === conversation.id)
        if(index === -1) {
          originalConversation.unshift(conversation)
        }else {
          originalConversation.splice(index, 1, conversation)
        }

        return {
          ...state,
          [target]: originalConversation
        }
      }
      break
    case TYPES.SETCONVERSATIONUNREADCOUNT:
      {
        const {count, conversationId, conversationType} = action
        let originalConversation = [], target = ''
        if(conversationType === conversationTypes.CONTACT_NOTIFY) {
          originalConversation = [...state.contactNotify]
          target = 'contactNotify'
        }else if(conversationType === conversationTypes.GROUP_NOTIFY) {
          originalConversation = [...state.groupNotify]
          target = 'groupNotify'
        }else if(conversationType === conversationTypes.COMMON){
          originalConversation = [...state.conversation]
          target = 'conversation'
        }

        const index = originalConversation.findIndex(item => item.id === conversationId)
        if(index !== -1) {
          const temp = {...originalConversation[index]}
          temp.unreadMessagesCount = count
          originalConversation.splice(index, 1, temp)
          return {
            ...state,
            [target]: originalConversation
          }
        }

        return state
      }
      break
    case TYPES.REMOVECONVERSATION:
      {
        const {conversationId, flag} = action
        const originalHideConversationList = [...state.hideConversationList]
        const index = originalHideConversationList.findIndex(item => item === conversationId)
        if(flag) {
          if(index === -1) {
            originalHideConversationList.push(conversationId)
          }
        }else {
          if(index !== -1) {
            originalHideConversationList.splice(index, 1)
          }
        }
        return {
          ...state,
          hideConversationList: originalHideConversationList
        }
      }
      break
    default:
      return state
  }
}
