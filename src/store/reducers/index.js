import {combineReducers} from 'redux';
import userReducer from './user';
import chatEntryReducer from './chatEntry';
import commonReducer from './common';
import relationshipReducer from './relationship';
import conversationReducer from './conversation';
import persistConfig from '../../config/persistConfig';
import {persistReducer} from 'redux-persist';

const userPersistConfig = {
  key: 'user',
  ...persistConfig
}

const chatEntryPersistConfig = {
  key: 'chatEntry',
  ...persistConfig,
  blacklist: ['netStatus']
}

const commonPersistConfig = {
  key: 'common',
  ...persistConfig,
  blacklist: ['loading']
}

const relationshipPersistConfig = {
  key: 'relationship',
  ...persistConfig
}

const conversationPersistConfig = {
  key: 'conversation',
  ...persistConfig
}

export default combineReducers({
  user: persistReducer(userPersistConfig, userReducer),
  chatEntry: persistReducer(chatEntryPersistConfig, chatEntryReducer),
  common: persistReducer(commonPersistConfig, commonReducer),
  relationship: persistReducer(relationshipPersistConfig, relationshipReducer),
  conversation: persistReducer(conversationPersistConfig, conversationReducer)
})
