import * as TYPES from '../actionTypes/relationship';

const initialState = {
  contact: [],
  group: {},
  blackList: {}
}

export default function relationship(state = initialState, action) {
  switch(action.type) {
    case TYPES.SETCONTACTLIST:
      {
        const newContact = action.contactList
        const originalContact = [...state.contact]
        for(let i = 0, len = originalContact.length;i < len;i ++) {
          const index = newContact.findIndex(item => item.name === originalContact[i].name)
          if(index === -1) {
            originalContact.shift()
          }else {
            originalContact[i].label = newContact[index].label
            originalContact[i].list = [...newContact[index].list]
            newContact.splice(index, 1)
          }
        }
        originalContact = originalContact.concat(newContact)
        return {
          ...state,
          contact: originalContact
        }
      }
      break
    case TYPES.SETGROUPLIST:
      {
        const newGroup = action.groupList
        const originalGroup = {...state.group}
        originalGroup.label = newGroup.label
        originalGroup.list = newGroup.list
        originalGroup.name = newGroup.name
        originalGroup.expand = originalGroup.expand || newGroup.expand
        return {
          ...state,
          group: originalGroup
        }
      }
      break
    case TYPES.SETBLACKLIST:
      {
        const newBlackList = action.blackList
        const originalBlackList = {...state.blackList}
        originalBlackList.label = newBlackList.label
        originalBlackList.list = newBlackList.list
        originalBlackList.name = newBlackList.name
        originalBlackList.expand = originalBlackList.expand || newBlackList.expand
        return {
          ...state,
          blackList: originalBlackList
        }
      }
      break
    case TYPES.CLEARRELATIONSHIP:
      return initialState
      break
    case TYPES.TOGGLEEXPAND:
      {
        const {divideType, divideId} = action
        let relation
        if(divideType === 'contact') {
          relation = [...state.contact]
        }else if(divideType === 'group') {
          relation = [state.group]
        }else if(divideType === 'blackList') {
          relation = [state.blackList]
        }

        relation.forEach(item => {
          if(item.name === divideId) {
            item.expand = !item.expand
          }
        })

        if(divideType === 'group' || divideType === 'blackList') {
          relation = relation.shift()
        }

        return {
          ...state,
          [divideType]: relation
        }
      }
      break
    case TYPES.PATCHMODIFYCONTACT:
      {
        const contact = action.contact
        return {
          ...state,
          contact
        }
      }
      break
    case TYPES.PATCHMODIFYGROUP:
      {
        const originalGroup = {...state.group}
        const group = action.group
        const index = originalGroup.list.findIndex(item => item.objectId === group.objectId)
        if(index !== -1) {
          if(group.avatar) {
            originalGroup.list[index] = {
              ...originalGroup.list[index],
              avatar: group.avatar
            }
          }else if(group.nick) {
            originalGroup.list[index] = {
              ...originalGroup.list[index],
              nick: group.nick
            }
          }
        }

        return {
          ...state,
          group: originalGroup
        }
      }
      break
    case TYPES.REMOVEUSER:
      {
        const {username, divideName} = action
        const originalContact = [...state.contact]
        originalContact.forEach(item => {
          if(item.name === divideName) {
            const index = item.list.findIndex(item => item.username === username)
            if(index !== -1) {
              item.list.splice(index, 1)
            }
          }
        })
        return {
          ...state,
          contact: originalContact
        }
      }
      break
    case TYPES.MOVETOBLACKLIST:
      {
        const {user, divideName} = action
        const originalContact = [...state.contact]
        const originalBlacklist = {...state.blackList}
        originalContact.forEach(item => {
          if(item.name === divideName) {
            const index = item.list.findIndex(item => item.username === user.username)
            if(index !== -1) {
              item.list.splice(index, 1)
            }
          }
        })
        originalBlacklist.list.unshift(user)
        return {
          ...state,
          contact: originalContact,
          blackList: originalBlacklist
        }
      }
      break
    case TYPES.MOVEFROMBLACKLIST:
      {
        const username = action.username
        const originalBlacklist = {...state.blackList}
        const index = originalBlacklist.list.findIndex(item => item.username === username)
        if(index !== -1) {
          originalBlacklist.list.splice(index, 1)
        }
        return {
          ...state,
          blackList: originalBlacklist
        }
      }
      break
    case TYPES.EXITGROUP:
      {
        const originalGroup = {...state.group}
        const groupId = action.groupId
        const index = originalGroup.list.findIndex(item => item.groupId === groupId)
        if(index !== -1) {
          originalGroup.list.splice(index, 1)
        }
        return {
          ...state,
          group: originalGroup
        }
      }
      break
    default:
      return state
  }
}
