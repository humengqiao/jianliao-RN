import * as TYPES from '../actionTypes/user';

const initialState = {
  isLoggedIn: false,
  user: {},
  loginedUsers: [],
  refreshing: false
}

export default function user(state = initialState, action) {
  switch(action.type) {
    case TYPES.USER_LOGIN:
      return {
        ...state,
        isLoggedIn: true,
        user: action.user
      }
      break
    case TYPES.USER_LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        user: {}
      }
      break
    case TYPES.UPDATEINFO:
      {
        let type = action.data.type
        let info = action.data.info
        let username = action.data.username
        let loginedUsers = [...state.loginedUsers]
        let index = loginedUsers.findIndex(item => item.username === username)
        if(index !== -1) {
          loginedUsers[index][type] = info
        }
        return {
          ...state,
          user: {
            ...state.user,
            [type]: info
          },
          loginedUsers
        }
      }
      break
    case TYPES.USERS_ADD:
      {
        const index = state.loginedUsers.findIndex(item => item.username === action.user.username)
        const loginedUsers = Array.from(state.loginedUsers)
        if(index === -1) {
          loginedUsers.unshift(action.user)
        }else {
          loginedUsers.splice(index, 1)
          loginedUsers.unshift(action.user)
        }
        return {
          ...state,
          loginedUsers
        }
      }
      break
    case TYPES.USERS_REMOVE:
      {
        const index = state.loginedUsers.findIndex(item => item.username === action.username)
        const loginedUsers = Array.from(state.loginedUsers)
        if(index !== -1) {
          loginedUsers.splice(index, 1)
        }
        return {
          ...state,
          loginedUsers
        }
      }
      break
    case TYPES.REMOVE_ALL:
      return {
        ...initialState
      }
      break
    case TYPES.UPDATE_AVATAR:
      {
        let index = state.loginedUsers.findIndex(item => item.username === action.data.username)
        let loginedUsers = [...state.loginedUsers]
        if(index !== -1){
          loginedUsers[index].avatar = {...action.data.pic}
        }

        return {
          ...state,
          loginedUsers,
          user: {
            ...state.user,
            avatar: action.data.pic
          }
        }
      }
      break
    case TYPES.REFRESHUSERINDICATOR:
      return {
        ...state,
        refreshing: action.refreshing
      }
      break
    case TYPES.REFRESHUSER:
      {
        let user = action.user
        let index = state.loginedUsers.findIndex(item => item.username === user.username)
        let loginedUsers = [...state.loginedUsers]
        if(index !== -1) {
          loginedUsers[index] = {...user}
        }
        return {
          ...state,
          user,
          loginedUsers
        }
      }
      break
    default:
      return state
  }
}
