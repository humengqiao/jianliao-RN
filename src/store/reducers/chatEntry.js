import * as TYPES from '../actionTypes/chatEntry';
import config from '../../config';

const initialState = {
  selectedIndex: config.selectedIndex,
  netStatus: {}
}

export default function chatEnty(state = initialState, action) {
  switch(action.type) {
    case TYPES.CHANGE_SELECTEDINDEX:
      return {
        ...state,
        selectedIndex: parseInt(action.selectedIndex)
      }
      break;
    case TYPES.CHANGE_NETSTATUS:
      return {
        ...state,
        netStatus: action.netStatus
      }
    default:
      return state
  }
}
