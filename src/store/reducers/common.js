import * as TYPES from '../actionTypes/common';
import {themeColor} from '../../styles/common';

const initialState = {
  loading: false,
  themeColor
}

export default function common(state = initialState, action) {
  switch(action.type) {
    case TYPES.SETLOADING:
      return {
        ...state,
        loading: action.loading
      }
      break
    case TYPES.SETTHEMECOLOR:
      return {
        ...state,
        themeColor: action.themeColor
      }
      break
    default:
      return state
  }
}
