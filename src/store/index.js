import {applyMiddleware, createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import reducers from './reducers';
import persistConfig from '../config/persistConfig';
import reduxMiddlewares from '../config/reduxMiddlewares';
import createSagaMiddleware, {END} from 'redux-saga';
import startApp, {HOME_SCREEN, LOGIN_SCREEN} from '../../setup';
import {createClient} from '../service/user';
import notifyUtil from '../utils/notifyUtil';
import {AppState} from 'react-native';

export default configureStore = initialState => {
  const sagaMiddleware = createSagaMiddleware()
  const store = createStore(reducers, initialState, applyMiddleware(...[sagaMiddleware, ...reduxMiddlewares]))
  const persistor = persistStore(store, undefined, () => {
    const state = store.getState()
    if(state.user.isLoggedIn) {
      const selectedIndex = state.chatEntry.selectedIndex
      const themeColor = state.common.themeColor
      startApp(HOME_SCREEN, themeColor, selectedIndex)
    }else {
      startApp(LOGIN_SCREEN)
    }
  })

  store.runSaga = sagaMiddleware.run
  store.close = () => store.dispatch(END)
  return {
    store,
    persistor
  }
}
