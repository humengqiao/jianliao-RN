import {Navigation} from 'react-native-navigation';
import LoginScreen from '../pages/LoginScreen';
import ConversationScreen from '../pages/ConversationScreen';
import RegisterScreen from '../pages/RegisterScreen';
import ContactScreen from '../pages/ContactScreen';
import MeScreen from '../pages/MeScreen';
import LoginUsersScreen from '../pages/LoginUsersScreen';
import VerifyScreen from '../pages/VerifyScreen';
import RuleScreen from '../pages/RuleScreen';
import ResetPassScreen from '../pages/ResetPassScreen';
import ResetPassConfirmScreen from '../pages/ResetPassConfirmScreen';
import UpdateInfoScreen from '../pages/UpdateInfoScreen';
import SettingsScreen from '../pages/SettingsScreen';
import FeedbackScreen from '../pages/FeedbackScreen';
import AboutScreen from '../pages/AboutScreen';
import ModifyThemeScreen from '../pages/ModifyThemeScreen';
import DivideManageScreen from '../pages/DivideManageScreen';
import ContactInfoDetailScreen from '../pages/ContactInfoDetailScreen';
import GroupInfoDetailScreen from '../pages/GroupInfoDetailScreen';
import BlacklistInfoDetailScreen from '../pages/BlacklistInfoDetailScreen';
import MoveToDivideScreen from '../pages/MoveToDivideScreen';
import GroupMembersScreen from '../pages/GroupMembersScreen';
import AddUserOrGroupScreen from '../pages/AddUserOrGroupScreen';
import ChatScreen from '../pages/ChatScreen';
import ModifyGroupInfoScreen from '../pages/ModifyGroupInfoScreen';
import ContactNotifyScreen from '../pages/ContactNotifyScreen';
import ContactNotifyDetailScreen from '../pages/ContactNotifyDetailScreen';
import AcceptContactToDivideScreen from '../pages/AcceptContactToDivideScreen';
import SearchContactOrGroupScreen from '../pages/SearchContactOrGroupScreen';
import GroupNotifyScreen from '../pages/GroupNotifyScreen';
import GroupNotifyDetailScreen from '../pages/GroupNotifyDetailScreen';
import CreateGroupScreen from '../pages/CreateGroupScreen';

export function registerScreens(store, Provider) {
  Navigation.registerComponent('jianliao.LoginScreen', () => LoginScreen, store, Provider)
  Navigation.registerComponent('jianliao.ConversationScreen', () => ConversationScreen, store, Provider)
  Navigation.registerComponent('jianliao.RegisterScreen', () => RegisterScreen, store, Provider)
  Navigation.registerComponent('jianliao.ContactScreen', () => ContactScreen, store, Provider)
  Navigation.registerComponent('jianliao.MeScreen', () => MeScreen, store, Provider)
  Navigation.registerComponent('jianliao.LoginUsersScreen', () => LoginUsersScreen, store, Provider)
  Navigation.registerComponent('jianliao.VerifyScreen', () => VerifyScreen, store, Provider)
  Navigation.registerComponent('jianliao.RuleScreen', () => RuleScreen)
  Navigation.registerComponent('jianliao.ResetPassScreen', () => ResetPassScreen)
  Navigation.registerComponent('jianliao.ResetPassConfirmScreen', () => ResetPassConfirmScreen)
  Navigation.registerComponent('jianliao.UpdateInfoScreen', () => UpdateInfoScreen, store, Provider)
  Navigation.registerComponent('jianliao.SettingsScreen', () => SettingsScreen, store, Provider)
  Navigation.registerComponent('jianliao.FeedbackScreen', () => FeedbackScreen)
  Navigation.registerComponent('jianliao.AboutScreen', () => AboutScreen)
  Navigation.registerComponent('jianliao.ModifyThemeScreen', () => ModifyThemeScreen, store, Provider)
  Navigation.registerComponent('jianliao.DivideManageScreen', () => DivideManageScreen, store, Provider)
  Navigation.registerComponent('jianliao.ContactInfoDetailScreen', () => ContactInfoDetailScreen, store, Provider)
  Navigation.registerComponent('jianliao.GroupInfoDetailScreen', () => GroupInfoDetailScreen, store, Provider)
  Navigation.registerComponent('jianliao.BlacklistInfoDetailScreen', () => BlacklistInfoDetailScreen, store, Provider)
  Navigation.registerComponent('jianliao.MoveToDivideScreen', () => MoveToDivideScreen, store, Provider)
  Navigation.registerComponent('jianliao.GroupMembersScreen', () => GroupMembersScreen, store, Provider)
  Navigation.registerComponent('jianliao.AddUserOrGroupScreen', () => AddUserOrGroupScreen, store, Provider)
  Navigation.registerComponent('jianliao.ChatScreen', () => ChatScreen, store, Provider)
  Navigation.registerComponent('jianliao.ModifyGroupInfoScreen', () => ModifyGroupInfoScreen, store, Provider)
  Navigation.registerComponent('jianliao.ContactNotifyScreen', () => ContactNotifyScreen, store, Provider)
  Navigation.registerComponent('jianliao.ContactNotifyDetailScreen', () => ContactNotifyDetailScreen, store, Provider)
  Navigation.registerComponent('jianliao.AcceptContactToDivideScreen', () => AcceptContactToDivideScreen, store, Provider)
  Navigation.registerComponent('jianliao.SearchContactOrGroupScreen', () => SearchContactOrGroupScreen)
  Navigation.registerComponent('jianliao.GroupNotifyScreen', () => GroupNotifyScreen, store, Provider)
  Navigation.registerComponent('jianliao.GroupNotifyDetailScreen', () => GroupNotifyDetailScreen, store, Provider)
  Navigation.registerComponent('jianliao.CreateGroupScreen', () => CreateGroupScreen)
}
